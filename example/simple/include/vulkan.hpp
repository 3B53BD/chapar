#ifndef CHAPAR_FTqjAHt3K3HWCThS_HEADER_GUARD
#define CHAPAR_FTqjAHt3K3HWCThS_HEADER_GUARD

#include <chapar/graphics/raw_scene.hpp>
#include <vector>
#include <memory>

namespace chapar {
    class WindowImpl;
}

namespace chapar::sdl {
    class Window;
}

namespace chapar::vulkan {
    class Device;
    class Instance;
    class PhysicalDevice;
    class Queue;
    class QueueFamily;
    class Surface;
    class Swapchain;
    class Window;
}

using namespace chapar;





// ==================================================
// Instance creation
// ==================================================

std::unique_ptr<vulkan::Instance> create_instance(sdl::Window const& window);





// ==================================================
// Surface creation
// ==================================================

std::unique_ptr<vulkan::Surface> create_surface(
    vulkan::Window const& window,
    vulkan::Instance const& instance
);





// ==================================================
// Physical device creation
// ==================================================

std::vector<vulkan::QueueFamily> queue_families(
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Surface const& surface
) noexcept;

std::unique_ptr<vulkan::PhysicalDevice> choose_physical_device(
    vulkan::Instance const& instance,
    vulkan::Surface& surface
);

VkFormat choose_format(
    vulkan::PhysicalDevice const& physical_device,
    std::vector<VkFormat> const& candidates,
    VkImageTiling tiling,
    VkFormatFeatureFlags features
) noexcept;

VkFormat default_depth_format(vulkan::PhysicalDevice const& physical_device) noexcept;





// ==================================================
// Device creation
// ==================================================

std::unique_ptr<vulkan::Device> create_device(
    vulkan::Instance const& instance,
    vulkan::Surface const& surface,
    vulkan::PhysicalDevice const& physical_device
);





// ==================================================
// Queue creation
// ==================================================

std::pair<vulkan::Queue, vulkan::Queue> create_queues(
    vulkan::Surface const& surface,
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Device const& device
);





// ==================================================
// Swapchain creation
// ==================================================

std::unique_ptr<vulkan::Swapchain> create_swapchain(
    WindowImpl const& window,
    vulkan::Surface const& surface,
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Device const& device
);

#endif
