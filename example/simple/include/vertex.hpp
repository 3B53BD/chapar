#ifndef CHAPAR_X99VjZ2vnrCypYxS_HEADER_GUARD
#define CHAPAR_X99VjZ2vnrCypYxS_HEADER_GUARD

#include <chapar/graphics/raw_scene.hpp>
#include <glm/glm.hpp>
#include <vector>

using namespace chapar;

struct Vertex {
    static std::vector<graphics::VertexComponent> components() noexcept;
    static std::vector<VkVertexInputBindingDescription> bindings() noexcept;
    static std::vector<VkVertexInputAttributeDescription> attributes() noexcept;

    glm::vec3 pos;
    glm::vec3 color;
    glm::vec2 uv;
};

#endif
