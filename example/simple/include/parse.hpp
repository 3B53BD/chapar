#ifndef CHAPAR_BmW8mGKTde2pLTiJ_HEADER_GUARD
#define CHAPAR_BmW8mGKTde2pLTiJ_HEADER_GUARD

#include "chapar/graphics/raw_scene.hpp"
#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

namespace chapar::vulkan {
    class Allocator;
    class Camera;
    class CommandPool;
    class DescriptorSetLayout;
    class Device;
    class PipelineLayout;
    class Queue;
    class Scene;
    class Swapchain;
}

using namespace chapar;

struct SceneInfo {
    std::vector<std::unique_ptr<vulkan::DescriptorSetLayout>> set_layouts;
    std::unique_ptr<vulkan::PipelineLayout> pipeline_layout;
    std::unique_ptr<vulkan::Scene> model;
    std::unique_ptr<vulkan::Camera> camera;
};

SceneInfo load_config(
    vulkan::Device const& device,
    vulkan::Allocator const& allocator,
    vulkan::Queue const& transfer_queue,
    vulkan::Swapchain const& swapchain,
    vulkan::CommandPool const& command_pool
);

#endif
