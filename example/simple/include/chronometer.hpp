#ifndef CHAPAR_fRSUd5wiAVtdYbFu_HEADER_GUARD
#define CHAPAR_fRSUd5wiAVtdYbFu_HEADER_GUARD

#include <chrono>

class Chronometer
{
private:
    using ms = std::chrono::milliseconds;
    using clock = std::chrono::high_resolution_clock;
    using time_point = std::chrono::time_point<clock>;

public:
    Chronometer() noexcept;
    Chronometer(Chronometer const&) = default;
    Chronometer& operator=(Chronometer const&) = default;
    Chronometer(Chronometer&&) = default;
    Chronometer& operator=(Chronometer&&) = default;
    ~Chronometer() = default;

    void reset() noexcept;
    double elapsed() const noexcept;

private:
    time_point start_time_;
};


#endif
