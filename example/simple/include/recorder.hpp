#ifndef CHAPAR_vrmuxpy3BcnZDUiT_HEADER_GUARD
#define CHAPAR_vrmuxpy3BcnZDUiT_HEADER_GUARD

struct CmdBeginRenderPassInfo {
    VkRenderPass render_pass = VK_NULL_HANDLE;
    VkFramebuffer framebuffer = VK_NULL_HANDLE;
    VkRect2D render_area = {};
    std::vector<VkClearValue> clear_values;
};

class Recorder {
public:

private:
    uint32_t image_index_ = 0;
    std::unique_ptr<vulkan::CommandPool> command_pool_;
    std::vector<std::unique_ptr<vulkan::CommandPool>> command_buffers_;
    std::vector<VkCommandBuffer> secondary_command_buffers_;
};

#endif
