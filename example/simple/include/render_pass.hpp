#ifndef CHAPAR_C2YmrkFnWVa6dM4Z_HEADER_GUARD
#define CHAPAR_C2YmrkFnWVa6dM4Z_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

namespace chapar::vulkan {
    class Allocator;
    class Attachment;
    class Device;
    class Framebuffer;
    class PhysicalDevice;
    class RenderPass;
    class Surface;
    class Swapchain;
}

using namespace chapar;

std::unique_ptr<vulkan::RenderPass> create_render_pass(
    vulkan::Device const& device,
    vulkan::Surface const& surface,
    vulkan::Swapchain const& swapchain,
    VkSampleCountFlagBits samples,
    VkFormat depth_format
);

void create_framebuffers(
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Device const& device,
    vulkan::Allocator const& allocator,
    vulkan::Swapchain const& swapchain,
    vulkan::RenderPass const& render_pass,
    VkSampleCountFlagBits samples,
    VkFormat depth_format,
    std::vector<std::unique_ptr<vulkan::Attachment>>& attachments,
    std::vector<VkClearValue>& clear_values,
    std::vector<std::unique_ptr<vulkan::Framebuffer>>& framebuffers
);

#endif
