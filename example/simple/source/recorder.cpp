#include "recorder.hpp"

Renderer::Renderer()
{
    auto pool_info = vulkan::CommandPool::create_info(graphics_queue.family());
    pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    auto command_pool = std::make_unique<vulkan::CommandPool>(device->handle(), pool_info);

    auto alloc_info = command_pool->allocate_info(1);
    std::vector<std::unique_ptr<vulkan::CommandBuffer>> command_buffers(image_count);
    for (auto& command_buffer : command_buffers) {
        command_buffer = std::make_unique<vulkan::CommandBuffer>(*command_pool, alloc_info);
    }
}

void Renderer::begin(RenderPassBeginInfo const& info)
{
    auto const& cmd = command_buffers_[image_index_];

    vkResetCommandBuffer(cmd->handle(), {});

    cmd->begin(vulkan::CommandBuffer::begin_info());
    cmd->begin_render_pass(info.render_pass, info.framebuffer, info.render_area, info.clear_values, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
}

void Renderer::end()
{
    auto const& cmd = command_buffers_[image_index_];

    vkCmdExecuteCommands(cmd->handle(), secondary_command_buffers_.size(), secondary_command_buffers_.data());

    cmd->end_render_pass();
    cmd->end();
}

void Renderer::next()
{
    auto const& cmd = command_buffers_[image_index_];

    vkCmdNextSubpass(cmd->handle(), VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
}

void Renderer::bind(VkPipelineLayout layout, Node const& mesh)
{

}

void Renderer::record(Scene const& scenes)
{
    std::vector<VkDescriptorSet> node_sets;


    // Records scene
    struct Binder {
        vulkan::CommandBuffer const* cb;
        vulkan::Camera const* camera;
        VkPipelineLayout layout;
        gsl::index index;
        void bind(vulkan::Node const& node) const noexcept
        {
            for (auto const& child : node.children) {
                bind(child);
            }

            if (node.mesh) {
                cb->bind(layout, {
                    node.uniforms[index].descriptor_set,
                    camera->descriptor_set(index)
                });

                for (auto const& primitive : node.mesh->primitives) {
                    primitive.buffer->bind(cb->handle());
                    primitive.buffer->draw(cb->handle());
                }
            }
        }
    };

    auto const& scb = scene.command_buffers[image_index];
    vkResetCommandBuffer(scb->handle(), {});

    Binder binder;
    binder.cb = scb.get();
    binder.camera = camera.get();
    binder.layout = layout->handle();
    binder.index = image_index;

    auto inherit_info = vulkan::CommandBuffer::inheritance_info();
    inherit_info.renderPass = render_pass->handle();
    inherit_info.framebuffer = framebuffers[image_index]->handle();
    begin_info = vulkan::CommandBuffer::begin_info();
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
    begin_info.pInheritanceInfo = &inherit_info;

    scb->begin(begin_info);
    scb->bind(pipeline->handle());
    for (auto const& node : scene.nodes) {
        binder.bind(node);
    }
    scb->end();



    // Records imgui
    inherit_info = vulkan::CommandBuffer::inheritance_info();
    inherit_info.renderPass = render_pass->handle();
    inherit_info.subpass = 1;
    inherit_info.framebuffer = framebuffers[image_index]->handle();
    auto imgui_cmd_buf = imgui->record(inherit_info, image_index);

    std::vector<VkCommandBuffer> all_cmd_bufs = {
        scene.command_buffers[image_index]->handle(),
        imgui_cmd_buf
    };


}
