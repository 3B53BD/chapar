#include "chronometer.hpp"
#include "imgui.hpp"
#include "parse.hpp"
#include "render_pass.hpp"
#include "vertex.hpp"
#include "vulkan.hpp"

#include "chapar/core/error.hpp"
// #include "chapar/core/window.hpp"

#include "chapar/graphics/port/tinygltf/scene.hpp"
#include "chapar/graphics/port/vulkan/scene.hpp"
#include "chapar/graphics/port/vulkan/imgui.hpp"

#include "chapar/sdl/window.hpp"

#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/buffer.hpp"
#include "chapar/vulkan/camera.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/descriptor.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/frame_sync.hpp"
#include "chapar/vulkan/framebuffer.hpp"
// #include "chapar/vulkan/image.hpp"
#include "chapar/vulkan/instance.hpp"
#include <chapar/vulkan/physical_device.hpp>
#include "chapar/vulkan/pipeline.hpp"
// #include "chapar/vulkan/queue.hpp"
#include <chapar/vulkan/render_pass.hpp>
#include "chapar/vulkan/scene.hpp"
#include "chapar/vulkan/shader.hpp"
#include "chapar/vulkan/surface.hpp"
#include "chapar/vulkan/swapchain.hpp"
// #include "chapar/vulkan/texture.hpp"
// #include "chapar/vulkan/window.hpp"

#include <glm/glm.hpp>
#include <gsl-lite/gsl-lite.hpp>
#include <SDL2/SDL.h>

#include <filesystem>
#include <map>
#include <set>
#include <thread>
#include <vector>
#include <cstring>

#include <iostream>





int main()
{
    std::cout << std::boolalpha;

    auto window_impl = sdl::Window::vulkan();

    auto instance = create_instance(*window_impl);

    auto surface = create_surface(*window_impl, *instance);

    auto physical_device = choose_physical_device(*instance, *surface);

    auto device = create_device(*instance, *surface, *physical_device);

    auto allocator = std::make_unique<vulkan::Allocator>(instance->handle(), physical_device->handle(), device->handle());

    auto [graphics_queue, present_queue] = create_queues(*surface, *physical_device, *device);

    auto swapchain = create_swapchain(*window_impl, *surface, *physical_device, *device);

    auto frames = std::make_unique<vulkan::FrameSync>(device->handle(), 2);

    auto image_count = swapchain->image_count();
    auto depth_format = default_depth_format(*physical_device);
    auto samples = physical_device->max_samples();



    // RenderPass.
    auto render_pass = create_render_pass(*device, *surface, *swapchain, samples, depth_format);

    // Framebuffers.
    std::vector<std::unique_ptr<vulkan::Attachment>> attachments;
    std::vector<VkClearValue> clear_values;
    std::vector<std::unique_ptr<vulkan::Framebuffer>> framebuffers;
    create_framebuffers(*physical_device, *device, *allocator, *swapchain, *render_pass, samples, depth_format, attachments, clear_values, framebuffers);



    // // ===================
    // std::vector<CmdBeginRenderPassInfo> cmd_brps(image_count);
    // for (gsl::index i = 0; i < image_count; ++i) {
    //     cmd_brps[i].render_pass = render_pass->handle();
    //     cmd_brps[i].framebuffer = framebuffers[i]->handle();
    //     cmd_brps[i].render_area = {{0, 0}, swapchain->image_extent()};
    //     cmd_brps[i].clear_values = clear_values;
    // }
    // // ===================



    // Commands.
    auto pool_info = vulkan::CommandPool::create_info(graphics_queue.family());
    pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    auto command_pool = std::make_unique<vulkan::CommandPool>(device->handle(), pool_info);

    auto alloc_info = command_pool->allocate_info(1);
    std::vector<std::unique_ptr<vulkan::CommandBuffer>> command_buffers(image_count);
    for (auto& command_buffer : command_buffers) {
        command_buffer = std::make_unique<vulkan::CommandBuffer>(*command_pool, alloc_info);
    }



    // Loads and creates descriptor set layouts.
    auto scene = load_config(*device, *allocator, graphics_queue, *swapchain, *command_pool);



    // Creates scene pipeline
    vulkan::PipelineCreateInfoBuilder pcib;

    auto vertex_bindings = Vertex::bindings();
    auto vertex_attributes = Vertex::attributes();
    pcib.set_vertex_input(vertex_bindings, vertex_attributes);

    auto const& swapchain_extent = swapchain->image_extent();
    pcib.add_viewport(0, 0, static_cast<float>(swapchain_extent.width), static_cast<float>(swapchain_extent.height), 0, 1);
    pcib.add_scissor({0, 0}, swapchain_extent);

    pcib.set_samples(samples);

    pcib.add_color_blend_attachment();

    auto vert_spirv = vulkan::Shader::read_spirv("build/debug/shd/scene.vert.spv");
    auto vert_shader = std::make_unique<vulkan::VertexShader>(device->handle(), vert_spirv);
    pcib.add_shader_stage(vert_shader->pipeline_stage());

    auto frag_spirv = vulkan::Shader::read_spirv("build/debug/shd/scene.frag.spv");
    auto frag_shader = std::make_unique<vulkan::FragmentShader>(device->handle(), frag_spirv);
    pcib.add_shader_stage(frag_shader->pipeline_stage());

    pcib.set_render_pass(render_pass->handle());
    pcib.set_pipeline_layout(scene.pipeline_layout->handle());

    auto pipeline = std::make_unique<vulkan::Pipeline>(device->handle(), pcib.pipeline_create_info());

    vert_spirv.clear();
    vert_shader.reset();
    frag_spirv.clear();
    frag_shader.reset();




    // Creates imgui
    auto imgui = std::make_unique<graphics::vulkan_port::Imgui>(*window_impl, *instance, *physical_device, *device, graphics_queue, *swapchain, *render_pass, *command_pool);



    float val = 0.1;

    Chronometer chronometer;
    while (true) {
        auto delta_time = chronometer.elapsed();
        chronometer.reset();
        auto spf = std::chrono::milliseconds{30};
        if (delta_time < spf.count()) {
            using std::chrono::duration;
            auto dt = std::chrono::milliseconds{static_cast<long>(delta_time)};
            std::this_thread::sleep_for(spf - dt);
        }

        SDL_Event e;
        while (SDL_PollEvent(&e)) {
            imgui->process_event(&e);
            if (e.type == SDL_QUIT) break;
            if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_RESIZED && e.window.windowID == SDL_GetWindowID(window_impl->handle())) {
                throw Exception("resize");
            }
        }

        if (e.type == SDL_QUIT) break;

        auto [frame, result] = frames->wait_idle();
        VkResult res = swapchain->aquire_next_image(frame.aquired);
        if (res == VK_ERROR_OUT_OF_DATE_KHR) {
            throw Exception{"VK_ERROR_OUT_OF_DATE_KHR"};
        }



        imgui->new_frame(&val);

        auto image_index = swapchain->image_index();

        scene.model->image_index = image_index;
        scene.model->nodes[0].model.rot += glm::vec3(0.0f, delta_time * val, 0.0f);
        scene.model->update();



        // ============================================
        // rec->begin(cmd_brps[image_index]);
        // ============================================



        // Groups record calls
        auto const& pcb = command_buffers[image_index];
        vkResetCommandBuffer(pcb->handle(), {});
        auto begin_info = vulkan::CommandBuffer::begin_info();
        pcb->begin(begin_info);
        pcb->begin_render_pass(render_pass->handle(), framebuffers[image_index]->handle(), {{0, 0}, swapchain->image_extent()}, clear_values, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

        // Records scene
        auto const& scb = scene.model->command_buffers[image_index];
        vkResetCommandBuffer(scb->handle(), {});

        auto inherit_info = vulkan::CommandBuffer::inheritance_info();
        inherit_info.renderPass = render_pass->handle();
        inherit_info.framebuffer = framebuffers[image_index]->handle();

        begin_info.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
        begin_info.pInheritanceInfo = &inherit_info;

        scb->begin(begin_info);
        scb->bind(pipeline->handle());
        scb->bind(scene.pipeline_layout->handle(), 1, {scene.camera->descriptor_set(image_index)});
        scene.model->record(scene.pipeline_layout->handle());
        scb->end();

        vkCmdNextSubpass(pcb->handle(), VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

        // Records imgui
        inherit_info.subpass = 1;
        auto imgui_cmd_buf = imgui->record(inherit_info, image_index);

        std::vector<VkCommandBuffer> all_cmd_bufs = {
            scb->handle(),
            imgui_cmd_buf
        };

        vkCmdExecuteCommands(pcb->handle(), all_cmd_bufs.size(), all_cmd_bufs.data());

        pcb->end_render_pass();
        pcb->end();


        swapchain->wait_idle(frame.idle);
        vkResetFences(device->handle(), 1, &frame.idle);

        vulkan::QueueSumbitInfo submit;
        submit.wait(frame.aquired, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
        submit.command(command_buffers[image_index]->handle());
        submit.signal(frame.rendered);
        graphics_queue.submit(submit.info(), frame.idle);

        res = swapchain->present(present_queue, {frame.rendered});
        if (res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR) {
            throw Exception("swapchain.present is out of date.");
        }

        frames->advance();
    }



    // Cleanup

    device->wait_idle();

    imgui.reset();

    pipeline.reset();
    scene.pipeline_layout.reset();

    scene.camera.reset();

    scene.model->nodes.clear();
    scene.model->meshes.clear();
    scene.model->textures.clear();
    scene.model->descriptor_pool.reset();
    scene.model->command_buffers.clear();

    scene.set_layouts.clear();

    command_buffers.clear();
    command_pool.reset();

    framebuffers.clear();
    attachments.clear();
    render_pass.reset();

    frames.reset();
    swapchain.reset();

    allocator.reset();
    device.reset();
    physical_device.reset();
    surface.reset();

    instance.reset();

    window_impl.reset();

    return 0;
}
