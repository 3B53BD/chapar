#include "vulkan.hpp"
#include "chapar/core/error.hpp"
#include "chapar/sdl/window.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/instance.hpp"
#include <chapar/vulkan/physical_device.hpp>
#include "chapar/vulkan/queue.hpp"
#include "chapar/vulkan/surface.hpp"
#include "chapar/vulkan/swapchain.hpp"
#include "chapar/vulkan/window.hpp"

#include <set>
#include <cstring>

// ==================================================
// Instance creation
// ==================================================

bool check_validation_layers_support(
    std::vector<gsl::czstring> enabled_extensions
)
{
    auto available = vulkan::Instance::layer_properties();

    for (auto const name : enabled_extensions) {
        bool found = false;
        for (auto const& ava : available) {
            if (strcmp(ava.layerName, name) == 0) {
                found = true;
                break;
            }
        }

        if (!found) {
            return false;
        }
    }

    return true;
}

std::unique_ptr<vulkan::Instance> create_instance(sdl::Window const& window)
{
    std::vector<gsl::czstring> validation_layers;
#ifndef NDEBUG
    validation_layers.push_back("VK_LAYER_KHRONOS_validation");
#endif
    auto result = check_validation_layers_support(validation_layers);
    if (!result) {
        throw Exception{"Not all validation layers supported."};
    }

    auto enabled_extensions = window.extensions();
    enabled_extensions.push_back("VK_KHR_get_physical_device_properties2");
#ifndef NDEBUG
    enabled_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    std::sort(enabled_extensions.begin(), enabled_extensions.end());
    auto last = std::unique(enabled_extensions.begin(), enabled_extensions.end());
    enabled_extensions.erase(last, enabled_extensions.end());

#ifndef NDEBUG
    bool debug = true;
#else
    bool debug = false;
#endif
    return std::make_unique<vulkan::Instance>(validation_layers, enabled_extensions, debug);
}





// ==================================================
// Surface creation
// ==================================================

std::unique_ptr<vulkan::Surface> create_surface(
    vulkan::Window const& window,
    vulkan::Instance const& instance
)
{
    auto [surface, result] = window.surface(instance.handle());
    if (!result) {
        throw Exception{"SDL failed to create surface."};
    }

    return std::make_unique<vulkan::Surface>(instance.handle(), surface);
}






// ==================================================
// Physical device creation
// ==================================================

std::pair<VkSurfaceFormatKHR, bool> supported_format(
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Surface const& surface,
    std::vector<VkSurfaceFormatKHR> const& candidates
) noexcept
{
    auto available = physical_device.formats(surface.handle());

    for (auto const& can : candidates) {
        for (auto const& fmt : available) {
            if (can.format == fmt.format && can.colorSpace == fmt.colorSpace) {
                return std::make_pair(can, true);
            }
        }
    }

    VkSurfaceFormatKHR fmt;
    fmt.format = VK_FORMAT_B8G8R8A8_SRGB;
    fmt.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    return std::make_pair(fmt, false);
}

std::vector<vulkan::QueueFamily> queue_families(
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Surface const& surface
) noexcept
{
    auto properties = physical_device.queue_family_properties();
    std::optional<vulkan::QueueFamily> graphics_family;
    std::optional<vulkan::QueueFamily> present_family;
    for (uint32_t i = 0; i < properties.size(); ++i) {
        bool support_present = physical_device.support_surface(i, surface.handle());
        vulkan::QueueFamily family{i, properties[i], support_present};

        if (!graphics_family && family.graphics_bit()) {
            graphics_family = family;
        }
        if (!present_family && family.surface_bit()) {
            present_family = family;
        }

        if (graphics_family && present_family) {
            break;
        }
    }

    std::vector<vulkan::QueueFamily> families;

    if (!graphics_family || !present_family) {
        // Physical device must have queue families that support required support.
        return families;
    }

    families = {
        graphics_family.value(),
        present_family.value()
    };
    return families;
}

std::unique_ptr<vulkan::PhysicalDevice> choose_physical_device(
    vulkan::Instance const& instance,
    vulkan::Surface& surface
)
{
    auto handles = instance.physical_devices();
    std::vector<std::unique_ptr<vulkan::PhysicalDevice>> physical_devices;
    for (auto handle : handles) {
        physical_devices.emplace_back(std::make_unique<vulkan::PhysicalDevice>(handle));
    }

    std::vector<VkSurfaceFormatKHR> candidates(1);
    candidates[0].format = VK_FORMAT_B8G8R8A8_SRGB;
    candidates[0].colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;

    std::unique_ptr<vulkan::PhysicalDevice> ok_physical_device;
    for (auto& physical_device : physical_devices) {
        auto [format, result] = supported_format(*physical_device, surface, candidates);
        if (!result) {
            // Physical device must support any of requested formats.
            continue;
        }

        auto families = queue_families(*physical_device, surface);
        if (families.empty()) {
            // Physical device must have queue families that support required support.
            continue;
        }

        auto const& features = physical_device->features();
        if (!features.samplerAnisotropy || !features.sampleRateShading) {
            // Physical device must support required features.
            continue;
        }

        ok_physical_device = std::move(physical_device);
        surface.set_format(format);
        break;
    }

    if (!ok_physical_device) {
        throw Exception{"Failed to find suitable physical device."};
    }

    return ok_physical_device;
}

VkFormat choose_format(
    vulkan::PhysicalDevice const& physical_device,
    std::vector<VkFormat> const& candidates,
    VkImageTiling tiling,
    VkFormatFeatureFlags features
) noexcept
{
    for (auto format : candidates)
    {
        auto properties = physical_device.format_properties(format);

        if (tiling == VK_IMAGE_TILING_LINEAR && (properties.linearTilingFeatures & features) == features) {
            return format;
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (properties.optimalTilingFeatures & features) == features) {
            return format;
        }
    }

    return VK_FORMAT_UNDEFINED;
}

VkFormat default_depth_format(vulkan::PhysicalDevice const& physical_device) noexcept
{
    std::vector<VkFormat> candidates{
        VK_FORMAT_D32_SFLOAT,
        VK_FORMAT_D32_SFLOAT_S8_UINT,
        VK_FORMAT_D24_UNORM_S8_UINT
    };

    return choose_format(physical_device, candidates, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}





// ==================================================
// Device creation
// ==================================================

std::unique_ptr<vulkan::Device> create_device(
    vulkan::Instance const& instance,
    vulkan::Surface const& surface,
    vulkan::PhysicalDevice const& physical_device
)
{
    auto families = queue_families(physical_device, surface);
    if (families.empty()) {
        throw Exception{"Failed to get queue family indices."};
    }

    std::set<uint32_t> unique_indices;
    for (auto const& family : families) {
        unique_indices.insert(family.index());
    }

    std::vector<uint32_t> indices{unique_indices.begin(), unique_indices.end()};

    auto const& validation_layers = instance.validation_layers();

    std::vector<gsl::czstring> extensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        "VK_EXT_memory_budget"
    };

    VkPhysicalDeviceFeatures features = {};
    features.samplerAnisotropy = VK_TRUE;
    features.sampleRateShading = VK_TRUE;

    return std::make_unique<vulkan::Device>(
        physical_device.handle(),
        indices,
        validation_layers,
        extensions,
        features
    );
}





// ==================================================
// Queue creation
// ==================================================

std::pair<vulkan::Queue, vulkan::Queue> create_queues(
    vulkan::Surface const& surface,
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Device const& device
)
{
    auto families = queue_families(physical_device, surface);
    if (families.empty()) {
        throw Exception{"Failed to get physical device queue families."};
    }

    vulkan::Queue graphics_queue;
    vulkan::Queue present_queue;

    for (auto const& family : families) {
        if (family.graphics_bit()) {
            graphics_queue = vulkan::Queue{device.queue(family.index()), family.index()};
        }
        if (family.surface_bit()) {
            present_queue = vulkan::Queue{device.queue(family.index()), family.index()};
        }
    }

    return std::make_pair(graphics_queue, present_queue);
}





// ==================================================
// Swapchain creation
// ==================================================

std::pair<VkPresentModeKHR, bool> choose_present_mode(
    std::vector<VkPresentModeKHR> const& available,
    std::vector<VkPresentModeKHR> const& candidates
) noexcept
{
    for (auto can : candidates) {
        for (auto ava : available) {
            if (can == ava) {
                return std::make_pair(can, true);
            }
        }
    }

    return std::make_pair(VK_PRESENT_MODE_FIFO_KHR, false);
}

uint32_t choose_image_count(
    VkPresentModeKHR present_mode,
    VkSurfaceCapabilitiesKHR const& cap
)
{
    uint32_t count = 0;
    switch (present_mode) {
        case VK_PRESENT_MODE_MAILBOX_KHR:
            count = 3;
            break;
        case VK_PRESENT_MODE_IMMEDIATE_KHR:
            count = 1;
            break;
        case VK_PRESENT_MODE_FIFO_KHR:
            count = 2;
            break;
        case VK_PRESENT_MODE_FIFO_RELAXED_KHR:
            count = 2;
            break;
        default:
            throw Exception{"Present mode is not supported."};
    }

    if (count < cap.minImageCount + 1) {
        count = cap.minImageCount + 1;
    }

    if (cap.maxImageCount > 0 && cap.maxImageCount < count) {
        count = cap.maxImageCount;
    }

    return count;
}

VkExtent2D choose_image_extent(
    VkSurfaceCapabilitiesKHR const& cap,
    int width,
    int height
)  noexcept
{
    if (cap.currentExtent.width != UINT32_MAX) {
        return cap.currentExtent;
    }

    VkExtent2D extent;
    extent.width = glm::clamp(static_cast<uint32_t>(width), cap.minImageExtent.width, cap.maxImageExtent.width);
    extent.height = glm::clamp(static_cast<uint32_t>(height), cap.minImageExtent.height, cap.maxImageExtent.height);

    return extent;
}

std::unique_ptr<vulkan::Swapchain> create_swapchain(
    WindowImpl const& window,
    vulkan::Surface const& surface,
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Device const& device
)
{
    bool result;

    std::vector<VkPresentModeKHR> suitable_present_modes = {
        VK_PRESENT_MODE_MAILBOX_KHR,
        VK_PRESENT_MODE_IMMEDIATE_KHR,
        VK_PRESENT_MODE_FIFO_KHR
    };

    auto [width, height] = window.extent();
    auto surface_format = surface.format();
    auto h_surface = surface.handle();

    auto present_modes = physical_device.present_modes(h_surface);
    auto capabilities = physical_device.capabilities(h_surface);

    VkPresentModeKHR present_mode;
    std::tie(present_mode, result) = choose_present_mode(present_modes, suitable_present_modes);
    if (!result) {
        throw Exception{"Vulkan can not find suitable swapchain present mode."};
    }

    auto image_count = choose_image_count(present_mode, capabilities);
    auto image_extent = choose_image_extent(capabilities, width, height);

    auto families = queue_families(physical_device, surface);
    if (families.empty()) {
        throw Exception{"Failed to get physical device queue families."};
    }

    std::set<uint32_t> unique_indices;
    for (auto const& family : families) {
        unique_indices.insert(family.index());
    }

    std::vector<uint32_t> indices{unique_indices.begin(), unique_indices.end()};

    return std::make_unique<vulkan::Swapchain>(
        device.handle(),
        h_surface,
        capabilities.currentTransform,
        image_count,
        surface_format.format,
        surface_format.colorSpace,
        image_extent,
        indices,
        present_mode
    );
}
