#include "vertex.hpp"

std::vector<graphics::VertexComponent> Vertex::components() noexcept
{
    std::vector<graphics::VertexComponent> components(3);
    components[0] = graphics::VertexComponent::position;
    components[1] = graphics::VertexComponent::color;
    components[2] = graphics::VertexComponent::texcoord;

    return components;
}

std::vector<VkVertexInputBindingDescription> Vertex::bindings() noexcept
{
    std::vector<VkVertexInputBindingDescription> bindings(1);
    bindings[0].binding = 0;
    bindings[0].stride = 32;
    bindings[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    return bindings;
}

std::vector<VkVertexInputAttributeDescription> Vertex::attributes() noexcept
{
    std::vector<VkVertexInputAttributeDescription> attributes(3);
    attributes[0].binding = 0;
    attributes[0].location = 0;
    attributes[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributes[0].offset = 0;
    attributes[1].binding = 0;
    attributes[1].location = 1;
    attributes[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributes[1].offset = 12;
    attributes[2].binding = 0;
    attributes[2].location = 2;
    attributes[2].format = VK_FORMAT_R32G32_SFLOAT;
    attributes[2].offset = 24;

    return attributes;
}
