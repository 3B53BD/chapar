#include "render_pass.hpp"

#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/framebuffer.hpp"
#include <chapar/vulkan/physical_device.hpp>
#include <chapar/vulkan/render_pass.hpp>
#include "chapar/vulkan/swapchain.hpp"





std::unique_ptr<vulkan::RenderPass> create_render_pass(
    vulkan::Device const& device,
    vulkan::Surface const& surface,
    vulkan::Swapchain const& swapchain,
    VkSampleCountFlagBits samples,
    VkFormat depth_format
)
{
    auto const& format = swapchain.image_format();

    std::vector<VkAttachmentDescription> ads(3);

    // Swapchain attachment
    ads[0].format = format.format;
    ads[0].samples = VK_SAMPLE_COUNT_1_BIT;
    ads[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    ads[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    ads[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    ads[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    ads[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    ads[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    // Color attachment
    ads[1].format = format.format;
    ads[1].samples = samples;
    ads[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    ads[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    ads[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    ads[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    ads[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    ads[1].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    // Depth attachment
    ads[2].format = depth_format;
    ads[2].samples = samples;
    ads[2].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    ads[2].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    ads[2].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    ads[2].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    ads[2].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    ads[2].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    // // ImGui attachment
    // ads[3].format = format.format;
    // ads[3].samples = VK_SAMPLE_COUNT_1_BIT;
    // ads[3].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    // ads[3].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    // ads[3].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    // ads[3].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    // ads[3].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    // ads[3].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    std::vector<vulkan::SubpassDescription> descs(2);

    // Scene description
    descs[0].color.push_back({1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL});
    descs[0].resolve.push_back({0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL});
    descs[0].depth = {2, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL};

    // ImGui description
    descs[1].color.push_back({0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL});
    // descs[1].color.push_back({3, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL});

    // // ImGui to Scene description
    // descs[2].input.push_back({3, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL});
    // descs[2].color.push_back({0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL});

    std::vector<VkSubpassDescription> descriptions(2);
    descriptions[0] = subpass_description(descs[0]);
    descriptions[1] = subpass_description(descs[1]);
    // descriptions[2] = subpass_description(descs[2]);

    std::vector<VkSubpassDependency> deps(2);

    // External (none) <- Scene (write) dependency
    deps[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    deps[0].dstSubpass = 0;
    deps[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    deps[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    deps[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    deps[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    deps[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    // // External (none) <- ImGui (write) dependency
    // deps[1].srcSubpass = VK_SUBPASS_EXTERNAL;
    // deps[1].dstSubpass = 0;
    // deps[1].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    // deps[1].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    // deps[1].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    // deps[1].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    // deps[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
    //
    // // ImGui (write) <- ImGui (input) dependency
    // deps[2].srcSubpass = 0;
    // deps[2].dstSubpass = VK_SUBPASS_EXTERNAL;
    // deps[2].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    // deps[2].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    // deps[2].srcAccessMask =  VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    // deps[2].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    // deps[2].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    // Scene (write) <- ImGui (input) dependency
    deps[1].srcSubpass = 0;
    deps[1].dstSubpass = 1;
    deps[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    deps[1].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    // deps[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    deps[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    // deps[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    deps[1].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    // deps[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    deps[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    return std::make_unique<vulkan::RenderPass>(device.handle(), ads, descriptions, deps);
}





void create_framebuffers(
    vulkan::PhysicalDevice const& physical_device,
    vulkan::Device const& device,
    vulkan::Allocator const& allocator,
    vulkan::Swapchain const& swapchain,
    vulkan::RenderPass const& render_pass,
    VkSampleCountFlagBits samples,
    VkFormat depth_format,
    std::vector<std::unique_ptr<vulkan::Attachment>>& attachments,
    std::vector<VkClearValue>& clear_values,
    std::vector<std::unique_ptr<vulkan::Framebuffer>>& framebuffers
)
{
    auto const& images = swapchain.images();
    auto const& format = swapchain.image_format();
    auto const& extent = swapchain.image_extent();

    std::size_t attachment_count = 3;
    attachments.resize(images.size() * attachment_count);
    clear_values.resize(attachment_count);
    framebuffers.resize(images.size());

    for (gsl::index i = 0; i < images.size(); ++i) {
        std::vector<VkImageView> image_views;

        // Swapchain attachment
        auto& swapchain_att = attachments[i * attachment_count + 0];
        swapchain_att = std::make_unique<vulkan::SwapchainAttachment>(device.handle(), images[i], format.format);
        image_views.push_back(swapchain_att->image_view());

        // Color attachment
        auto ici = vulkan::Image::create_info();
        ici.format = format.format;
        ici.extent.width = extent.width;
        ici.extent.height = extent.height;
        ici.samples = samples;
        ici.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        auto& color_att = attachments[i * attachment_count + 1];
        color_att = std::make_unique<vulkan::ColorAttachment>(device.handle(), allocator.handle(), ici);
        image_views.push_back(color_att->image_view());

        // Depth attachment
        ici = vulkan::Image::create_info();
        ici.format = depth_format;
        ici.extent.width = extent.width;
        ici.extent.height = extent.height;
        ici.samples = samples;
        ici.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        auto& depth_att = attachments[i * attachment_count + 2];
        depth_att = std::make_unique<vulkan::DepthAttachment>(device.handle(), allocator.handle(), ici);
        image_views.push_back(depth_att->image_view());

        // // ImGui attachment
        // ici = vulkan::Image::create_info();
        // ici.format = format.format;
        // ici.extent.width = extent.width;
        // ici.extent.height = extent.height;
        // ici.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
        // auto& imgui_att = attachments[i * attachment_count + 3];
        // imgui_att = std::make_unique<vulkan::ColorAttachment>(device.handle(), allocator.handle(), ici);
        // image_views.push_back(imgui_att->image_view());

        framebuffers[i] = std::make_unique<vulkan::Framebuffer>(device.handle(), render_pass.handle(), image_views, extent.width, extent.height);
    }

    clear_values[0] = {{0.0f, 0.0f, 0.0f, 1.0f}};
    clear_values[1] = {{0.0f, 0.0f, 0.0f, 1.0f}};
    clear_values[2] = {1.0f, 0};
    // clear_values[3] = {{0.0f, 0.0f, 0.0f, 1.0f}};
}
