#include "parse.hpp"
#include "vertex.hpp"

#include "chapar/core/error.hpp"
#include "chapar/graphics/port/tinygltf/scene.hpp"
#include "chapar/graphics/port/vulkan/scene.hpp"
#include "chapar/lua/state.hpp"

#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/buffer.hpp"
#include "chapar/vulkan/camera.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/descriptor.hpp"
#include "chapar/vulkan/pipeline.hpp"
#include "chapar/vulkan/queue.hpp"
#include "chapar/vulkan/scene.hpp"
#include "chapar/vulkan/swapchain.hpp"

#include <filesystem>

#include <iostream>

struct ModelInfo {
    uint32_t set = 0;
    VkDescriptorSetLayoutBinding binding;
    std::string filename;
};

std::vector<ModelInfo> parse(gsl::czstring filename);

std::unique_ptr<vulkan::Scene> create_scene(
    vulkan::Device const& device,
    vulkan::Allocator const& allocator,
    vulkan::Queue const& transfer_queue,
    vulkan::Swapchain const& swapchain,
    vulkan::CommandPool const& pool,

    std::filesystem::path filename,
    std::vector<graphics::VertexComponent> const& vertex_components
);

SceneInfo load_config(
    vulkan::Device const& device,
    vulkan::Allocator const& allocator,
    vulkan::Queue const& transfer_queue,
    vulkan::Swapchain const& swapchain,
    vulkan::CommandPool const& command_pool
)
{
    SceneInfo scene;

    auto models = parse("example/simple/script/config.lua");
    std::map<uint32_t, std::vector<VkDescriptorSetLayoutBinding>> binding_sets;
    for (auto const& model : models) {
        binding_sets[model.set].push_back(model.binding);
    }
    for (auto const& [set, bindings] : binding_sets) {
        scene.set_layouts.push_back(std::make_unique<vulkan::DescriptorSetLayout>(device.handle(), bindings));
    }

    // Creates scene pipeline layout
    std::vector<VkDescriptorSetLayout> vk_set_layouts = {scene.set_layouts[0]->handle(), scene.set_layouts[1]->handle()};
    auto layout_info = vulkan::PipelineLayout::create_info(vk_set_layouts);
    scene.pipeline_layout = std::make_unique<vulkan::PipelineLayout>(device.handle(), layout_info);



    // Creates scene
    scene.model = create_scene(device, allocator, transfer_queue, swapchain, command_pool, "example/simple/model/box.gltf", Vertex::components());
    scene.model->binding = 0;
    scene.model->describe(scene.set_layouts[1]->handle());



    // Creates camera
    auto image_count = swapchain.image_count();
    scene.camera = std::make_unique<vulkan::Camera>(device, allocator, image_count, scene.set_layouts[0]->handle());
    scene.camera->write(0);
    scene.camera->set_aspect(swapchain.image_extent());
    for (gsl::index i = 0; i < image_count; ++i) {
        scene.camera->update(i);
    }

    return scene;
}

std::vector<ModelInfo> parse(gsl::czstring filename)
{
    chapar::lua::State lua_state;
    int error;

    error = lua_state.dofile("include/lua/script/globals.lua");
    if (error) {
        throw chapar::Exception{"Failde to load lua globals."};
    }

    error = lua_state.dofile(filename);
    if (error) {
        std::cout << error << '\n';
        throw chapar::Exception{"Failed to load lua state."};
    }

    std::vector<ModelInfo> models;
    auto ch = lua_state["ch"];
    auto scene = lua_state["scene"];

    auto type_camera = ch["camera"].to_integer();
    auto type_model = ch["model"].to_integer();

    for (gsl::index i = 1; i <= scene.len(); ++i) {
        auto obj = scene[i];
        auto type = obj["type"].to_integer();
        if (type_camera == type) {
            ModelInfo info;
            info.set = obj["set"].to_integer();
            info.binding.binding = static_cast<uint32_t>(obj["binding"].to_integer());
            info.binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            info.binding.descriptorCount = 1;
            info.binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
            models.push_back(info);
        }
        else if (type_model == type) {
            ModelInfo info;
            info.set = obj["set"].to_integer();
            info.binding.binding = static_cast<uint32_t>(obj["binding"].to_integer());
            info.binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            info.binding.descriptorCount = 1;
            info.binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
            info.filename = obj["filename"].to_string();
            models.push_back(info);
        }
    }

    return models;
}


std::unique_ptr<vulkan::Scene> create_scene(
    vulkan::Device const& device,
    vulkan::Allocator const& allocator,
    vulkan::Queue const& transfer_queue,
    vulkan::Swapchain const& swapchain,
    vulkan::CommandPool const& pool,

    std::filesystem::path filename,
    std::vector<graphics::VertexComponent> const& vertex_components
)
{
    std::unique_ptr<vulkan::Scene> scene;

    if (filename.extension() == ".gltf") {
        auto tiny_scene = graphics::tinygltf_port::load_scene(filename.c_str(), vertex_components);

        graphics::vulkan_port::SceneCreateInfo ci = {};
        ci.data = &tiny_scene;
        ci.device = device.handle();
        ci.allocator = allocator.handle();
        ci.transfer_queue = &transfer_queue;
        ci.image_count = swapchain.image_count();
        ci.pool = &pool;

        scene = create_scene(ci);
    }
    else {
        throw Exception{"Wrong extension for scene file."};
    }

    return scene;
}
