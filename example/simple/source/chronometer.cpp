#include "chronometer.hpp"

Chronometer::Chronometer() noexcept
{
    start_time_ = clock::now();
}

void Chronometer::reset() noexcept
{
    start_time_ = clock::now();
}

double Chronometer::elapsed() const noexcept
{
    using namespace std::chrono;
    return duration_cast<ms>(clock::now() - start_time_).count();
}
