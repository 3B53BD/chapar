#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UBO {
    mat4 model;
} ubo;

layout(set = 1, binding = 0) uniform Camera {
    mat4 view;
    mat4 proj;
} cam;

layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec2 in_uv;

layout(location = 0) out vec4 out_color;
layout(location = 1) out vec2 out_uv;

void main() {
    gl_Position = cam.proj * cam.view * ubo.model * vec4(in_pos, 1.0);
    out_color = vec4(in_color, 1.0);
    out_uv = in_uv;
}
