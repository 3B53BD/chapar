cmake_minimum_required(VERSION 3.18)
project(chapar VERSION 0.1.0 LANGUAGES CXX)

if (PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
    message("##################################################")
    message("Error in ${PROJECT_NAME}: in-source build is disabled.")
    message("Make directory (like __build__) and run CMake from inside it.")
    message("##################################################")
    message(FATAL_ERROR "Quiting.")
endif()



# ==================================================
#                     Includes
# ==================================================

include(FindPkgConfig)
include(GNUInstallDirs)
include(GenerateExportHeader)
include(CMakePackageConfigHelpers)

if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    include(CTest)
    enable_testing()
endif()

# find_package(doctest REQUIRED)
find_package(fmt REQUIRED)
find_package(glm REQUIRED)
find_package(gsl-lite REQUIRED)
# find_package(Ktx REQUIRED)
find_package(SDL2 REQUIRED)
find_package(TinyGLTF REQUIRED)
pkg_check_modules(vulkan REQUIRED vulkan)



# ==================================================
#                     Options
# ==================================================

option(CMAKE_EXPORT_COMPILE_COMMANDS "" ON)

option(BUILD_SHARED_LIBS "Build shared libraries instead static" ON)
# set(CMAKE_CXX_VISIBILITY_PRESET hidden)
# set(CMAKE_VISIBILITY_INLINES_HIDDEN YES)

set(CHAPAR_WERROR YES)
include(cmake/warnings.cmake)

# Set output directories
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../bin)
set( FAKE_INSTALL_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../fin)
set(      PACKAGE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../pkg)
set(       SHADER_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../shd)

option(CHAPAR_BUILD_EXAMPLES "" ON)



# ==================================================
#                  Subdirectories
# ==================================================

add_subdirectory(external)
# add_subdirectory(lib)
add_subdirectory(source)

if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME AND BUILD_TESTING)
    add_subdirectory(test)
endif()

if(CHAPAR_BUILD_EXAMPLES)
    add_subdirectory(example)
endif()



add_custom_target(fake_install
    ALL
    ${CMAKE_COMMAND}
    -D CMAKE_INSTALL_PREFIX:string=${FAKE_INSTALL_OUTPUT_DIRECTORY}
    -P ${CMAKE_BINARY_DIR}/cmake_install.cmake
    DEPENDS
        chapar::chapar
        chapar::stb_image
        chapar::stb_image_write
        chapar::TinyGLTF
        chapar::vma
)
