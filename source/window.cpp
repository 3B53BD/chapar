#include "chapar/window.hpp"

namespace chapar {

    ApplicationWindow::ApplicationWindow(std::unique_ptr<WindowImpl> window)
    {
        impl_ = std::move(window);
    }
}
