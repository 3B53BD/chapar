#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/instance.hpp"
#include "chapar/vulkan/physical_device.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/core/error.hpp"

namespace chapar::vulkan {

    VmaAllocationCreateInfo Allocator::allocation_create_info() noexcept
    {
        VmaAllocationCreateInfo info = {};
        info.flags = {};
        info.usage = {};
        info.requiredFlags = {};
        info.preferredFlags = {};
        info.memoryTypeBits = {};
        info.pool = {};
        info.pUserData = {};

        return info;
    }

    Allocator::Allocator(VkInstance instance, VkPhysicalDevice physical_device, VkDevice device)
    {
        VmaAllocatorCreateInfo info = {};
        info.vulkanApiVersion = VK_API_VERSION_1_2;
        info.instance = instance;
        info.physicalDevice = physical_device;
        info.device = device;

        VkResult result = vmaCreateAllocator(&info, &allocator_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vma failed to create allocator."};
        }
    }

    Allocator::~Allocator() noexcept
    {
        if (allocator_ == VK_NULL_HANDLE) return;
        vmaDestroyAllocator(allocator_);
    }
}
