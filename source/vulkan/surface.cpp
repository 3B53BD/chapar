#include "chapar/vulkan/surface.hpp"
#include "chapar/vulkan/instance.hpp"
#include "chapar/sdl/window.hpp"

namespace chapar::vulkan {

    Surface::Surface(VkInstance instance, VkSurfaceKHR surface)
    : surface_{surface}
    , instance_{instance}
    { }

    Surface::~Surface() noexcept
    {
        if (surface_ == VK_NULL_HANDLE) return;
        vkDestroySurfaceKHR(instance_, surface_, nullptr);
    }

    void Surface::set_format(VkSurfaceFormatKHR format) noexcept
    {
        format_ = format;
    }

    VkSurfaceFormatKHR const& Surface::format() const noexcept
    {
        return format_;
    }
}
