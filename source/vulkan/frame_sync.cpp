#include "chapar/vulkan/frame_sync.hpp"

namespace chapar::vulkan {

    FrameSync::FrameSync(VkDevice device, std::size_t parallel_frames) noexcept
    : device_{device}
    {
        VkSemaphoreCreateInfo sci = {};
        sci.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        sci.pNext = {};
        sci.flags = {};

        VkFenceCreateInfo fci = {};
        fci.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fci.pNext = {};
        fci.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        frames_.resize(parallel_frames);
        for (auto& frame : frames_) {
            vkCreateFence(device_, &fci, nullptr, &frame.idle);
            vkCreateSemaphore(device_, &sci, nullptr, &frame.aquired);
            vkCreateSemaphore(device_, &sci, nullptr, &frame.rendered);
        }
    }

    FrameSync::~FrameSync() noexcept
    {
        if (device_ == VK_NULL_HANDLE) return;

        for (auto& frame : frames_) {
            vkDestroyFence(device_, frame.idle, nullptr);
            vkDestroySemaphore(device_, frame.aquired, nullptr);
            vkDestroySemaphore(device_, frame.rendered, nullptr);
        }
    }

    std::pair<Frame, VkResult> FrameSync::wait_idle() noexcept
    {
        VkResult result = vkWaitForFences(device_, 1, &frames_[current_].idle, VK_TRUE, UINT64_MAX);

        return std::make_pair(frames_[current_], result);
    }

    void FrameSync::advance() noexcept
    {
        current_ = (current_ + 1) % frames_.size();
    }
}
