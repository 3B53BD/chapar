#include "chapar/vulkan/instance.hpp"
#include "chapar/core/error.hpp"
#include "chapar/sdl/window.hpp"
#include <iostream>
#include <string>
#include <cstring>

using namespace std::literals::string_literals;

namespace chapar::vulkan {

    VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
        VkDebugUtilsMessageSeverityFlagBitsEXT,
        VkDebugUtilsMessageTypeFlagsEXT,
        VkDebugUtilsMessengerCallbackDataEXT const* data,
        void*
    )
    {
        std::cerr << "#### validation layer ####\n" << data->pMessage << "\n\n";

        return VK_FALSE;
    }

    std::vector<VkLayerProperties> Instance::layer_properties()
    {
        uint32_t count;
        vkEnumerateInstanceLayerProperties(&count, nullptr);
        std::vector<VkLayerProperties> properties(count);
        vkEnumerateInstanceLayerProperties(&count, properties.data());

        return properties;
    }

    Instance::Instance(
        std::vector<gsl::czstring> const& validation_layers,
        std::vector<gsl::czstring> const& enabled_extensions,
        bool debug
    )
    : validation_layers_{validation_layers}
    , extensions_{enabled_extensions}
    {
        VkResult result;

        VkApplicationInfo app = {};
        app.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app.pNext = {};
        app.pApplicationName = "Manuka";
        app.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
        app.pEngineName = "No Engine";
        app.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        app.apiVersion = VK_API_VERSION_1_2;

        VkInstanceCreateInfo ici = {};
        ici.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        ici.flags = {};
        ici.pApplicationInfo = &app;
        ici.enabledLayerCount = static_cast<uint32_t>(validation_layers_.size());
        ici.ppEnabledLayerNames = validation_layers_.data();
        ici.enabledExtensionCount = static_cast<uint32_t>(extensions_.size());
        ici.ppEnabledExtensionNames = extensions_.data();

        std::unique_ptr<VkDebugUtilsMessengerCreateInfoEXT> dci;
        if (debug) {
            dci = std::make_unique<VkDebugUtilsMessengerCreateInfoEXT>();
            dci->sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
            dci->messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
            dci->messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
            dci->pfnUserCallback = debug_callback;

            ici.pNext = dci.get();
        }

        result = vkCreateInstance(&ici, nullptr, &instance_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create instance."};
        }

        if (debug) {
            auto func = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance_, "vkCreateDebugUtilsMessengerEXT"));
            if (!func) {
                throw Exception{"Vulkan failed to get address of debug utils messenger ext."};
            }

            result = func(instance_, dci.get(), nullptr, &debug_messenger_);
            if (result != VK_SUCCESS) {
                throw Exception{"Vulkan failed to create debug utils messenger ext."};
            }
        }
    }

    Instance::~Instance() noexcept
    {
        if (instance_ == VK_NULL_HANDLE) return;

        if (debug_messenger_ != VK_NULL_HANDLE) {
            auto func = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance_, "vkDestroyDebugUtilsMessengerEXT" ));
            if (func) {
                func(instance_, debug_messenger_, nullptr);
            }
        }

        vkDestroyInstance(instance_, nullptr);
    }

    std::vector<VkPhysicalDevice> Instance::physical_devices() const noexcept
    {
        uint32_t count;
        vkEnumeratePhysicalDevices(instance_, &count, nullptr);
        std::vector<VkPhysicalDevice> devices(count);
        vkEnumeratePhysicalDevices(instance_, &count, devices.data());

        return devices;
    }

    std::vector<gsl::czstring> const& Instance::validation_layers() const noexcept
    {
        return validation_layers_;
    }

    std::vector<gsl::czstring> const& Instance::enabled_extensions() const noexcept
    {
        return extensions_;
    }
}
