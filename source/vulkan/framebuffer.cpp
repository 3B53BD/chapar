#include "chapar/vulkan/framebuffer.hpp"
#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/image.hpp"
#include "chapar/core/error.hpp"

namespace chapar::vulkan {

    SwapchainAttachment::SwapchainAttachment(
        VkDevice device,
        VkImage image,
        VkFormat format
    )
    {
        device_ = device;

        VkImageViewCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.image = image;
        info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        info.format = format;
        info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        info.subresourceRange.baseMipLevel = 0;
        info.subresourceRange.levelCount = 1;
        info.subresourceRange.baseArrayLayer = 0;
        info.subresourceRange.layerCount = 1;

        VkResult result = vkCreateImageView(device_, &info, nullptr, &image_view_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create image view."};
        }
    }

    SwapchainAttachment::~SwapchainAttachment() noexcept
    {
        vkDestroyImageView(device_, image_view_, nullptr);
    }

    VkDescriptorImageInfo SwapchainAttachment::descriptor() const noexcept
    {
        VkDescriptorImageInfo info = {};
        info.sampler = VK_NULL_HANDLE;
        info.imageView = image_view_;
        info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        return info;
    }





    ColorAttachment::ColorAttachment(
        VkDevice device,
        VmaAllocator allocator,
        VkImageCreateInfo ici
    )
    {
        device_ = device;
        allocator_ = allocator;

        ici.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        auto aci = Allocator::allocation_create_info();
        aci.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        VkResult ir = vmaCreateImage(allocator_, &ici, &aci, &image_, &allocation_, nullptr);
        if (ir != VK_SUCCESS) {
            throw Exception{"Vma failed to create image."};
        }

        VkImageViewCreateInfo ivci = {};
        ivci.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        ivci.pNext = {};
        ivci.flags = {};
        ivci.image = image_;
        ivci.viewType = VK_IMAGE_VIEW_TYPE_2D;
        ivci.format = ici.format;
        ivci.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        ivci.subresourceRange.baseMipLevel = 0;
        ivci.subresourceRange.levelCount = 1;
        ivci.subresourceRange.baseArrayLayer = 0;
        ivci.subresourceRange.layerCount = 1;

        VkResult ivr = vkCreateImageView(device_, &ivci, nullptr, &image_view_);
        if (ivr != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create image view."};
        }
    }

    ColorAttachment::~ColorAttachment() noexcept
    {
        vkDestroyImageView(device_, image_view_, nullptr);
        vmaDestroyImage(allocator_, image_, allocation_);
    }

    VkDescriptorImageInfo ColorAttachment::descriptor() const noexcept
    {
        VkDescriptorImageInfo info = {};
        info.sampler = VK_NULL_HANDLE;
        info.imageView = image_view_;
        info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        return info;
    }





    DepthAttachment::DepthAttachment(
        VkDevice device,
        VmaAllocator allocator,
        VkImageCreateInfo ici
    )
    {
        device_ = device;
        allocator_ = allocator;

        ici.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

        auto aci = Allocator::allocation_create_info();
        aci.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        VkResult ir = vmaCreateImage(allocator_, &ici, &aci, &image_, &allocation_, nullptr);
        if (ir != VK_SUCCESS) {
            throw Exception{"Vma failed to create image."};
        }

        VkImageViewCreateInfo ivci = {};
        ivci.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        ivci.pNext = {};
        ivci.flags = {};
        ivci.image = image_;
        ivci.viewType = VK_IMAGE_VIEW_TYPE_2D;
        ivci.format = ici.format;
        ivci.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        ivci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        ivci.subresourceRange.baseMipLevel = 0;
        ivci.subresourceRange.levelCount = 1;
        ivci.subresourceRange.baseArrayLayer = 0;
        ivci.subresourceRange.layerCount = 1;

        VkResult ivr = vkCreateImageView(device_, &ivci, nullptr, &image_view_);
        if (ivr != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create image view."};
        }
    }

    DepthAttachment::~DepthAttachment() noexcept
    {
        vkDestroyImageView(device_, image_view_, nullptr);
        vmaDestroyImage(allocator_, image_, allocation_);
    }

    VkDescriptorImageInfo DepthAttachment::descriptor() const noexcept
    {
        VkDescriptorImageInfo info = {};
        info.sampler = VK_NULL_HANDLE;
        info.imageView = image_view_;
        info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        return info;
    }





    Framebuffer::Framebuffer(
        VkDevice device,
        VkRenderPass render_pass,
        std::vector<VkImageView> image_views,
        uint32_t width,
        uint32_t height
    )
    {
        device_ = device;

        VkFramebufferCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.renderPass = render_pass;
        info.attachmentCount = static_cast<uint32_t>(image_views.size());
        info.pAttachments = image_views.data();
        info.width = width;
        info.height = height;
        info.layers = 1;

        VkResult result = vkCreateFramebuffer(device_, &info, nullptr, &framebuffer_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create framebuffer."};
        }
    }

    Framebuffer::~Framebuffer() noexcept
    {
        if (framebuffer_ == VK_NULL_HANDLE) return;
        vkDestroyFramebuffer(device_, framebuffer_, nullptr);
    }
}
