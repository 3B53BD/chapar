#include "chapar/vulkan/image.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/core/error.hpp"
#include "image_memory_barriers.hpp"

namespace chapar::vulkan {

    // ==================================================
    // Image
    // ==================================================

    VkImageCreateInfo Image::create_info() noexcept
    {
        VkImageCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.imageType = VK_IMAGE_TYPE_2D;
        info.format = VK_FORMAT_UNDEFINED;
        info.extent = {1, 1, 1};
        info.mipLevels = 1;
        info.arrayLayers = 1;
        info.samples = VK_SAMPLE_COUNT_1_BIT;
        info.tiling = VK_IMAGE_TILING_OPTIMAL;
        info.usage = {};
        info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        info.queueFamilyIndexCount = {};
        info.pQueueFamilyIndices = {};
        info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        return info;
    }

    Image::Image(
        VmaAllocator allocator,
        VkImageCreateInfo const& image_create_info,
        VmaAllocationCreateInfo const& allocation_create_info
    )
    : allocator_{allocator}
    , layout_{image_create_info.initialLayout}
    , usage_{image_create_info.usage}
    , format_{image_create_info.format}
    , extent_{image_create_info.extent}
    , mip_levels_{image_create_info.mipLevels}
    {
        VkResult result = vmaCreateImage(allocator_, &image_create_info, &allocation_create_info, &image_, &allocation_, &allocation_info_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vma failed to create image."};
        }
    }

    Image::~Image() noexcept
    {
        if (image_ == VK_NULL_HANDLE) return;
        vmaDestroyImage(allocator_, image_, allocation_);
    }

    void Image::transition_layout(
        CommandBuffer const& cmd_buffer,
        VkImageLayout to_layout
    ) noexcept
    {
        std::vector<VkImageMemoryBarrier> barriers(1);
        barriers[0].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barriers[0].pNext = {};
        barriers[0].oldLayout = layout_;
        barriers[0].newLayout = to_layout;
        barriers[0].srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barriers[0].dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barriers[0].image = image_;
        barriers[0].subresourceRange.baseMipLevel = 0;
        barriers[0].subresourceRange.levelCount = mip_levels_;
        barriers[0].subresourceRange.baseArrayLayer = 0;
        barriers[0].subresourceRange.layerCount = 1;

        if (to_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
            barriers[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            if (format_ == VK_FORMAT_D32_SFLOAT_S8_UINT || format_ == VK_FORMAT_D24_UNORM_S8_UINT) {
                barriers[0].subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
            }
        } else {
            barriers[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        }

        auto src_access_masks = details::src_access_mask_table();
        barriers[0].srcAccessMask = (*src_access_masks[layout_])();

        auto dst_access_masks = details::dst_access_mask_table(barriers[0]);
        barriers[0].dstAccessMask = (*dst_access_masks[to_layout])();

        VkPipelineStageFlags src_stage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
        VkPipelineStageFlags dst_stage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

        cmd_buffer.pipeline_barrier(src_stage, dst_stage, barriers);
        layout_ = to_layout;
    }

    VkImageLayout Image::layout() const noexcept
    {
        return layout_;
    }
    VkImageUsageFlags Image::usage() const noexcept
    {
        return usage_;
    }
    VkFormat Image::format() const noexcept
    {
        return format_;
    }
    VkExtent3D const& Image::extent() const noexcept
    {
        return extent_;
    }
    uint32_t Image::mip_levels() const noexcept
    {
        return mip_levels_;
    }





    // ==================================================
    // ImageView
    // ==================================================

    ImageView::ImageView(
        VkDevice device,
        VkImage image,
        VkFormat format,
        uint32_t mip_levels,
        VkImageAspectFlags aspect_mask
    )
    : device_{device}
    {
        create(image, format, aspect_mask, mip_levels);
    }

    ImageView::ImageView(
        VkDevice device,
        Image const& image,
        VkImageAspectFlags aspect_mask
    )
    : device_{device}
    {
        create(image.handle(), image.format(), aspect_mask, image.mip_levels());
    }

    ImageView::~ImageView() noexcept
    {
        if (view_ == VK_NULL_HANDLE) return;
        vkDestroyImageView(device_, view_, nullptr);
    }

    void ImageView::create(
        VkImage image,
        VkFormat format,
        VkImageAspectFlags aspect_mask,
        uint32_t mip_levels
    )
    {
        VkImageViewCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.image = image;
        info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        info.format = format;
        info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.subresourceRange.aspectMask = aspect_mask;
        info.subresourceRange.baseMipLevel = 0;
        info.subresourceRange.levelCount = mip_levels;
        info.subresourceRange.baseArrayLayer = 0;
        info.subresourceRange.layerCount = 1;

        VkResult result = vkCreateImageView(device_, &info, nullptr, &view_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create image view."};
        }
    }





    // ==================================================
    // Sampler
    // ==================================================

    VkSamplerCreateInfo Sampler::create_info() noexcept
    {
        VkSamplerCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.magFilter = VK_FILTER_LINEAR;
        info.minFilter = VK_FILTER_LINEAR;
        info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        info.mipLodBias = 0.0f;
        info.anisotropyEnable = VK_TRUE;
        info.maxAnisotropy = 16.0f;
        info.compareEnable = VK_FALSE;
        info.compareOp = VK_COMPARE_OP_ALWAYS;
        info.minLod = 0;
        info.maxLod = 10;
        info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
        info.unnormalizedCoordinates = VK_FALSE;

        return info;
    }

    Sampler::Sampler(VkDevice device, VkSamplerCreateInfo const& info)
    : device_{device}
    {
        VkResult result = vkCreateSampler(device_, &info, nullptr, &sampler_);
        if (result != VK_NULL_HANDLE) {
            throw Exception{"Vulkan failed to create sampler."};
        }
    }

    Sampler::~Sampler() noexcept
    {
        if (sampler_ == VK_NULL_HANDLE) return;
        vkDestroySampler(device_, sampler_, nullptr);
    }
}
