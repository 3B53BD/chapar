#include "chapar/vulkan/buffer.hpp"
#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/queue.hpp"
#include "chapar/core/error.hpp"
#include <cstring>

namespace chapar::vulkan {

    // ==================================================
    // Buffer
    // ==================================================

    VkBufferCreateInfo Buffer::create_info() noexcept
    {
        VkBufferCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.size = {};
        info.usage = {};
        info.sharingMode = {};
        info.queueFamilyIndexCount = {};
        info.pQueueFamilyIndices = {};

        return info;
    }

    Buffer::Buffer(
        VmaAllocator allocator,
        VkBufferCreateInfo const& buffer_info,
        VmaAllocationCreateInfo const& allocator_info
    )
    : allocator_{allocator}
    {
        VkResult result = vmaCreateBuffer(allocator_, &buffer_info, &allocator_info, &buffer_, &allocation_, &allocation_info_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vma failed to create buffer."};
        }
    }

    Buffer::~Buffer() noexcept
    {
        if (allocator_ == VK_NULL_HANDLE) return;
        vmaDestroyBuffer(allocator_, buffer_, allocation_);
    }

    VkResult Buffer::map() noexcept
    {
        return vmaMapMemory(allocator_, allocation_, &mapped_data_);
    }

    void Buffer::unmap() noexcept
    {
        vmaUnmapMemory(allocator_, allocation_);
    }

    void* Buffer::memcpy(std::size_t offset, std::span<std::byte const> source) noexcept
    {
        auto data = static_cast<std::byte*>(mapped_data_);
        auto target = static_cast<void*>(data + offset);

        return std::memcpy(target, source.data(), source.size());
    }

    VkDeviceSize Buffer::size() const noexcept
    {
        return allocation_info_.size;
    }

    VkDescriptorBufferInfo Buffer::descriptor() const noexcept
    {
        VkDescriptorBufferInfo descriptor;
        descriptor.buffer = buffer_;
        descriptor.offset = 0;
        descriptor.range = allocation_info_.size;

        return descriptor;
    }





    // ==================================================
    // VertexBuffer
    // ==================================================

    VertexBuffer::VertexBuffer(
        VkDevice device,
        VmaAllocator allocator,
        Queue const& transfer_queue,
        std::vector<std::byte> const& vertices,
        uint32_t vertex_count
    )
    {
        vertex_count_ = vertex_count;

        // Creates staging buffer
        auto bci = Buffer::create_info();
        bci.size = vertices.size();
        bci.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        auto aci = Allocator::allocation_create_info();
        aci.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        auto staging = std::make_unique<Buffer>(allocator, bci, aci);

        // Creates device local buffer
        bci.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
        aci.usage = VMA_MEMORY_USAGE_GPU_ONLY;
        buffer_ = std::make_unique<Buffer>(allocator, bci, aci);

        // Copies data to staging buffer
        staging->map();
        staging->memcpy(0, vertices);
        staging->unmap();

        // Copies staging buffer to device local buffer
        auto submit = std::make_unique<vulkan::OneTimeSubmit>(device, transfer_queue);

        std::vector<VkBufferCopy> regions(1);
        regions[0].srcOffset = 0;
        regions[0].dstOffset = 0;
        regions[0].size = staging->size();

        auto cmd_buf = submit->command_buffer();
        cmd_buf->copy_to(*staging, *buffer_, regions);

        submit.reset();
    }

    void VertexBuffer::bind(VkCommandBuffer command_buffer) const noexcept
    {
        auto buffer = buffer_->handle();
        VkDeviceSize offset = 0;
        vkCmdBindVertexBuffers(
            command_buffer,
            0, // firstBinding
            1, // bindingCount
            &buffer, // pBuffers
            &offset // pOffsets
        );
    }

    void VertexBuffer::draw(VkCommandBuffer command_buffer) const noexcept
    {
        vkCmdDraw(
            command_buffer,
            vertex_count_, // vertexCount
            1, // instanceCount
            0, // firstVertex
            0  // firstInstance
        );
    }

    VkDeviceSize VertexBuffer::size() const noexcept
    {
        return buffer_->size();
    }

    VkDescriptorBufferInfo VertexBuffer::descriptor() const noexcept
    {
        return buffer_->descriptor();
    }

    VkBuffer VertexBuffer::handle() const noexcept
    {
        return buffer_->handle();
    }





    // ==================================================
    // IndexVertexBuffer
    // ==================================================

    IndexVertexBuffer::IndexVertexBuffer(
        VkDevice device,
        VmaAllocator allocator,
        Queue const& transfer_queue,
        std::vector<std::byte> const& vertices,
        std::vector<std::byte> const& indices,
        uint32_t index_count,
        VkIndexType index_type
    )
    {
        vertex_offset_ = indices.size();
        index_count_ = index_count;
        index_type_ = index_type;

        auto bci = Buffer::create_info();
        bci.size = indices.size() + vertices.size();
        bci.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        auto aci = Allocator::allocation_create_info();
        aci.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        auto staging = std::make_unique<Buffer>(allocator, bci, aci);

        bci.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
        aci.usage = VMA_MEMORY_USAGE_GPU_ONLY;
        buffer_ = std::make_unique<Buffer>(allocator, bci, aci);

        staging->map();
        staging->memcpy(0, indices);
        staging->memcpy(vertex_offset_, vertices);
        staging->unmap();

        auto submit = std::make_unique<vulkan::OneTimeSubmit>(device, transfer_queue);

        std::vector<VkBufferCopy> regions(1);
        regions[0].srcOffset = 0;
        regions[0].dstOffset = 0;
        regions[0].size = staging->size();

        auto cmd_buf = submit->command_buffer();
        cmd_buf->copy_to(*staging, *buffer_, regions);

        cmd_buf->copy_to(*staging, *buffer_, regions);
    }

    void IndexVertexBuffer::bind(VkCommandBuffer command_buffer) const noexcept
    {
        auto buffer = buffer_->handle();
        vkCmdBindVertexBuffers(
            command_buffer,
            0, // firstBinding
            1, // bindingCount
            &buffer, // pBuffers
            &vertex_offset_ // pOffsets
        );
        vkCmdBindIndexBuffer(
            command_buffer,
            buffer, // buffer
            0, // offset
            index_type_ // indexType
        );
    }

    void IndexVertexBuffer::draw(VkCommandBuffer command_buffer) const noexcept
    {
        vkCmdDrawIndexed(
            command_buffer,
            index_count_, // indexCount
            1, // instanceCount
            0, // firstIndex
            0, // vertexOffset
            0  // firstInstance
        );
    }

    VkDeviceSize IndexVertexBuffer::size() const noexcept
    {
        return buffer_->size();
    }

    VkDescriptorBufferInfo IndexVertexBuffer::descriptor() const noexcept
    {
        return buffer_->descriptor();
    }

    VkBuffer IndexVertexBuffer::handle() const noexcept
    {
        return buffer_->handle();
    }





    // ==================================================
    // UniformBuffer.
    // ==================================================

    UniformBuffer::UniformBuffer(VmaAllocator allocator, std::size_t size)
    {
        auto bci = Buffer::create_info();
        bci.size = size;
        bci.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

        auto aci = Allocator::allocation_create_info();
        aci.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

        buffer_ = std::make_unique<Buffer>(allocator, bci, aci);
    }

    VkResult UniformBuffer::map() noexcept
    {
        return buffer_->map();
    }

    void UniformBuffer::unmap() noexcept
    {
        buffer_->unmap();
    }

    void* UniformBuffer::memcpy(std::size_t offset, std::span<std::byte const> source) noexcept
    {
        return buffer_->memcpy(offset, source);
    }

    VkDeviceSize UniformBuffer::size() const noexcept
    {
        return buffer_->size();
    }

    VkDescriptorBufferInfo UniformBuffer::descriptor() const noexcept
    {
        return buffer_->descriptor();
    }

    VkBuffer UniformBuffer::handle() const noexcept
    {
        return buffer_->handle();
    }
}
