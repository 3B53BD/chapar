#include "chapar/vulkan/physical_device.hpp"

namespace chapar::vulkan {

    PhysicalDevice::PhysicalDevice(VkPhysicalDevice physical_device) noexcept
    : physical_device_{physical_device}
    {
        uint32_t count;
        vkEnumerateDeviceExtensionProperties(physical_device_, nullptr, &count, nullptr);
        extensions_.resize(count);
        vkEnumerateDeviceExtensionProperties(physical_device_, nullptr, &count, extensions_.data());

        vkGetPhysicalDeviceFeatures(physical_device_, &features_);
        vkGetPhysicalDeviceProperties(physical_device_, &properties_);
        vkGetPhysicalDeviceMemoryProperties(physical_device_, &memory_properties_);
    }

    VkSampleCountFlagBits PhysicalDevice::max_samples() const noexcept
    {
        auto properties = this->properties();
        uint32_t const count = std::min(
            properties.limits.framebufferColorSampleCounts, properties.limits.framebufferDepthSampleCounts
        );

        if (count & VK_SAMPLE_COUNT_64_BIT) return VK_SAMPLE_COUNT_64_BIT;
        if (count & VK_SAMPLE_COUNT_32_BIT) return VK_SAMPLE_COUNT_32_BIT;
        if (count & VK_SAMPLE_COUNT_16_BIT) return VK_SAMPLE_COUNT_16_BIT;
        if (count & VK_SAMPLE_COUNT_8_BIT) return VK_SAMPLE_COUNT_8_BIT;
        if (count & VK_SAMPLE_COUNT_4_BIT) return VK_SAMPLE_COUNT_4_BIT;
        if (count & VK_SAMPLE_COUNT_2_BIT) return VK_SAMPLE_COUNT_2_BIT;

        return VK_SAMPLE_COUNT_1_BIT;
    }

    std::vector<VkQueueFamilyProperties> PhysicalDevice::queue_family_properties() const noexcept
    {
        uint32_t count;
        vkGetPhysicalDeviceQueueFamilyProperties(physical_device_, &count, nullptr);
        std::vector<VkQueueFamilyProperties> properties(count);
        vkGetPhysicalDeviceQueueFamilyProperties(physical_device_, &count, properties.data());

        return properties;
    }

    VkBool32 PhysicalDevice::support_surface(uint32_t index, VkSurfaceKHR surface) const noexcept
    {
        VkBool32 result;
        vkGetPhysicalDeviceSurfaceSupportKHR(physical_device_, index, surface, &result);

        return result;
    }

    std::vector<VkSurfaceFormatKHR> PhysicalDevice::formats(VkSurfaceKHR surface) const noexcept
    {
        uint32_t count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device_, surface, &count, nullptr);
        std::vector<VkSurfaceFormatKHR> formats(count);
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device_, surface, &count, formats.data());

        return formats;
    }

    VkSurfaceCapabilitiesKHR PhysicalDevice::capabilities(VkSurfaceKHR surface) const noexcept
    {
        VkSurfaceCapabilitiesKHR cap;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device_, surface, &cap);

        return cap;
    }

    std::vector<VkPresentModeKHR> PhysicalDevice::present_modes(VkSurfaceKHR surface) const noexcept
    {
        uint32_t count;
        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device_, surface, &count, nullptr);
        std::vector<VkPresentModeKHR> modes(count);
        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device_, surface, &count, modes.data());

        return modes;
    }

    VkFormatProperties PhysicalDevice::format_properties(VkFormat format) const noexcept
    {
        VkFormatProperties properties;
        vkGetPhysicalDeviceFormatProperties(physical_device_, format, &properties);

        return properties;
    }

    VkPhysicalDeviceFeatures const& PhysicalDevice::features() const noexcept
    {
        return features_;
    }

    VkPhysicalDeviceProperties const& PhysicalDevice::properties() const noexcept
    {
        return properties_;
    }
}
