#include "chapar/vulkan/shader.hpp"
#include "chapar/core/error.hpp"
#include <fstream>
#include <cmath>

using namespace std::string_literals;

namespace chapar::vulkan {

    // ==================================================
    // Shader
    // ==================================================

    std::vector<uint32_t> Shader::read_spirv(gsl::czstring filename)
    {
        auto file = std::ifstream{filename, std::ios::binary | std::ios::in | std::ios::ate};
        if (!file.is_open()) {
            throw Exception{"Failed to open file: "s + filename};
        }

        std::vector<uint32_t> code;
        auto size = file.tellg();
        auto size32 = std::ceil(static_cast<double>(size) / 4);
        code.resize(static_cast<std::size_t>(size32));

        file.seekg(0, std::ios::beg);
        file.read(reinterpret_cast<char*>(code.data()), size);
        file.close();

        return code;
    }

    std::pair<VkShaderModule, VkResult> Shader::create_module(VkDevice device, std::vector<uint32_t> const& code) noexcept
    {
        VkShaderModuleCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.codeSize = static_cast<uint32_t>(code.size() * 4);
        info.pCode = code.data();

        VkShaderModule module;
        VkResult result = vkCreateShaderModule(device, &info, nullptr, &module);

        return std::make_pair(module, result);
    }





    // ==================================================
    // VertexShader
    // ==================================================

    VertexShader::VertexShader(VkDevice device, std::vector<uint32_t> const& code)
    : device_{device}
    {
        VkResult result;
        std::tie(module_, result) = create_module(device_, code);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create vertex shader module."};
        }
    }

    VertexShader::~VertexShader() noexcept
    {
        if (module_ == VK_NULL_HANDLE) return;
        vkDestroyShaderModule(device_, module_, nullptr);
    }

    VkPipelineShaderStageCreateInfo VertexShader::pipeline_stage() const noexcept
    {
        VkPipelineShaderStageCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.stage = VK_SHADER_STAGE_VERTEX_BIT;
        info.module = module_;
        info.pName = "main";
        info.pSpecializationInfo = {};

        return info;
    }





    // ==================================================
    // FragmentShader
    // ==================================================

    FragmentShader::FragmentShader(VkDevice device, std::vector<uint32_t> const& code)
    : device_{device}
    {
        VkResult result;
        std::tie(module_, result) = create_module(device_, code);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create fragment shader module."};
        }
    }

    FragmentShader::~FragmentShader() noexcept
    {
        if (module_ == VK_NULL_HANDLE) return;
        vkDestroyShaderModule(device_, module_, nullptr);
    }

    VkPipelineShaderStageCreateInfo FragmentShader::pipeline_stage() const noexcept
    {
        VkPipelineShaderStageCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        info.module = module_;
        info.pName = "main";
        info.pSpecializationInfo = {};

        return info;
    }
}
