#include "chapar/vulkan/camera.hpp"
#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/buffer.hpp"
#include "chapar/vulkan/descriptor.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/swapchain.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <gsl-lite/gsl-lite.hpp>

namespace chapar::vulkan {

    Camera::Camera(
        Device const& device,
        Allocator const& allocator,
        uint32_t image_count,
        VkDescriptorSetLayout set_layout
    )
    {
        device_ = device.handle();

        std::vector<VkDescriptorPoolSize> pool_sizes = {{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, image_count}};
        descriptor_pool_ = std::make_unique<DescriptorPool>(device_, image_count, pool_sizes);

        std::vector<VkDescriptorSet> sets = descriptor_pool_->allocate_sets(image_count, set_layout);

        uniforms_.resize(image_count);
        for (gsl::index i = 0; i < image_count; ++i) {
            uniforms_[i].descriptor_set = sets[i];
            uniforms_[i].buffer = std::make_unique<UniformBuffer>(allocator.handle(), sizeof(UBO));
        }
    }

    void Camera::set_aspect(VkExtent2D const& extent) noexcept
    {
        aspect_ = extent.width / static_cast<float>(extent.height);
    }

    void Camera::rotate(glm::vec3 const& delta) noexcept
    {
        rotation_ += delta;
    }

    void Camera::translate(glm::vec3 const& delta) noexcept
    {
        translation_ += delta;
    }

    void Camera::update(std::size_t buffer_index) noexcept
    {
        UBO ubo;
        ubo.view = glm::lookAt(
            glm::vec3(2.0f, 2.0f, 2.0f),
            glm::vec3(0.0f, 0.0f, 0.0f),
            glm::vec3(0.0f, 0.0f, 1.0f)
        );
        ubo.proj = glm::perspective(
            glm::radians(45.0f),
            aspect_,
            0.1f,
            10.0f
        );
        ubo.proj[1][1] *= -1;

        auto& ub = uniforms_[buffer_index].buffer;
        ub->map();
        ub->memcpy(0, std::span{reinterpret_cast<std::byte const*>(&ubo), sizeof(ubo)});
        ub->unmap();
    }

    void Camera::write(uint32_t binding) const noexcept
    {
        for (gsl::index i = 0; i < uniforms_.size(); ++i) {
            auto info = uniforms_[i].buffer->descriptor();

            VkWriteDescriptorSet write = {};
            write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            write.pNext = {};
            write.dstSet = uniforms_[i].descriptor_set;
            write.dstBinding = binding;
            write.dstArrayElement = 0;
            write.descriptorCount = 1;
            write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            write.pBufferInfo = &info;
            write.pImageInfo = nullptr;
            write.pTexelBufferView = nullptr;

            vkUpdateDescriptorSets(device_, 1, &write, 0, nullptr);
        }
    }

    VkDescriptorSet Camera::descriptor_set(std::size_t index) const noexcept
    {
        return uniforms_[index].descriptor_set;
    }
}
