#include "chapar/vulkan/descriptor.hpp"
#include "chapar/core/error.hpp"

namespace chapar::vulkan {

    // ==================================================
    // DescriptorPool
    // ==================================================

    DescriptorPool::DescriptorPool(
        VkDevice device,
        uint32_t max_sets,
        std::vector<VkDescriptorPoolSize> const& pool_sizes
    )
    : device_{device}
    {
        VkDescriptorPoolCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.maxSets = max_sets;
        info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
        info.pPoolSizes = pool_sizes.data();

        VkResult result = vkCreateDescriptorPool(device_, &info, nullptr, &pool_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create descriptor pool."};
        }
    }

    DescriptorPool::~DescriptorPool() noexcept
    {
        if (pool_ == VK_NULL_HANDLE) return;
        vkDestroyDescriptorPool(device_, pool_, nullptr);
    }

    std::vector<VkDescriptorSet> DescriptorPool::allocate_sets(
        uint32_t count,
        VkDescriptorSetLayout layout
    ) const
    {
        std::vector<VkDescriptorSetLayout> layouts(count, layout);

        VkDescriptorSetAllocateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        info.pNext = {};
        info.descriptorPool = pool_;
        info.descriptorSetCount = static_cast<uint32_t>(layouts.size());
        info.pSetLayouts = layouts.data();

        std::vector<VkDescriptorSet> sets(count);
        VkResult result = vkAllocateDescriptorSets(device_, &info, sets.data());
        if (result != VK_NULL_HANDLE) {
            throw Exception{"Vulkan failed to allocate descriptor set"};
        }

        return sets;
    }





    // ==================================================
    // DescriptorSetLayout
    // ==================================================

    DescriptorSetLayout::DescriptorSetLayout(
        VkDevice device,
        std::vector<VkDescriptorSetLayoutBinding> const& bindings
    )
    : device_{device}
    {
        VkDescriptorSetLayoutCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.bindingCount = static_cast<uint32_t>(bindings.size());
        info.pBindings = bindings.data();

        VkResult result = vkCreateDescriptorSetLayout(device_, &info, nullptr, &layout_);
        if (result != VK_NULL_HANDLE) {
            throw Exception{"Vulkan failed to create descriptor ser layout."};
        }
    }

    DescriptorSetLayout::~DescriptorSetLayout() noexcept
    {
        if (layout_ == VK_NULL_HANDLE) return;
        vkDestroyDescriptorSetLayout(device_, layout_, nullptr);
    }





    // ==================================================
    // write_descriptor_set
    // ==================================================

    VkWriteDescriptorSet write_descriptor_set(
        VkDescriptorSet descriptor_set,
        uint32_t binding,
        std::vector<VkDescriptorBufferInfo> const& infos
    )
    {
        VkWriteDescriptorSet write = {};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.pNext = {};
        write.dstSet = descriptor_set;
        write.dstBinding = binding;
        write.dstArrayElement = 0;
        write.descriptorCount = static_cast<uint32_t>(infos.size());
        write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        write.pBufferInfo = infos.data();
        write.pImageInfo = nullptr;
        write.pTexelBufferView = nullptr;

        return write;
    }

    VkWriteDescriptorSet write_descriptor_set(
        VkDescriptorSet descriptor_set,
        uint32_t binding,
        std::vector<VkDescriptorImageInfo> const& infos
    )
    {
        VkWriteDescriptorSet write = {};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.pNext = {};
        write.dstSet = descriptor_set;
        write.dstBinding = binding;
        write.dstArrayElement = 0;
        write.descriptorCount = static_cast<uint32_t>(infos.size());
        write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        write.pBufferInfo = nullptr;
        write.pImageInfo = infos.data();
        write.pTexelBufferView = nullptr;

        return write;
    }
}
