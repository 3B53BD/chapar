/*
    Create an image barrier for changing the layout.
*/

#ifndef CHAPAR_2oRPibx8MrC5y4Ry_HEADER_GUARD
#define CHAPAR_2oRPibx8MrC5y4Ry_HEADER_GUARD

#include <map>

namespace chapar::vulkan::details {
    namespace details {}

    // ==================================================
    // Old Image Layout
    // ==================================================
    /*
        Source layouts.
        Source access mask controls actions that have to be finished on the old layout
        before it can be transitioned to the new layout.
    */

    struct OldImageLayout {
        virtual ~OldImageLayout() noexcept {}
        virtual VkPipelineStageFlags operator()() const noexcept = 0;
    };

    struct Old_VkImageLayoutUndefined : public OldImageLayout {
        ~Old_VkImageLayoutUndefined() noexcept override {}
        VkPipelineStageFlags operator()() const noexcept override
        {
            return 0;
        }
    };

    struct Old_VkImageLayoutPreinitialized : public OldImageLayout {
        ~Old_VkImageLayoutPreinitialized() noexcept override {}
        VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_HOST_WRITE_BIT;
        }
    };

    struct Old_VkImageLayoutColorAttachmentOptimal : public OldImageLayout {
        ~Old_VkImageLayoutColorAttachmentOptimal() noexcept override {}
        VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        }
    };

    struct Old_VkImageLayoutDepthStencilAttachmentOptimal : public OldImageLayout {
        ~Old_VkImageLayoutDepthStencilAttachmentOptimal() noexcept override {}
        VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        }
    };

    struct Old_VkImageLayoutTransferSrcOptimal : public OldImageLayout {
        ~Old_VkImageLayoutTransferSrcOptimal() noexcept override {}
        VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_TRANSFER_READ_BIT;
        }
    };

    struct Old_VkImageLayoutTransferDstOptimal : public OldImageLayout {
        ~Old_VkImageLayoutTransferDstOptimal() noexcept override {}
        VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_TRANSFER_WRITE_BIT;
        }
    };

    struct Old_VkImageLayoutSharedReadOnlyOptimal : public OldImageLayout {
        ~Old_VkImageLayoutSharedReadOnlyOptimal() noexcept override {}
        VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_SHADER_READ_BIT;
        }
    };

    std::map<VkImageLayout, std::unique_ptr<OldImageLayout>> src_access_mask_table()
    {
        std::map<VkImageLayout, std::unique_ptr<OldImageLayout>> table;

        table[VK_IMAGE_LAYOUT_UNDEFINED] = std::make_unique<Old_VkImageLayoutUndefined>();
        table[VK_IMAGE_LAYOUT_PREINITIALIZED] = std::make_unique<Old_VkImageLayoutPreinitialized>();
        table[VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL] = std::make_unique<Old_VkImageLayoutColorAttachmentOptimal>();
        table[VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL] = std::make_unique<Old_VkImageLayoutDepthStencilAttachmentOptimal>();
        table[VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL] = std::make_unique<Old_VkImageLayoutTransferSrcOptimal>();
        table[VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL] = std::make_unique<Old_VkImageLayoutTransferDstOptimal>();
        table[VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL] = std::make_unique<Old_VkImageLayoutSharedReadOnlyOptimal>();

        return table;
    }

    // ==================================================
    // Old Image Layout
    // ==================================================
    /*
        Target layouts.
        Destination access mask controls the dependency for the new image layout.
     */

    struct NewImageLayout {
        virtual ~NewImageLayout() noexcept {}
        virtual VkPipelineStageFlags operator()() const noexcept = 0;
    };

    struct New_VkImageLayoutTransferDstOptimal : public NewImageLayout {
        ~New_VkImageLayoutTransferDstOptimal() noexcept override {}
        virtual VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_TRANSFER_WRITE_BIT;
        }
    };

    struct New_VkImageLayoutTransferSrcOptimal : public NewImageLayout {
        ~New_VkImageLayoutTransferSrcOptimal() noexcept override {}
        virtual VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_TRANSFER_READ_BIT;
        }
    };

    struct New_VkImageLayoutColorAttachmentOptimal : public NewImageLayout {
        ~New_VkImageLayoutColorAttachmentOptimal() noexcept override {}
        virtual VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        }
    };

    struct New_VkImageLayoutDepthStencilAttachmentOptimal : public NewImageLayout {
        ~New_VkImageLayoutDepthStencilAttachmentOptimal() noexcept override {}
        virtual VkPipelineStageFlags operator()() const noexcept override
        {
            return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        }
    };

    struct New_VkImageLayoutSharedReadOnlyOptimal : public NewImageLayout {
        New_VkImageLayoutSharedReadOnlyOptimal(VkPipelineStageFlags& stage)
            : stage_{stage}
        { }
        ~New_VkImageLayoutSharedReadOnlyOptimal() noexcept override {}
        virtual VkPipelineStageFlags operator()() const noexcept override
        {
            if (stage_ == 0) {
                stage_ = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
            }
            return VK_ACCESS_SHADER_READ_BIT;
        }
        VkPipelineStageFlags& stage_;
    };

    std::map<VkImageLayout, std::unique_ptr<NewImageLayout>> dst_access_mask_table(
        VkImageMemoryBarrier& barrier
    )
    {
        std::map<VkImageLayout, std::unique_ptr<NewImageLayout>> table;

        table[VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL] = std::make_unique<New_VkImageLayoutTransferDstOptimal>();
        table[VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL] = std::make_unique<New_VkImageLayoutTransferSrcOptimal>();
        table[VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL] = std::make_unique<New_VkImageLayoutColorAttachmentOptimal>();
        table[VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL] = std::make_unique<New_VkImageLayoutDepthStencilAttachmentOptimal>();
        table[VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL] = std::make_unique<New_VkImageLayoutSharedReadOnlyOptimal>(barrier.srcAccessMask);

        return table;
    }
}

#endif
