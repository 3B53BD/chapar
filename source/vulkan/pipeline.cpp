#include "chapar/vulkan/pipeline.hpp"
#include "chapar/core/error.hpp"

#include <iostream>

namespace chapar::vulkan {

    VkPipelineLayoutCreateInfo PipelineLayout::create_info(std::vector<VkDescriptorSetLayout> const& layouts) noexcept
    {
        VkPipelineLayoutCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.setLayoutCount = static_cast<uint32_t>(layouts.size());
        info.pSetLayouts = layouts.data();

        return info;
    }

    PipelineLayout::PipelineLayout(VkDevice device, VkPipelineLayoutCreateInfo const& create_info)
    {
        device_ = device;

        VkResult result = vkCreatePipelineLayout(device_, &create_info, nullptr, &pipeline_layout_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create pipeline layout."};
        }
    }

    PipelineLayout::~PipelineLayout() noexcept
    {
        if (pipeline_layout_ == VK_NULL_HANDLE) return;
        vkDestroyPipelineLayout(device_, pipeline_layout_, nullptr);
    }





    // ==================================================
    // PipelineCreateInfoBuilder
    // ==================================================

    PipelineCreateInfoBuilder::PipelineCreateInfoBuilder() noexcept
    {
        reset();
    }

    void PipelineCreateInfoBuilder::reset() noexcept
    {

    }

    void PipelineCreateInfoBuilder::set_vertex_input(
        std::vector<VkVertexInputBindingDescription> const& bindings,
        std::vector<VkVertexInputAttributeDescription> const& attributes
    ) noexcept
    {
        vertex_input_sci_ = {};
        vertex_input_sci_.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertex_input_sci_.pNext = {};
        vertex_input_sci_.flags = {};
        vertex_input_sci_.vertexBindingDescriptionCount = static_cast<uint32_t>(bindings.size());
        vertex_input_sci_.pVertexBindingDescriptions = bindings.data();
        vertex_input_sci_.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributes.size());
        vertex_input_sci_.pVertexAttributeDescriptions = attributes.data();

        input_assembly_sci_ = {};
        input_assembly_sci_.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        input_assembly_sci_.pNext = {};
        input_assembly_sci_.flags = {};
        input_assembly_sci_.primitiveRestartEnable = VK_FALSE;
        input_assembly_sci_.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    }

    void PipelineCreateInfoBuilder::add_viewporct(VkViewport const& viewport) noexcept
    {
        viewports_.push_back(viewport);
    }

    void PipelineCreateInfoBuilder::add_viewport(float x, float y, float width, float height, float min_depth, float max_depth) noexcept
    {
        VkViewport viewport = {};
        viewport.x = x;
        viewport.y = y;
        viewport.width = width;
        viewport.height = height;
        viewport.minDepth = min_depth;
        viewport.maxDepth = max_depth;

        viewports_.push_back(viewport);
    }

    void PipelineCreateInfoBuilder::add_scissor(VkRect2D const& scissor) noexcept
    {
        scissors_.push_back(scissor);
    }

    void PipelineCreateInfoBuilder::add_scissor(VkOffset2D const& offset, VkExtent2D const& extent) noexcept
    {
        VkRect2D scissor = {};
        scissor.offset = offset;
        scissor.extent = extent;

        scissors_.push_back(scissor);
    }

    void PipelineCreateInfoBuilder::add_scissor(int32_t x, int32_t y, uint32_t width, uint32_t height) noexcept
    {
        VkRect2D scissor = {};
        scissor.offset = {x, y};
        scissor.extent = {width, height};

        scissors_.push_back(scissor);
    }

    void PipelineCreateInfoBuilder::set_samples(VkSampleCountFlagBits samples) noexcept
    {
        multisample_sci_ = {};
        multisample_sci_.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisample_sci_.pNext = {};
        multisample_sci_.flags = {};
        multisample_sci_.sampleShadingEnable = VK_TRUE;
        multisample_sci_.minSampleShading = 0.2f;
        multisample_sci_.pSampleMask = {};
        multisample_sci_.alphaToCoverageEnable = {};
        multisample_sci_.alphaToOneEnable = {};
        multisample_sci_.rasterizationSamples = samples;
    }

    void PipelineCreateInfoBuilder::add_color_blend_attachment() noexcept
    {
        VkPipelineColorBlendAttachmentState attachment = {};
        attachment.blendEnable = VK_FALSE;
        attachment.srcColorBlendFactor = {};
        attachment.dstColorBlendFactor = {};
        attachment.colorBlendOp = {};
        attachment.srcAlphaBlendFactor = {};
        attachment.dstAlphaBlendFactor = {};
        attachment.alphaBlendOp = {};
        attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        color_blend_attachment_ss_.push_back(attachment);
    }

    void PipelineCreateInfoBuilder::add_shader_stage(VkPipelineShaderStageCreateInfo info) noexcept
    {
        shader_scis_.push_back(info);
    }

    void PipelineCreateInfoBuilder::set_render_pass(VkRenderPass render_pass) noexcept
    {
        render_pass_ = render_pass;
    }

    void PipelineCreateInfoBuilder::set_pipeline_layout(VkPipelineLayout pipeline_layout) noexcept
    {
        pipeline_layout_ = pipeline_layout;
    }

    VkGraphicsPipelineCreateInfo PipelineCreateInfoBuilder::pipeline_create_info() noexcept
    {
        viewport_sci_.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewport_sci_.pNext = {};
        viewport_sci_.flags = {};
        viewport_sci_.viewportCount = static_cast<uint32_t>(viewports_.size());
        viewport_sci_.pViewports = viewports_.data();
        viewport_sci_.scissorCount = static_cast<uint32_t>(scissors_.size());
        viewport_sci_.pScissors = scissors_.data();

        rasterization_sci_.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterization_sci_.pNext = {};
        rasterization_sci_.flags = {};
        rasterization_sci_.depthClampEnable = VK_FALSE;
        rasterization_sci_.rasterizerDiscardEnable = VK_FALSE;
        rasterization_sci_.polygonMode = VK_POLYGON_MODE_FILL;
        rasterization_sci_.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterization_sci_.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterization_sci_.depthBiasEnable = VK_FALSE;
        rasterization_sci_.depthBiasConstantFactor = {};
        rasterization_sci_.depthBiasClamp = {};
        rasterization_sci_.depthBiasSlopeFactor = {};
        rasterization_sci_.lineWidth = 1.0f;

        depth_stencil_sci_.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depth_stencil_sci_.pNext = {};
        depth_stencil_sci_.flags = {};
        depth_stencil_sci_.depthTestEnable = VK_TRUE;
        depth_stencil_sci_.depthWriteEnable = VK_TRUE;
        depth_stencil_sci_.depthCompareOp = VK_COMPARE_OP_LESS;
        depth_stencil_sci_.depthBoundsTestEnable = VK_FALSE;
        depth_stencil_sci_.stencilTestEnable = VK_FALSE;
        depth_stencil_sci_.front = {};
        depth_stencil_sci_.back = {};
        depth_stencil_sci_.minDepthBounds = 0.0f;
        depth_stencil_sci_.maxDepthBounds = 1.0f;

        color_blend_sci_.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        color_blend_sci_.pNext = {};
        color_blend_sci_.flags = {};
        color_blend_sci_.logicOpEnable = VK_FALSE;
        color_blend_sci_.logicOp = VK_LOGIC_OP_COPY;
        color_blend_sci_.attachmentCount = static_cast<uint32_t>(color_blend_attachment_ss_.size());
        color_blend_sci_.pAttachments = color_blend_attachment_ss_.data();
        color_blend_sci_.blendConstants[0] = 0.0f;
        color_blend_sci_.blendConstants[1] = 0.0f;
        color_blend_sci_.blendConstants[2] = 0.0f;
        color_blend_sci_.blendConstants[3] = 0.0f;

        VkGraphicsPipelineCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.stageCount = static_cast<uint32_t>(shader_scis_.size());
        info.pStages = shader_scis_.data();
        info.pVertexInputState = &vertex_input_sci_;
        info.pInputAssemblyState = &input_assembly_sci_;
        info.pTessellationState = {};
        info.pViewportState = &viewport_sci_;
        info.pRasterizationState = &rasterization_sci_;
        info.pMultisampleState = &multisample_sci_;
        info.pDepthStencilState = &depth_stencil_sci_;
        info.pColorBlendState = &color_blend_sci_;
        info.pDynamicState = {};
        info.layout = pipeline_layout_;
        info.renderPass = render_pass_;
        info.subpass = 0;
        info.basePipelineHandle = VK_NULL_HANDLE;
        info.basePipelineIndex = {};

        return info;
    }





    // ==================================================
    // Pipeline
    // ==================================================

    Pipeline::Pipeline(
        VkDevice device,
        VkGraphicsPipelineCreateInfo const& create_info
    )
    : device_{device}
    {
        VkResult result = vkCreateGraphicsPipelines(
            device_,        // device
            VK_NULL_HANDLE, // pipelineCache
            1,              // createInfoCount
            &create_info,   // pCreateInfos
            nullptr,        // pAllocator
            &pipeline_      // pPipelines
        );
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create pipeline."};
        }
    }

    Pipeline::~Pipeline() noexcept
    {
        if (pipeline_ == VK_NULL_HANDLE) return;
        vkDestroyPipeline(device_, pipeline_, nullptr);
    }
}
