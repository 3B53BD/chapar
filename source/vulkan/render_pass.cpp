#include "chapar/vulkan/render_pass.hpp"
#include "chapar/core/error.hpp"
#include <gsl-lite/gsl-lite.hpp>

#include <iostream>

namespace chapar::vulkan {

    // ==================================================
    // Subpass
    // ==================================================

    VkSubpassDescription subpass_description(SubpassDescription const& sd)
    {
        if (!sd.resolve.empty() && sd.resolve.size() != sd.color.size()) {
            throw Exception{"Resolve count must be same as color count."};
        }

        VkSubpassDescription info = {};
        info.flags = {};
        info.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        info.inputAttachmentCount = static_cast<uint32_t>(sd.input.size());
        info.pInputAttachments = sd.input.data();
        info.colorAttachmentCount = static_cast<uint32_t>(sd.color.size());
        info.pColorAttachments = sd.color.data();
        info.pResolveAttachments = sd.resolve.data();
        info.pDepthStencilAttachment = sd.depth.has_value() ? &sd.depth.value() : nullptr;
        info.preserveAttachmentCount = {};
        info.pPreserveAttachments = {};

        return info;
    }

    SubpassDependency external_dependency()
    {
        SubpassDependency dep = {};
        dep.subpass = VK_SUBPASS_EXTERNAL;
        dep.stage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dep.access = 0;

        return dep;
    }

    VkSubpassDependency dependency(
        SubpassDependency const& src,
        SubpassDependency const& dst
    )
    {
        VkSubpassDependency dep = {};
        dep.srcSubpass = src.subpass;
        dep.dstSubpass = dst.subpass;
        dep.srcStageMask = src.stage;
        dep.dstStageMask = dst.stage;
        dep.srcAccessMask = src.access;
        dep.dstAccessMask = dst.access;
        dep.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        return dep;
    }





    // ==================================================
    // RenderPass
    // ==================================================

    RenderPass::RenderPass(
        VkDevice device,
        std::vector<VkAttachmentDescription> const& attachments,
        std::vector<VkSubpassDescription> const& descriptions,
        std::vector<VkSubpassDependency> const& dependencies
    )
    : device_{device}
    {
        VkRenderPassCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.attachmentCount = static_cast<uint32_t>(attachments.size());
        info.pAttachments = attachments.data();
        info.subpassCount = static_cast<uint32_t>(descriptions.size());
        info.pSubpasses = descriptions.data();
        info.dependencyCount = static_cast<uint32_t>(dependencies.size());
        info.pDependencies = dependencies.data();

        VkResult result = vkCreateRenderPass(device_, &info, nullptr, &render_pass_);
        if (result != VK_NULL_HANDLE) {
            throw Exception{"Vulkan failed to create render pass."};
        }
    }

    RenderPass::~RenderPass() noexcept
    {
        if (render_pass_ == VK_NULL_HANDLE) return;
        vkDestroyRenderPass(device_, render_pass_, nullptr);
    }
}
