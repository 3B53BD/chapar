#include "chapar/vulkan/queue.hpp"

namespace chapar::vulkan {

    // ==================================================
    // QueueFamily
    // ==================================================

    QueueFamily::QueueFamily(
        uint32_t index,
        VkQueueFamilyProperties const& properties,
        bool surface_support
    )
    {
        index_ = index;
        capabilities_ = properties.queueFlags;
        surface_support_ = surface_support;
    }

    uint32_t QueueFamily::index() const noexcept
    {
        return index_;
    }
    bool QueueFamily::graphics_bit() const noexcept
    {
        return capabilities_ & VK_QUEUE_GRAPHICS_BIT;
    }
    bool QueueFamily::compute_bit() const noexcept
    {
        return capabilities_ & VK_QUEUE_COMPUTE_BIT;
    }
    bool QueueFamily::transfer_bit() const noexcept
    {
        return capabilities_ & VK_QUEUE_TRANSFER_BIT;
    }
    bool QueueFamily::sparse_binding_bit() const noexcept
    {
        return capabilities_ & VK_QUEUE_SPARSE_BINDING_BIT;
    }
    bool QueueFamily::protected_bit() const noexcept
    {
        return capabilities_ & VK_QUEUE_PROTECTED_BIT;
    }
    bool QueueFamily::surface_bit() const noexcept
    {
        return surface_support_;
    }





    // ==================================================
    // Queue
    // ==================================================

    QueueSumbitInfo::QueueSumbitInfo() noexcept
    {
        info_.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        info_.pNext = {};
    }

    void QueueSumbitInfo::wait(VkSemaphore semaphore, VkPipelineStageFlags stage) noexcept
    {
        wait_semaphores_.push_back(semaphore);
        wait_stages_.push_back(stage);
        info_.waitSemaphoreCount = static_cast<uint32_t>(wait_semaphores_.size());
        info_.pWaitSemaphores = wait_semaphores_.data();
        info_.pWaitDstStageMask = wait_stages_.data();
    }

    void QueueSumbitInfo::command(VkCommandBuffer buffer) noexcept
    {
        command_buffers_.push_back(buffer);
        info_.commandBufferCount = static_cast<uint32_t>(command_buffers_.size());
        info_.pCommandBuffers = command_buffers_.data();
    }

    void QueueSumbitInfo::signal(VkSemaphore signal) noexcept
    {
        signal_semaphores_.push_back(signal);
        info_.signalSemaphoreCount = static_cast<uint32_t>(signal_semaphores_.size());
        info_.pSignalSemaphores = signal_semaphores_.data();
    }

    VkSubmitInfo const& QueueSumbitInfo::info() const noexcept
    {
        return info_;
    }





    // ==================================================
    // Queue
    // ==================================================

    Queue::Queue(VkQueue queue, uint32_t family) noexcept
    : queue_{queue}
    , family_{family}
    { }

    void Queue::wait_idle() const noexcept
    {
        vkQueueWaitIdle(queue_);
    }

    void Queue::submit(VkSubmitInfo const& info, VkFence fence) const noexcept
    {
        vkQueueSubmit(queue_, 1, &info, fence);
    }

    uint32_t Queue::family() const noexcept
    {
        return family_;
    }
}
