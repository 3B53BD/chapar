#include "chapar/vulkan/texture.hpp"
#include "chapar/vulkan/image.hpp"

namespace chapar::vulkan {

    Texture::Texture(
        std::shared_ptr<Image> image,
        std::shared_ptr<ImageView> image_view,
        std::shared_ptr<Sampler> sampler
    )
    {
        image_ = image;
        image_view_ = image_view;
        sampler_ = sampler;
    }

    Image const& Texture::image() const noexcept
    {
        return *image_;
    }

    ImageView const& Texture::image_view() const noexcept
    {
        return *image_view_;
    }

    Sampler const& Texture::sampler() const noexcept
    {
        return *sampler_;
    }

    VkDescriptorImageInfo Texture::descriptor() const noexcept
    {
        VkDescriptorImageInfo descriptor;
        descriptor.sampler = sampler_->handle();
        descriptor.imageView = image_view_->handle();
        descriptor.imageLayout = image_->layout();

        return descriptor;
    }
}
