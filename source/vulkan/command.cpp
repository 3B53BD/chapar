#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/buffer.hpp"
#include "chapar/vulkan/image.hpp"
#include "chapar/core/error.hpp"

namespace chapar::vulkan {

    // ==================================================
    // CommandPool
    // ==================================================

    VkCommandPoolCreateInfo CommandPool::create_info(uint32_t queue_family_index) noexcept
    {
        VkCommandPoolCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.queueFamilyIndex = queue_family_index;

        return info;
    }

    CommandPool::CommandPool(
        VkDevice device,
        VkCommandPoolCreateInfo const& info
    )
    {
        device_ = device;

        VkResult result = vkCreateCommandPool(device_, &info, nullptr, &command_pool_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create command pool."};
        }
    }

    CommandPool::~CommandPool() noexcept
    {
        if (command_pool_ == VK_NULL_HANDLE) return;
        vkDestroyCommandPool(device_, command_pool_, nullptr);
    }

    VkCommandBufferAllocateInfo CommandPool::allocate_info(uint32_t count) const noexcept
    {
        VkCommandBufferAllocateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        info.pNext = {};
        info.commandPool = command_pool_;
        info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        info.commandBufferCount = count;

        return info;
    }

    std::vector<VkCommandBuffer> CommandPool::allocate_command_buffers(VkCommandBufferAllocateInfo const& info) const
    {
        std::vector<VkCommandBuffer> buffers(info.commandBufferCount);
        VkResult result = vkAllocateCommandBuffers(device_, &info, buffers.data());
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to allocate command buffers."};
        }

        return buffers;
    }

    void CommandPool::free_command_buffers(std::vector<VkCommandBuffer> const& command_buffers) const noexcept
    {
        vkFreeCommandBuffers(device_, command_pool_, static_cast<uint32_t>(command_buffers.size()), command_buffers.data());
    }

    Queue const& CommandPool::queue() const noexcept
    {
        return queue_;
    }





    // ==================================================
    // CommandBuffer
    // ==================================================

    VkCommandBufferBeginInfo CommandBuffer::begin_info() noexcept
    {
        VkCommandBufferBeginInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        info.pNext = {};
        info.flags = {};
        info.pInheritanceInfo = {};

        return info;
    }

    VkCommandBufferInheritanceInfo CommandBuffer::inheritance_info() noexcept
    {
        VkCommandBufferInheritanceInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
        info.pNext = nullptr;
        info.renderPass = VK_NULL_HANDLE;
        info.subpass = 0;
        info.framebuffer = VK_NULL_HANDLE;
        info.occlusionQueryEnable = VK_FALSE;
        info.queryFlags = {};
        info.pipelineStatistics = {};

        return info;
    }

    CommandBuffer::CommandBuffer(CommandPool const& pool, VkCommandBuffer buffer) noexcept
    {
        command_buffer_ = buffer;
        command_pool_ = &pool;
    }

    CommandBuffer::CommandBuffer(CommandPool const& pool, VkCommandBufferAllocateInfo const& info)
    {
        command_pool_ = &pool;
        auto command_buffers = command_pool_->allocate_command_buffers(info);
        command_buffer_ = command_buffers[0];
    }

    CommandBuffer::~CommandBuffer() noexcept
    {
        if (command_buffer_ == VK_NULL_HANDLE) return;
        command_pool_->free_command_buffers({command_buffer_});
    }

    VkResult CommandBuffer::begin(VkCommandBufferBeginInfo const& info) const noexcept
    {
        return vkBeginCommandBuffer(command_buffer_, &info);
    }

    VkResult CommandBuffer::end() const noexcept
    {
        return vkEndCommandBuffer(command_buffer_);
    }

    void CommandBuffer::begin_render_pass(VkRenderPass render_pass, VkFramebuffer framebuffer, VkRect2D render_area, std::vector<VkClearValue> const& clear_values, VkSubpassContents contents) const noexcept
    {
        VkRenderPassBeginInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        info.pNext = {};
        info.renderPass = render_pass;
        info.framebuffer = framebuffer;
        info.renderArea = render_area;
        info.clearValueCount = static_cast<uint32_t>(clear_values.size());
        info.pClearValues = clear_values.data();

        vkCmdBeginRenderPass(command_buffer_, &info, contents);
    }

    void CommandBuffer::end_render_pass() const noexcept
    {
        vkCmdEndRenderPass(command_buffer_);
    }

    void CommandBuffer::bind(VkPipeline pipeline) const noexcept
    {
        vkCmdBindPipeline(command_buffer_, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
    }

    void CommandBuffer::bind(VkPipelineLayout layout, uint32_t first_set, std::vector<VkDescriptorSet> const& sets) const noexcept
    {
        vkCmdBindDescriptorSets(command_buffer_, VK_PIPELINE_BIND_POINT_GRAPHICS, layout, first_set, static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
    }

    void CommandBuffer::pipeline_barrier(
        VkPipelineStageFlags src,
        VkPipelineStageFlags dst,
        std::vector<VkImageMemoryBarrier> const& barriers
    ) const noexcept
    {
        vkCmdPipelineBarrier(
            command_buffer_,    // commandBuffer
            src,                // srcStageMask
            dst,                // dstStageMask
            0,                  // dependencyFlags
            0,                  // memoryBarrierCount
            nullptr,            // pMemoryBarriers
            0,                  // bufferMemoryBarrierCount
            nullptr,            // pBufferMemoryBarriers
            static_cast<uint32_t>(barriers.size()), // imageMemoryBarrierCount
            barriers.data()     // pImageMemoryBarriers
        );
    }

    void CommandBuffer::copy_to(
        Buffer const& src,
        Buffer const& dst,
        std::vector<VkBufferCopy> const& regions
    ) const noexcept
    {
        vkCmdCopyBuffer(command_buffer_, src.handle(), dst.handle(), static_cast<uint32_t>(regions.size()), regions.data());
    }

    void CommandBuffer::copy_to(
        Buffer const& src,
        Image const& dst,
        std::vector<VkBufferImageCopy> const& regions
    ) const noexcept
    {
        vkCmdCopyBufferToImage(command_buffer_, src.handle(), dst.handle(), dst.layout(), static_cast<uint32_t>(regions.size()), regions.data());
    }





    // ==================================================
    // OneTimeSubmit
    // ==================================================

    OneTimeSubmit::OneTimeSubmit(VkDevice device, Queue const& queue)
    {
        queue_ = queue;

        // Creates pool
        auto pool_info = CommandPool::create_info(queue_.family());
        pool_info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
        pool_ = std::make_unique<CommandPool>(device, pool_info);

        // Creates buffer
        auto alloc_info = pool_->allocate_info(1);
        auto handles = pool_->allocate_command_buffers(alloc_info);
        buffer_ = std::make_unique<CommandBuffer>(*pool_, handles[0]);

        // Starts recording
        auto begin_info = CommandBuffer::begin_info();
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        auto result = buffer_->begin(begin_info);
        if (result != VK_SUCCESS) {
            throw Exception{"Failed to begin command pool."};
        }
    }

    OneTimeSubmit::~OneTimeSubmit() noexcept
    {
        // Ends recording
        buffer_->end();

        // Submits commands
        QueueSumbitInfo submit;
        submit.command(buffer_->handle());
        queue_.submit(submit.info(), VK_NULL_HANDLE);
        queue_.wait_idle();

        // Clears commands
        buffer_.reset();
        pool_.reset();
    }

    VkCommandBuffer OneTimeSubmit::command_buffer_handle() const noexcept
    {
        return buffer_->handle();
    }

    CommandBuffer const* OneTimeSubmit::command_buffer() const noexcept
    {
        return buffer_.get();
    }
}
