#include "chapar/vulkan/scene.hpp"
#include "chapar/vulkan/buffer.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/descriptor.hpp"
#include "chapar/vulkan/image.hpp"
#include <glm/gtx/transform.hpp>
#include <gsl-lite/gsl-lite.hpp>

namespace chapar::vulkan {

    glm::mat4 Model::matrix() const noexcept
    {
        auto rot_x = glm::rotate(glm::radians(rot.x), glm::vec3(1.0f, 0.0f, 0.0f));
        auto rot_y = glm::rotate(glm::radians(rot.y), glm::vec3(0.0f, 1.0f, 0.0f));
        auto rot_z = glm::rotate(glm::radians(rot.z), glm::vec3(0.0f, 0.0f, 1.0f));
        return glm::translate(trl) * (rot_x * rot_y * rot_z) * glm::scale(scl)/* * matrix_*/;
    }





    void Uniform::write(VkDevice device, uint32_t binding) const noexcept
    {
        auto info = buffer->descriptor();

        VkWriteDescriptorSet write = {};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.pNext = {};
        write.dstSet = descriptor_set;
        write.dstBinding = binding;
        write.dstArrayElement = 0;
        write.descriptorCount = 1;
        write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        write.pBufferInfo = &info;
        write.pImageInfo = nullptr;
        write.pTexelBufferView = nullptr;

        vkUpdateDescriptorSets(device, 1, &write, 0, nullptr);
    }

    void Uniform::update(glm::mat4 const& m) const noexcept
    {
        buffer->map();
        buffer->memcpy(0, std::span{reinterpret_cast<std::byte const*>(&m), sizeof(glm::mat4)});
        buffer->unmap();
    }





    void Texture::write(VkDevice device, uint32_t binding) const noexcept
    {
        VkDescriptorImageInfo info = {};
        info.sampler = sampler->handle();
        info.imageView = image_view->handle();
        info.imageLayout = image->layout();

        VkWriteDescriptorSet write = {};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.pNext = {};
        write.dstSet = descriptor_set;
        write.dstBinding = binding;
        write.dstArrayElement = 0;
        write.descriptorCount = 1;
        write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        write.pBufferInfo = nullptr;
        write.pImageInfo = &info;
        write.pTexelBufferView = nullptr;

        vkUpdateDescriptorSets(device, 1, &write, 0, nullptr);
    }





    void Scene::describe(VkDescriptorSetLayout descriptor_set_layout) noexcept
    {
        for (auto& node : nodes) {
            describe(node, descriptor_set_layout);
        }
    }

    void Scene::write() const noexcept
    {
        for (auto const& node : nodes) {
            write(node);
        }

        for (auto const& texture : textures) {
            texture.write(device, binding);
        }
    }

    void Scene::update() const noexcept
    {
        for (auto const& node : nodes) {
            update(node);
        }
    }

    void Scene::record(VkPipelineLayout pipeline_layout) const noexcept
    {
        for (auto const& node : nodes) {
            record(node, pipeline_layout);
        }
    }

    void Scene::describe(Node& node, VkDescriptorSetLayout descriptor_set_layout) noexcept
    {
        for (auto& child : node.children) {
            describe(child, descriptor_set_layout);
        }

        if (node.mesh) {
            auto sets = descriptor_pool->allocate_sets(node.uniforms.size(), descriptor_set_layout);
            for (gsl::index i = 0; i < node.uniforms.size(); ++i) {
                node.uniforms[i].descriptor_set = sets[i];
                node.uniforms[i].write(device, binding);
            }
        }
    }

    void Scene::write(Node const& node) const noexcept
    {
        for (auto const& child : node.children) {
            write(child);
        }

        if (node.mesh) {
            node.uniforms[image_index].write(device, binding);
        }
    }

    void Scene::update(Node const& node) const noexcept
    {
        for (auto const& child : node.children) {
            update(child);
        }

        if (node.mesh) {
            auto m = node.model.matrix();
            auto p = node.parent;
            while (p) {
                m = p->model.matrix() * m;
                p = p->parent;
            }

            node.uniforms[image_index].update(m);
        }
    }

    void Scene::record(Node const& node, VkPipelineLayout pipeline_layout) const noexcept
    {
        for (auto const& child : node.children) {
            record(child, pipeline_layout);
        }

        if (node.mesh) {
            command_buffers[image_index]->bind(pipeline_layout, binding, {node.uniforms[image_index].descriptor_set});

            for (auto const& primitive : node.mesh->primitives) {
                primitive.buffer->bind(command_buffers[image_index]->handle());
                primitive.buffer->draw(command_buffers[image_index]->handle());
            }
        }
    }
}
