#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/queue.hpp"
#include "chapar/core/error.hpp"

namespace chapar::vulkan {

    Device::Device(
        VkPhysicalDevice physical_device,
        std::vector<uint32_t> const& queue_family_indices,
        std::vector<gsl::czstring> const& validation_layers,
        std::vector<gsl::czstring> const& extensions,
        VkPhysicalDeviceFeatures const& features
    )
    {
        std::array<float, 1> priorities = { 1.0f };
        std::vector<VkDeviceQueueCreateInfo> queues;
        for (auto const family : queue_family_indices) {
            VkDeviceQueueCreateInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            info.pNext = {};
            info.flags = {};
            info.queueFamilyIndex = family;
            info.queueCount = static_cast<uint32_t>(priorities.size());
            info.pQueuePriorities = priorities.data();

            queues.emplace_back(info);
        }

        VkDeviceCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.queueCreateInfoCount = static_cast<uint32_t>(queues.size());
        info.pQueueCreateInfos = queues.data();
        info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
        info.ppEnabledLayerNames = validation_layers.data();
        info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        info.ppEnabledExtensionNames = extensions.data();
        info.pEnabledFeatures = &features;

        VkResult res = vkCreateDevice(physical_device, &info, nullptr, &device_);
        if (res != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create device."};
        }
    }

    Device::~Device() noexcept
    {
        if (device_ == VK_NULL_HANDLE) return;
        vkDestroyDevice(device_, nullptr);
    }

    void Device::wait_idle() const noexcept
    {
        vkDeviceWaitIdle(device_);
    }

    VkQueue Device::queue(uint32_t family) const noexcept
    {
        VkQueue queue;
        vkGetDeviceQueue(device_, family, 0, &queue);

        return queue;
    }

    VkDevice Device::handle() const noexcept
    {
        return device_;
    }
}
