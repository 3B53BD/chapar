#include "chapar/vulkan/swapchain.hpp"
#include "chapar/vulkan/queue.hpp"
#include "chapar/core/error.hpp"

namespace chapar::vulkan {

    Swapchain::Swapchain(
        VkDevice device,
        VkSurfaceKHR surface,
        VkSurfaceTransformFlagBitsKHR pre_transform,
        uint32_t image_count,
        VkFormat image_format,
        VkColorSpaceKHR image_color_space,
        VkExtent2D image_extent,
        std::vector<uint32_t> const& queue_family_indices,
        VkPresentModeKHR present_mode
    )
    {
        images_.resize(image_count);
        image_extent_ = image_extent;
        present_mode_ = present_mode;
        queue_family_indices_ = queue_family_indices;
        pre_transform_ = pre_transform;
        image_format_.format = image_format;
        image_format_.colorSpace = image_color_space;
        surface_ = surface;
        device_ = device;

        create();
    }

    Swapchain::~Swapchain() noexcept
    {
        if (swapchain_ == VK_NULL_HANDLE) return;
        vkDestroySwapchainKHR(device_, swapchain_, nullptr);
    }

    void Swapchain::resize(VkExtent2D extent)
    {
        create();
    }

    VkResult Swapchain::aquire_next_image(VkSemaphore& semaphore)
    {
        VkResult result = vkAcquireNextImageKHR(
            device_,        // device
            swapchain_,     // swapchain
            UINT64_MAX,     // timeout
            semaphore,      // semaphore
            VK_NULL_HANDLE, // fence
            &image_index_   // pImageIndex
        );
        if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR && result != VK_ERROR_OUT_OF_DATE_KHR) {
            throw Exception{"Vulkan failed to aquire swapchain image."};
        }

        return result;
    }

    void Swapchain::wait_idle(VkFence fence) noexcept
    {
        auto& idle = idle_images_[image_index_];
        if (idle != VK_NULL_HANDLE) {
            vkWaitForFences(
                device_,    // device
                1,          // fenceCount
                &idle,      // pFences
                VK_TRUE,    // waitAll
                UINT64_MAX  // timeout
            );
        }

        idle = fence;
    }

    VkResult Swapchain::present(Queue const& queue, std::vector<VkSemaphore> const& wait_semaphores) const
    {
        VkPresentInfoKHR info = {};
        info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        info.pNext = {};
        info.waitSemaphoreCount = static_cast<uint32_t>(wait_semaphores.size());
        info.pWaitSemaphores = wait_semaphores.data();
        info.swapchainCount = 1;
        info.pSwapchains = &swapchain_;
        info.pImageIndices = &image_index_;
        info.pResults = {};

        VkResult res = vkQueuePresentKHR(queue.handle(), &info);
        if (res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR && res != VK_ERROR_OUT_OF_DATE_KHR) {
            throw Exception{"Vulkan failed to present swapchain image."};
        }

        return res;
    }

    std::vector<VkImage> const& Swapchain::images() const noexcept
    {
        return images_;
    }

    uint32_t Swapchain::image_count() const noexcept
    {
        return static_cast<uint32_t>(images_.size());
    }

    uint32_t Swapchain::image_index() const noexcept
    {
        return image_index_;
    }

    VkExtent2D const& Swapchain::image_extent() const noexcept
    {
        return image_extent_;
    }

    VkSurfaceFormatKHR const& Swapchain::image_format() const noexcept
    {
        return image_format_;
    }

    VkSwapchainKHR Swapchain::handle() const noexcept
    {
        return swapchain_;
    }

    void Swapchain::create()
    {
        VkSwapchainCreateInfoKHR info = {};
        info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        info.pNext = {};
        info.flags = {};
        info.surface = surface_;
        info.minImageCount = static_cast<uint32_t>(images_.size());
        info.imageFormat = image_format_.format;
        info.imageColorSpace = image_format_.colorSpace;
        info.imageExtent = image_extent_;
        info.imageArrayLayers = 1;
        info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

        if (queue_family_indices_.size() == 1) {
            info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        }
        else {
            info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            info.queueFamilyIndexCount = static_cast<uint32_t>(queue_family_indices_.size());
            info.pQueueFamilyIndices = queue_family_indices_.data();
        }

        info.preTransform = pre_transform_;
        info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        info.presentMode = present_mode_;
        info.clipped = VK_TRUE;

        VkSwapchainKHR old_swapchain = swapchain_;
        info.oldSwapchain = old_swapchain;

        VkResult result = vkCreateSwapchainKHR(device_, &info, nullptr, &swapchain_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create swapchain."};
        }

        uint32_t count = static_cast<uint32_t>(images_.size());
        vkGetSwapchainImagesKHR(device_, swapchain_, &count, nullptr);
        images_.resize(count);
        vkGetSwapchainImagesKHR(device_, swapchain_, &count, images_.data());

        idle_images_.resize(count, VK_NULL_HANDLE);
    }
}
