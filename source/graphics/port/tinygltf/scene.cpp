#include "chapar/graphics/port/tinygltf/scene.hpp"
#include "chapar/graphics/port/tinygltf/image.hpp"
#include "chapar/graphics/port/tinygltf/mesh.hpp"
#include "chapar/core/error.hpp"
#include "chapar/vulkan/image.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <tiny_gltf.h>

namespace chapar::graphics::tinygltf_port {
    namespace details {}
    namespace details {}

    // ==================================================
    // Loads TinyGLTF nodes.
    // ==================================================

    RawNode load_node(tinygltf::Model const& tiny_model, tinygltf::Node const& tiny_node)
    {
        RawNode node;
        for (auto tiny_child_index : tiny_node.children) {
            auto const& tiny_child = tiny_model.nodes[tiny_child_index];
            node.children.push_back(load_node(tiny_model, tiny_child));
        }

        if (tiny_node.mesh >= 0) {
            node.mesh_index = tiny_node.mesh;
        }

        return node;
    }





    // ==================================================
    // Loads TinyGLTF images.
    // ==================================================

    std::unique_ptr<GltfImage> load_image(tinygltf::Image const& tiny_image)
    {
        return std::make_unique<GltfImage>(static_cast<uint32_t>(tiny_image.width), static_cast<uint32_t>(tiny_image.height), reinterpret_cast<std::byte const*>(tiny_image.image.data()), tiny_image.image.size());
    }




    // ==================================================
    // Loads TinyGLTF sampler create infos.
    // ==================================================

    std::optional<VkFilter> filter_mode(int code) noexcept
    {
        switch (code) {
        case 9728: // NEAREST
            return VK_FILTER_NEAREST;
        case 9729: // LINEAR
            return VK_FILTER_LINEAR;
        case 9984: // NEAREST_MIPMAP_NEAREST
            return VK_FILTER_NEAREST;
        case 9985: // LINEAR_MIPMAP_NEAREST
            return VK_FILTER_NEAREST;
        case 9986: // NEAREST_MIPMAP_LINEAR
            return VK_FILTER_LINEAR;
        case 9987: // LINEAR_MIPMAP_LINEAR
            return VK_FILTER_LINEAR;
        default:
            return std::nullopt;
        }
    }

    std::optional<VkSamplerAddressMode> address_mode(int code) noexcept
    {
        switch (code) {
        case 10497: // REPEAT
            return VK_SAMPLER_ADDRESS_MODE_REPEAT;
        case 33071: // CLAMP_TO_EDGE
            return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        case 33648: // MIRRORED_REPEAT
            return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
        default:
            return std::nullopt;
        }
    }

    VkSamplerCreateInfo load_sampler_create_info(tinygltf::Sampler const& tiny_sampler)
    {
        auto minFilter = filter_mode(tiny_sampler.minFilter);
        auto magFilter = filter_mode(tiny_sampler.magFilter);
        auto addressModeU = address_mode(tiny_sampler.wrapS);
        auto addressModeV = address_mode(tiny_sampler.wrapT);
        auto addressModeW = address_mode(tiny_sampler.wrapR);

        auto info = vulkan::Sampler::create_info();
        info.minFilter = minFilter ? *minFilter : info.minFilter;
        info.magFilter = magFilter ? *magFilter : info.magFilter;
        info.addressModeU = addressModeU ? *addressModeU : info.addressModeU;
        info.addressModeV = addressModeV ? *addressModeV : info.addressModeV;
        info.addressModeW = addressModeW ? *addressModeW : info.addressModeW;

        return info;
    }





    // ==================================================
    // Loads TinyGLTF textures.
    // ==================================================

    RawTexture load_texture(tinygltf::Texture const& tiny_texture)
    {
        RawTexture texture;
        texture.image_index = static_cast<std::size_t>(tiny_texture.source);
        if (tiny_texture.sampler >= 0) {
            texture.sampler_index = tiny_texture.sampler;
        }

        return texture;
    }





    // ==================================================
    // Loads TinyGLTF scene.
    // ==================================================

    RawScene load_scene(
        gsl::czstring filename,
        std::vector<VertexComponent> const& vertex_components
    )
    {
        tinygltf::Model tiny_model;
        tinygltf::TinyGLTF tiny_ctx;
        std::string error;
        std::string warning;

        bool loaded = tiny_ctx.LoadASCIIFromFile(&tiny_model, &error, &warning, filename);
        if (!loaded) {
            throw Exception{"GLTF failed to load file."};
        }

        auto const& tiny_scene = tiny_model.scenes[tiny_model.defaultScene > -1 ? tiny_model.defaultScene : 0];

        RawScene scene;

        // Create meshes.
        for (auto const& tiny_mesh : tiny_model.meshes) {
            scene.meshes.push_back(load_mesh(tiny_model, tiny_mesh, vertex_components));
        }

        // Create nodes.
        for (auto tiny_node_index : tiny_scene.nodes) {
            auto const& tiny_node = tiny_model.nodes[tiny_node_index];
            scene.nodes.push_back(load_node(tiny_model, tiny_node));
        }

        // Create images.
        for (auto const& tiny_image : tiny_model.images) {
            scene.images.push_back(load_image(tiny_image));
        }

        // Create sampler create infos.
        for (auto const& tiny_sampler : tiny_model.samplers) {
            scene.sampler_create_infos.push_back(load_sampler_create_info(tiny_sampler));
        }

        // Create textures.
        for (auto const& tiny_texture : tiny_model.textures) {
            scene.textures.push_back(load_texture(tiny_texture));
        }

        return scene;
    }
}
