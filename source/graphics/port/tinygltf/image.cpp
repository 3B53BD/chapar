#include "chapar/graphics/port/tinygltf/image.hpp"
#include <tiny_gltf.h>

#include <iostream>

namespace chapar::graphics::tinygltf_port {
    namespace details {}
    namespace details {}

    GltfImage::GltfImage(uint32_t width, uint32_t height, std::byte const* data, std::size_t size)
    {
        width_ = width;
        height_ = height;
        data_ = data;
        size_ = size;
    }

    uint32_t GltfImage::width() const noexcept
    {
        return width_;
    }

    uint32_t GltfImage::height() const noexcept
    {
        return height_;
    }

    uint32_t GltfImage::mip_levels() const noexcept
    {
        return static_cast<uint32_t>(std::floor(std::log2(std::max(width_, height_))) + 1.0f);;
    }

    std::byte const* GltfImage::data() const noexcept
    {
        return data_;
    }

    std::size_t GltfImage::size() const noexcept
    {
        return size_;
    }

    std::optional<std::size_t> GltfImage::offset(uint32_t mip_level) const noexcept
    {
        return std::nullopt;
    }
}
