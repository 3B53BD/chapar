#include "chapar/graphics/port/tinygltf/mesh.hpp"
#include "chapar/core/error.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <gsl-lite/gsl-lite.hpp>
#include <tiny_gltf.h>
#include <cstddef>
#include <cstdint>

namespace chapar::graphics::tinygltf_port {
    namespace details {}
    namespace details {}

    // ==================================================
    // PositionAccessor.
    // ==================================================

    struct PositionAccessor {
        float const* data = nullptr;
        std::size_t count = 0;
        int stride = 0;
    };

    PositionAccessor position_accessor(
        tinygltf::Model const& tiny_model,
        tinygltf::Primitive const& tiny_primitive
    )
    {
        PositionAccessor acc;
        if (tiny_primitive.attributes.contains("POSITION")) {
            auto index = tiny_primitive.attributes.at("POSITION");
            auto const& tacc = tiny_model.accessors[index];
            auto const& tview = tiny_model.bufferViews[tacc.bufferView];
            auto const& tbuf = tiny_model.buffers[tview.buffer];

            auto offset = tacc.byteOffset + tview.byteOffset;
            auto stride = tacc.ByteStride(tview);

            acc.data = reinterpret_cast<float const*>(&tbuf.data[offset]);
            acc.count = tacc.count;
            acc.stride = stride != -1 ? stride / sizeof(float) : 3;
        }
        else {
            throw Exception{"Vertex must contain position"};
        }

        return acc;
    }





    // ==================================================
    // NormalAccessor.
    // ==================================================

    struct NormalAccessor {
        float const* data = nullptr;
        int stride = 0;
    };

    NormalAccessor normal_accessor(
        tinygltf::Model const& tiny_model,
        tinygltf::Primitive const& tiny_primitive
    )
    {
        NormalAccessor acc;
        if (tiny_primitive.attributes.contains("NORMAL")) {
            auto index = tiny_primitive.attributes.at("NORMAL");
            auto const& tacc = tiny_model.accessors[index];
            auto const& tview = tiny_model.bufferViews[tacc.bufferView];
            auto const& tbuf = tiny_model.buffers[tview.buffer];

            auto offset = tacc.byteOffset + tview.byteOffset;
            auto stride = tacc.ByteStride(tview);

            acc.data = reinterpret_cast<float const*>(&tbuf.data[offset]);
            acc.stride = stride != -1 ? stride / sizeof(float) : 3;
        }

        return acc;
    }





    // ==================================================
    // TexCoordAccessor.
    // ==================================================

    struct TexCoordAccessor {
        float const* data = nullptr;
        int stride = 0;
    };

    TexCoordAccessor tex_coord_accessor(
        tinygltf::Model const& tiny_model,
        tinygltf::Primitive const& tiny_primitive
    )
    {
        TexCoordAccessor acc;
        if (tiny_primitive.attributes.contains("TEXCOORD_0")) {
            auto index = tiny_primitive.attributes.at("TEXCOORD_0");
            auto const& tacc = tiny_model.accessors[index];
            auto const& tview = tiny_model.bufferViews[tacc.bufferView];
            auto const& tbuf = tiny_model.buffers[tview.buffer];

            auto offset = tacc.byteOffset + tview.byteOffset;
            auto stride = tacc.ByteStride(tview);

            acc.data = reinterpret_cast<float const*>(&tbuf.data[offset]);
            acc.stride = stride != -1 ? stride / sizeof(float) : 2;
        }

        return acc;
    }





    // ==================================================
    // ColorAccessor.
    // ==================================================

    struct ColorAccessor {
        float const* data = nullptr;
        int stride = 0;
    };

    ColorAccessor color_accessor(
        tinygltf::Model const& tiny_model,
        tinygltf::Primitive const& tiny_primitive
    )
    {
        ColorAccessor acc;
        if (tiny_primitive.attributes.contains("COLOR_0")) {
            auto index = tiny_primitive.attributes.at("COLOR_0");
            auto const& tacc = tiny_model.accessors[index];
            auto const& tview = tiny_model.bufferViews[tacc.bufferView];
            auto const& tbuf = tiny_model.buffers[tview.buffer];

            auto offset = tacc.byteOffset + tview.byteOffset;
            auto stride = tacc.ByteStride(tview);

            acc.data = reinterpret_cast<float const*>(&tbuf.data[offset]);
            acc.stride = stride != -1 ? stride / sizeof(float) : 3;
        }

        return acc;
    }





    // ==================================================
    // map_vertices.
    // ==================================================

    std::size_t vertices_stride(std::vector<VertexComponent> const& components)
    {
        std::size_t stride = 0;
        for (auto const& component : components) {
            switch (component) {
            case VertexComponent::position:
                stride += sizeof(glm::vec3);
                break;
            case VertexComponent::normal:
                stride += sizeof(glm::vec3);
                break;
            case VertexComponent::texcoord:
                stride += sizeof(glm::vec2);
                break;
            case VertexComponent::color:
                stride += sizeof(glm::vec3);
                break;
            }
        }

        return stride;
    }

    std::vector<std::byte> map_vertices(
        std::vector<VertexComponent> const& components,
        PositionAccessor const& pos_acc,
        NormalAccessor const& normal_acc,
        TexCoordAccessor const& texcoord_acc,
        ColorAccessor const& color_acc,
        std::size_t stride
    )
    {
        std::vector<std::byte> vertices(stride * pos_acc.count);
        std::size_t offset = 0;
        for (gsl::index i = 0; i < pos_acc.count; ++i) {
            for (auto const& component : components) {
                switch (component) {
                    case VertexComponent::position: {
                        auto x = pos_acc.data[i * pos_acc.stride + 0];
                        auto y = pos_acc.data[i * pos_acc.stride + 1];
                        auto z = pos_acc.data[i * pos_acc.stride + 2];
                        auto val = glm::vec3(x, y, z);
                        std::memcpy(&vertices[offset], &val, sizeof(val));
                        offset += sizeof(val);
                        break;
                    }
                    case VertexComponent::normal: {
                        glm::vec3 val;
                        if (normal_acc.data) {
                            auto x = normal_acc.data[i * normal_acc.stride + 0];
                            auto y = normal_acc.data[i * normal_acc.stride + 1];
                            auto z = normal_acc.data[i * normal_acc.stride + 2];
                            val = glm::vec3(x, y, z);
                        }
                        else {
                            val = glm::vec3(0.0f);
                        }
                        std::memcpy(&vertices[offset], &val, sizeof(val));
                        offset += sizeof(val);
                        break;
                    }
                    case VertexComponent::texcoord: {
                        glm::vec2 val;
                        if (texcoord_acc.data) {
                            auto x = texcoord_acc.data[i * texcoord_acc.stride + 0];
                            auto y = texcoord_acc.data[i * texcoord_acc.stride + 1];
                            val = glm::vec2(x, y);
                        }
                        else {
                            val = glm::vec2(0.0f);
                        }
                        std::memcpy(&vertices[offset], &val, sizeof(val));
                        offset += sizeof(val);
                        break;
                    }
                    case VertexComponent::color: {
                        glm::vec3 val;
                        if (color_acc.data) {
                            auto x = color_acc.data[i * color_acc.stride + 0];
                            auto y = color_acc.data[i * color_acc.stride + 1];
                            auto z = color_acc.data[i * color_acc.stride + 2];
                            val = glm::vec3(x, y, z);
                        }
                        else {
                            val = glm::vec3(1.0f, 0.0f, 0.0f);
                        }
                        std::memcpy(&vertices[offset], &val, sizeof(val));
                        offset += sizeof(val);
                        break;
                    }
                }
            }
        }

        return vertices;
    }





    // ==================================================
    // map_indices.
    // ==================================================

    std::vector<std::byte> map_indices(
        tinygltf::Model const& tiny_model,
        tinygltf::Primitive const& tiny_primitive,
        uint32_t& index_count,
        VkIndexType& index_type
    )
    {
        auto const& tacc = tiny_model.accessors[tiny_primitive.indices];
        index_count = tacc.count;

        std::size_t size = 0;
        switch (tacc.componentType) {
        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT:
            size = tacc.count * sizeof(uint32_t);
            index_type = VK_INDEX_TYPE_UINT32;
            break;
        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT:
            size = tacc.count * sizeof(uint16_t);
            index_type = VK_INDEX_TYPE_UINT16;
            break;
        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE:
            throw Exception{"Index type is byte long."};
            // size = tacc.count * sizeof(uint8_t);
            // index_type = VK_INDEX_TYPE_UINT8_EXT;
            // break;
        }

        auto const& tview = tiny_model.bufferViews[tacc.bufferView];
        auto const& tbuf = tiny_model.buffers[tview.buffer];
        void const* data = &tbuf.data[tacc.byteOffset + tview.byteOffset];

        std::vector<std::byte> indices(size);
        std::size_t offset = 0;
        switch (tacc.componentType) {
        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT:
            for (gsl::index i = 0; i < tacc.count; ++i) {
                auto val = reinterpret_cast<uint32_t const*>(data)[i];
                std::memcpy(&indices[offset], &val, sizeof(val));
                offset += sizeof(val);
            }
            break;
        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT:
            for (gsl::index i = 0; i < tacc.count; ++i) {
                auto val = reinterpret_cast<uint16_t const*>(data)[i];
                std::memcpy(&indices[offset], &val, sizeof(val));
                offset += sizeof(val);
            }
            break;
        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE:
            for (gsl::index i = 0; i < tacc.count; ++i) {
                auto val = reinterpret_cast<uint8_t const*>(data)[i];
                std::memcpy(&indices[offset], &val, sizeof(val));
                offset += sizeof(val);
            }
            break;
        }

        return indices;
    }





    // ==================================================
    // load_mesh.
    // ==================================================

    RawMesh load_mesh(
        tinygltf::Model const& tiny_model,
        tinygltf::Mesh const& tiny_mesh,
        std::vector<VertexComponent> const& components
    )
    {
        auto stride = vertices_stride(components);

        RawMesh mesh;
        for (auto const& tiny_primitive : tiny_mesh.primitives) {
            auto pos_acc = position_accessor(tiny_model, tiny_primitive);
            auto normal_acc = normal_accessor(tiny_model, tiny_primitive);
            auto texcoord_acc = tex_coord_accessor(tiny_model, tiny_primitive);
            auto color_acc = color_accessor(tiny_model, tiny_primitive);

            RawPrimitive primitive;
            primitive.vertices = map_vertices(components, pos_acc, normal_acc, texcoord_acc, color_acc, stride);
            primitive.vertex_count = pos_acc.count;
            if (tiny_primitive.indices >= 0) {
                primitive.indices = map_indices(tiny_model, tiny_primitive, primitive.index_count, primitive.index_type);
            }

            // fmt::print("Primitive: vertices={} indices={} vertex_count={} index_count={}", primitive.vertices.size(), primitive.indices.size(), primitive.vertex_count, primitive.index_count);

            mesh.primitives.push_back(std::move(primitive));
        }

        return mesh;
    }
}
