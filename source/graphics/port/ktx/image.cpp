#include "chapar/graphics/port/ktx/image.hpp"
#include "chapar/core/error.hpp"
#include <ktx.h>

namespace chapar::graphics {

    KtxImage::KtxImage(gsl::czstring filename)
    {
        ktxResult result = ktxTexture_CreateFromNamedFile(filename, KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &texture_);
        if (result != KTX_SUCCESS) {
            throw Exception{"KTX failed to create texture."};
        }
    }

    KtxImage::~KtxImage() noexcept
    {
        ktxTexture_Destroy(texture_);
    }

    uint32_t KtxImage::width() const noexcept
    {
        return texture_->baseWidth;
    }

    uint32_t KtxImage::height() const noexcept
    {
        return texture_->baseHeight;
    }

    uint32_t KtxImage::mip_levels() const noexcept
    {
        return texture_->numLevels;
    }

    std::byte const* KtxImage::data() const noexcept
    {
        return reinterpret_cast<std::byte*>(ktxTexture_GetData(texture_));
    }

    std::size_t KtxImage::size() const noexcept
    {
         return static_cast<std::size_t>(ktxTexture_GetDataSize(texture_));
    }

    std::optional<std::size_t> KtxImage::offset(uint32_t mip_level) const noexcept
    {
        ktx_size_t offset;
        KTX_error_code result = ktxTexture_GetImageOffset(texture_, mip_level, 0, 0, &offset);
        if (!result) {
            return std::nullopt;
        }

        return static_cast<std::size_t>(offset);
    }
}
