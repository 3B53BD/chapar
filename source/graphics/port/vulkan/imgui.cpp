#include "chapar/graphics/port/vulkan/imgui.hpp"
#include "chapar/sdl/window.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/descriptor.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/framebuffer.hpp"
#include "chapar/vulkan/instance.hpp"
#include "chapar/vulkan/physical_device.hpp"
#include "chapar/vulkan/queue.hpp"
#include "chapar/vulkan/render_pass.hpp"
#include "chapar/vulkan/swapchain.hpp"
#include <iostream>

namespace chapar::graphics::vulkan_port {
    namespace details {}
    namespace details {}

    void check_vk_result(VkResult err)
    {
        if (err == 0) return;
        std::cout << "[vulkan] Error: VkResult = " << err << "\n";
        if (err < 0) abort();
    }

    Imgui::Imgui(
        sdl::Window const& window,
        vulkan::Instance const& instance,
        vulkan::PhysicalDevice const& physical_device,
        vulkan::Device const& device,
        vulkan::Queue const& queue,
        vulkan::Swapchain const& swapchain,
        vulkan::RenderPass const& render_pass,
        vulkan::CommandPool const& command_pool
    )
    {
        window_ = window.handle();

        auto image_count = swapchain.image_count();
        auto format = swapchain.image_format();

        auto alloc_info = command_pool.allocate_info(1);
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
        command_buffers_.resize(image_count);
        for (auto& command_buffer : command_buffers_) {
            command_buffer = std::make_unique<vulkan::CommandBuffer>(command_pool, alloc_info);
        }

        std::vector<VkDescriptorPoolSize> pool_sizes =
        {
            { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
        };

        descriptor_pool_ = std::make_unique<vulkan::DescriptorPool>(device.handle(), image_count, pool_sizes);

        // Initializes ImGui state
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        ImGui::StyleColorsDark();

        // Binds ImGui to Vulkan
        ImGui_ImplSDL2_InitForVulkan(window_);
        ImGui_ImplVulkan_InitInfo info = {};
        info.Instance = instance.handle();
        info.PhysicalDevice = physical_device.handle();
        info.Device = device.handle();
        info.QueueFamily = queue.family();
        info.Queue = queue.handle();
        info.PipelineCache = VK_NULL_HANDLE;
        info.DescriptorPool = descriptor_pool_->handle();
        info.Subpass = 1;
        info.MinImageCount = image_count;
        info.ImageCount = image_count;
        info.MSAASamples = VK_SAMPLE_COUNT_1_BIT;
        info.Allocator = VK_NULL_HANDLE;
        info.CheckVkResultFn = check_vk_result;
        ImGui_ImplVulkan_Init(&info, render_pass.handle());

        // Creates fonts
        auto submit = std::make_unique<vulkan::OneTimeSubmit>(device.handle(), queue);
        ImGui_ImplVulkan_CreateFontsTexture(submit->command_buffer_handle());
        submit.reset();
        ImGui_ImplVulkan_DestroyFontUploadObjects();
    }

    Imgui::~Imgui() noexcept
    {
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplSDL2_Shutdown();
        ImGui::DestroyContext();

        descriptor_pool_.reset();
        command_buffers_.clear();
    }

    void Imgui::process_event(SDL_Event* event)
    {
        ImGui_ImplSDL2_ProcessEvent(event);
    }

    void Imgui::new_frame(float* val)
    {
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplSDL2_NewFrame(window_);
        ImGui::NewFrame();

        ImGui::Begin("Hello, world!");
            ImGui::SliderFloat("rotation", val, -0.5f, 0.5f);
        ImGui::End();

        ImGui::Render();
    }

    VkCommandBuffer Imgui::record(VkCommandBufferInheritanceInfo const& info, std::size_t buffer_index)
    {
        auto const& cb = command_buffers_[buffer_index];
        vkResetCommandBuffer(cb->handle(), {});

        auto begin_info = vulkan::CommandBuffer::begin_info();
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
        begin_info.pInheritanceInfo = &info;
        cb->begin(begin_info);

        ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), cb->handle());

        cb->end();

        return cb->handle();
    }
}
