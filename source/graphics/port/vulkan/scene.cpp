#include "chapar/graphics/port/vulkan/scene.hpp"
#include <chapar/graphics/raw_scene.hpp>
#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/buffer.hpp"
#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/descriptor.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/image.hpp"
#include "chapar/vulkan/queue.hpp"
#include "chapar/vulkan/scene.hpp"
#include "chapar/vulkan/swapchain.hpp"
#include <gsl-lite/gsl-lite.hpp>

#include <iostream>

namespace chapar::graphics::vulkan_port {
    namespace details {}
    namespace details {}

    void create_meshes(SceneCreateInfo const& ci, vulkan::Scene& scene);
    uint32_t create_nodes(SceneCreateInfo const& ci, vulkan::Scene& scene);
    uint32_t create_node(
        SceneCreateInfo const& ci,
        RawNode const& node,
        vulkan::Scene& scene,
        vulkan::Node& s_node
    );
    void set_parent(vulkan::Node& node);
    void create_textures(SceneCreateInfo const& ci, vulkan::Scene& scene);
    std::shared_ptr<vulkan::Image> create_image(
        SceneCreateInfo const& ci,
        RawImage const& image,
        VkFormat format,
        VkImageUsageFlags usage,
        VkImageLayout final_layout
    );
    void create_descriptor_pool(
        SceneCreateInfo const& ci,
        uint32_t uniform_count,
        vulkan::Scene& scene
    );
    void create_secondary_command_buffers(
        SceneCreateInfo const& ci,
        vulkan::Scene& scene
    );





    std::unique_ptr<vulkan::Scene> create_scene(SceneCreateInfo const& ci)
    {
        auto scene = std::make_unique<vulkan::Scene>();
        scene->device = ci.device;
        create_meshes(ci, *scene);
        auto link_count = create_nodes(ci, *scene);
        create_textures(ci, *scene);
        create_descriptor_pool(ci, link_count * ci.image_count, *scene);
        create_secondary_command_buffers(ci, *scene);

        return scene;
    }

    void create_meshes(SceneCreateInfo const& ci, vulkan::Scene& scene)
    {
        auto const& meshes = ci.data->meshes;
        scene.meshes.resize(meshes.size());

        for (gsl::index mi = 0; mi < meshes.size(); ++mi) {
            auto const& mesh = meshes[mi];
            auto& s_meshes = scene.meshes[mi];
            s_meshes.primitives.resize(mesh.primitives.size());

            for (gsl::index pi = 0; pi < mesh.primitives.size(); ++pi) {
                auto const& prim = mesh.primitives[pi];
                auto& s_prim = s_meshes.primitives[pi];

                if (prim.indices.empty()) {
                    // Create primitive without indices.
                     s_prim.buffer = std::make_unique<vulkan::VertexBuffer>(ci.device, ci.allocator, *ci.transfer_queue, prim.vertices, prim.vertex_count);
                }
                else {
                    // Crete primitive with indices.
                    s_prim.buffer =std::make_unique<vulkan::IndexVertexBuffer>(ci.device, ci.allocator, *ci.transfer_queue, prim.vertices, prim.indices, prim.index_count, prim.index_type);
                }
            }
        }
    }

    /**
     * Creates nodes from provided information in SceneCreateInfo.
     *
     * Scene must already contain valid meshes.
     *
     * @param[in]   ci    Scene creation information structure.
     * @param[out]  scene Container for nodes.
     * @return            Number of meshes that can be used to allocate enough
     *                    space for descriptor pool's uniform buffers.
     */
    uint32_t create_nodes(SceneCreateInfo const& ci, vulkan::Scene& scene)
    {
        uint32_t count = 0;
        auto const& nodes = ci.data->nodes;

        scene.nodes.resize(nodes.size());
        for (gsl::index i = 0; i < nodes.size(); ++i) {
            count += create_node(ci, nodes[i], scene, scene.nodes[i]);
        }

        for (auto& node : scene.nodes) {
            set_parent(node);
        }

        return count;
    }

    uint32_t create_node(
        SceneCreateInfo const& ci,
        RawNode const& node,
        vulkan::Scene& scene,
        vulkan::Node& s_node
    )
    {
        uint32_t link_count = 0;
        s_node.children.resize(node.children.size());

        // Iterates over children.
        for (gsl::index i = 0; i < node.children.size(); ++i) {
            link_count += create_node(ci, node.children[i], scene, s_node.children[i]);
        }

        if (node.mesh_index) {
            // Links to mesh.
            s_node.mesh = &scene.meshes[*node.mesh_index];
            ++link_count;

            // Creates uniform buffers for mesh for each swapchain image.
            s_node.uniforms.resize(ci.image_count);
            for (gsl::index i = 0; i < ci.image_count; ++i) {
                s_node.uniforms[i].buffer = std::make_unique<vulkan::UniformBuffer>(ci.allocator, sizeof(vulkan::Node::UBO));
            }
        }

        return link_count;
    }

    void set_parent(vulkan::Node& node)
    {
        for (auto& child : node.children) {
            child.parent = &node;
            set_parent(child);
        }
    }

    void create_textures(SceneCreateInfo const& ci, vulkan::Scene& scene)
    {
        // Creates images.
        auto const& images = ci.data->images;
        using SImage = std::shared_ptr<vulkan::Image>;
        std::vector<SImage> s_images(images.size());

        for (gsl::index i = 0; i < images.size(); ++i) {
            s_images[i] = create_image(ci, *images[i], VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        }

        // Creates samplers.
        auto const& sampler_cis = ci.data->sampler_create_infos;
        using SSampler = std::shared_ptr<vulkan::Sampler>;
        std::vector<SSampler> s_samplers(sampler_cis.size());
        std::shared_ptr<vulkan::Sampler> default_sampler;

        for (gsl::index i = 0; i < sampler_cis.size(); ++i) {
            s_samplers[i] = std::make_shared<vulkan::Sampler>(ci.device, sampler_cis[i]);
        }

        // Creates textures.
        auto const& textures = ci.data->textures;
        scene.textures.resize(textures.size());

        for (gsl::index i = 0; i < textures.size(); ++i) {
            auto const& tex = textures[i];
            auto& s_tex = scene.textures[i];

            // Assigns image.
            s_tex.image = s_images[tex.image_index];

            // Creates image view.
            s_tex.image_view = std::make_shared<vulkan::ImageView>(ci.device, *s_tex.image, VK_IMAGE_ASPECT_COLOR_BIT);

            // Creates or assigns sampler.
            // If sampler create info is not provided, then use default one.
            if (tex.sampler_index) {
                s_tex.sampler = s_samplers[*tex.sampler_index];
            }
            else if (default_sampler) {
                s_tex.sampler = default_sampler;
            }
            else {
                default_sampler = std::make_shared<vulkan::Sampler>(ci.device, vulkan::Sampler::create_info());
                s_tex.sampler = default_sampler;
            }
        }
    }

    std::shared_ptr<vulkan::Image> create_image(
        SceneCreateInfo const& ci,
        RawImage const& image,
        VkFormat format,
        VkImageUsageFlags usage,
        VkImageLayout final_layout
    )
    {
        // Creates staging buffer.
        auto bci = vulkan::Buffer::create_info();
        bci.size = image.size();
        bci.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

        auto aci = vulkan::Allocator::allocation_create_info();
        aci.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;

        auto buffer = std::make_unique<vulkan::Buffer>(ci.allocator, bci, aci);
        auto offset = image.offset(0);

        // Copies data to buffer.
        buffer->map();
        buffer->memcpy(offset ? *offset : 0, std::span{image.data(), image.size()});
        buffer->unmap();

        // Creates image.
        auto width = image.width();
        auto height = image.height();

        auto ici = vulkan::Image::create_info();
        ici.format = format;
        ici.extent = {width, height, 1};
        ici.mipLevels = image.mip_levels();
        ici.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

        aci.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        auto s_image = std::make_shared<vulkan::Image>(ci.allocator, ici, aci);

        // Copies buffer to image.
        std::vector<VkBufferImageCopy> regions(1);
        regions[0].imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        regions[0].imageSubresource.mipLevel = 0;
        regions[0].imageSubresource.baseArrayLayer = 0;
        regions[0].imageSubresource.layerCount = 1;
        regions[0].imageExtent.width = width;
        regions[0].imageExtent.height = height;
        regions[0].imageExtent.depth = 1;
        regions[0].bufferOffset = 0;

        auto submit = std::make_unique<vulkan::OneTimeSubmit>(ci.device, *ci.transfer_queue);

        auto cmd_buf = submit->command_buffer();
        s_image->transition_layout(*cmd_buf, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        cmd_buf->copy_to(*buffer, *s_image, regions);
        s_image->transition_layout(*cmd_buf, final_layout);

        submit.reset();

        return s_image;
    }

    void create_descriptor_pool(
        SceneCreateInfo const& ci,
        uint32_t uniform_count,
        vulkan::Scene& scene
    )
    {
        std::vector<VkDescriptorPoolSize> pool_sizes;
        if (uniform_count != 0) {
            pool_sizes.push_back({VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, uniform_count});
        }
        if (!scene.textures.empty()) {
            pool_sizes.push_back({VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, static_cast<uint32_t>(scene.textures.size())});
        }

        scene.descriptor_pool = std::make_unique<vulkan::DescriptorPool>(ci.device, ci.image_count, pool_sizes);
    }

    void create_secondary_command_buffers(
        SceneCreateInfo const& ci,
        vulkan::Scene& scene
    )
    {
        auto info = ci.pool->allocate_info(1);
        info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
        scene.command_buffers.resize(ci.image_count);
        for (auto& command_buffer : scene.command_buffers) {
            command_buffer = std::make_unique<vulkan::CommandBuffer>(*ci.pool, info);
        }
    }
}
