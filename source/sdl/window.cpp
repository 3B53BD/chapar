#include "chapar/sdl/window.hpp"
#include "chapar/core/error.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

namespace chapar::sdl {

    std::unique_ptr<Window> Window::vulkan()
    {
        return std::make_unique<Window>(SDL_WINDOW_VULKAN);
    }

    Window::Window(uint32_t flags)
    {
        flags |= SDL_WINDOW_ALLOW_HIGHDPI;
        window_ = SDL_CreateWindow("SDL window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, flags);
        if (!window_) {
            throw Exception{"SDL failed to create window"};
        }
    }

    Window::~Window() noexcept
    {
        SDL_DestroyWindow(window_);
    }

    std::string Window::title() const noexcept
    {
        return SDL_GetWindowTitle(window_);
    }

    std::pair<int, int> Window::origin() const noexcept
    {
        int x = 0;
        int y = 0;
        SDL_GetWindowPosition(window_, &x, &y);
        return std::make_pair(x, y);
    }

    std::pair<int, int> Window::extent() const noexcept
    {
        int width;
        int height;
        SDL_GetWindowSize(window_, &width, &height);
        return std::make_pair(width, height);
    }

    bool Window::is_fullscreen() const noexcept
    {
        return status_.test(fullscreen_);
    }

    bool Window::is_focused() const noexcept
    {
        return status_.test(focused_);
    }

    bool Window::is_resizable() const noexcept
    {
        return status_.test(resizable_);
    }

    void Window::set_origin(int x, int y) const noexcept
    {
        SDL_SetWindowPosition(window_, x, y);
    }

    void Window::set_extent(int width, int height) const noexcept
    {
        SDL_SetWindowSize(window_, width, height);
    }

    void Window::set_resizable(bool resizable) noexcept
    {
        SDL_SetWindowResizable(window_, resizable ? SDL_TRUE : SDL_FALSE);
        status_.set(resizable_);
    }

    void Window::set_fullscreen(WindowFullscreanMode mode) noexcept
    {
        uint32_t flags = 0;
        switch (mode) {
        case WindowFullscreanMode::real:
            flags = SDL_WINDOW_FULLSCREEN;
            break;
        case WindowFullscreanMode::fake:
            flags = SDL_WINDOW_FULLSCREEN_DESKTOP;
            break;
        case WindowFullscreanMode::off:
            flags = 0;
        default:
            flags = 0;
        }

        SDL_SetWindowFullscreen(window_, flags);
        status_.set(fullscreen_);
    }

    void Window::set_maximize() const noexcept
    {
        SDL_MaximizeWindow(window_);
    }

    void Window::set_minimize() const noexcept
    {
        SDL_MinimizeWindow(window_);
    }

    void Window::set_focus() noexcept
    {
        SDL_SetWindowInputFocus(window_);
    }

    void Window::set_title(std::string_view title) const noexcept
    {
        SDL_SetWindowTitle(window_, title.data());
    }

    std::vector<gsl::czstring> Window::extensions() const noexcept
    {
        uint32_t count;
        SDL_Vulkan_GetInstanceExtensions(window_, &count, nullptr);
        std::vector<gsl::czstring> extensions(count);
        SDL_Vulkan_GetInstanceExtensions(window_, &count, extensions.data());

        return extensions;
    }

    std::pair<VkSurfaceKHR, bool> Window::surface(VkInstance instance) const noexcept
    {
        VkSurfaceKHR surface;
        SDL_bool result = SDL_Vulkan_CreateSurface(window_, instance, &surface);

        return std::make_pair(surface, result);
    }
}
