#include "chapar/lua/ref.hpp"
#include <string>

// nil | boolean | number | string | function | userdata | thread | table
// integer | float
// full userdata | light userdata

namespace chapar::lua {

    Ref::Ref(Stack::Id id, Stack* stack, lua_State* L) noexcept
        : id_{id}
        , stack_{stack}
        , L_{L}
    { }

    Ref::Ref(Ref&& other) noexcept
    {
        id_ = std::exchange(other.id_, 0);
        stack_ = std::exchange(other.stack_, nullptr);
        L_ = std::exchange(other.L_, nullptr);
    }

    Ref& Ref::operator=(Ref&& other) noexcept
    {
        if (this == &other) return *this;
        if (valid()) {
            lua_remove(L_, (*stack_)[id_].index);
            stack_->erase(id_);
        }
        id_ = std::exchange(other.id_, 0);
        stack_ = std::exchange(other.stack_, nullptr);
        L_ = std::exchange(other.L_, nullptr);
        return *this;
    }

    Ref::~Ref() noexcept
    {
        if (!valid()) return;
        lua_remove(L_, (*stack_)[id_].index);
        stack_->erase(id_);
    }

    Ref& Ref::operator=(lua_Integer n) noexcept
    {
        lua_pushinteger(L_, n);
        update();
        return *this;
    }

    Ref& Ref::operator=(gsl::czstring s) noexcept
    {
        lua_pushstring(L_, s);
        update();
        return *this;
    }

    Ref Ref::operator[](gsl::czstring key) noexcept
    {
        return getfield(key);
    }

    Ref Ref::operator[](lua_Integer i) noexcept
    {
        lua_geti(L_, (*stack_)[id_].index, i);
        return {stack_->push(std::to_string(i).c_str(), id_), stack_, L_};
    }

    bool Ref::to_boolean() const noexcept
    {
        return lua_toboolean(L_, (*stack_)[id_].index);
    }
    lua_Number Ref::to_number() const noexcept
    {
        return lua_tonumber(L_, (*stack_)[id_].index);
    }
    lua_Integer Ref::to_integer() const noexcept
    {
        return lua_tointeger(L_, (*stack_)[id_].index);
    }
    std::string Ref::to_string() const noexcept
    {
        std::size_t l;
        auto s = lua_tolstring(L_, (*stack_)[id_].index, &l);
        return {s, l};
    }

    bool Ref::is_nil() const noexcept
    {
        return lua_isnil(L_, (*stack_)[id_].index);
    }
    bool Ref::is_none() const noexcept
    {
        return lua_isnone(L_, (*stack_)[id_].index);
    }
    bool Ref::is_noneornil() const noexcept
    {
        return lua_isnoneornil(L_, (*stack_)[id_].index);
    }
    bool Ref::is_boolean() const noexcept
    {
        return lua_isboolean(L_, (*stack_)[id_].index);
    }
    bool Ref::is_number() const noexcept
    {
        return lua_isnumber(L_, (*stack_)[id_].index);
    }
    bool Ref::is_integer() const noexcept
    {
        return lua_isinteger(L_, (*stack_)[id_].index);
    }
    bool Ref::is_string() const noexcept
    {
        return lua_isstring(L_, (*stack_)[id_].index);
    }
    bool Ref::is_function() const noexcept
    {
        return lua_isfunction(L_, (*stack_)[id_].index);
    }
    bool Ref::is_userdata() const noexcept
    {
        return lua_isuserdata(L_, (*stack_)[id_].index);
    }
    bool Ref::is_lightuserdata() const noexcept
    {
        return lua_islightuserdata(L_, (*stack_)[id_].index);
    }
    bool Ref::is_thread() const noexcept
    {
        return lua_isthread(L_, (*stack_)[id_].index);
    }
    bool Ref::is_table() const noexcept
    {
        return lua_istable(L_, (*stack_)[id_].index);
    }

    lua_Integer Ref::len() const noexcept
    {
        lua_len(L_, (*stack_)[id_].index);
        auto len = lua_tointeger(L_, -1);
        lua_pop(L_, 1);

        return len;
    }

    RefIt Ref::begin() noexcept
    {
        return {this};
    }

    RefSentinel Ref::end() noexcept
    {
        return {};
    }

    std::string Ref::type() const noexcept
    {
        return lua_typename(L_, lua_type(L_, (*stack_)[id_].index));
    }

    bool Ref::valid() const noexcept
    {
        return id_ && stack_ && L_;
    }

    void Ref::update() noexcept
    {
        int id = stack_->push();

        Stack::Object const& o = (*stack_)[id_];
        if (o.parent) {
            lua_setfield(L_, o.index, o.key.data());
            *this = getfield(o.key.data());
        } else {
            lua_setglobal(L_, o.key.data());
            *this = getglobal(o.key.data());
        }

        stack_->erase(id);
    }

    Ref Ref::getglobal(gsl::czstring key) noexcept
    {
        lua_getglobal(L_, key);
        return {stack_->push(key), stack_, L_};
    }

    Ref Ref::getfield(gsl::czstring key) noexcept
    {
        lua_getfield(L_, (*stack_)[id_].index, key);
        return {stack_->push(key, id_), stack_, L_};
    }

    RefIt::RefIt(Ref* arr) noexcept
    {
        if (!arr->len()) {
            return;
        }

        elm_ = (*arr_)[idx_];
    }

    bool operator==(Ref const& lhs, Ref const& rhs) noexcept
    {
        return lhs.id_ == rhs.id_;
    }

    bool operator!=(Ref const& lhs, Ref const& rhs) noexcept
    {
        return !(lhs == rhs);
    }

    bool operator==(RefIt const& lhs, RefIt const& rhs) noexcept
    {
        return lhs.elm_ == rhs.elm_;
    }

    bool operator!=(RefIt const& lhs, RefIt const& rhs) noexcept
    {
        return !(lhs == rhs);
    }

    bool operator==(RefIt const& lhs, RefSentinel) noexcept
    {
        return lhs.idx_ >= lhs.arr_->len();
    }

    bool operator==(RefSentinel, RefIt const& rhs) noexcept
    {
        return (rhs == RefSentinel{});
    }

    bool operator!=(RefIt const& lhs, RefSentinel) noexcept
    {
        return !(lhs == RefSentinel{});
    }

    bool operator!=(RefSentinel, RefIt const& rhs) noexcept
    {
        return (rhs != RefSentinel{});
    }
}
