#include "chapar/lua/stack.hpp"

namespace chapar::lua {

    Stack::Object const& Stack::operator[](Id id) const
    {
        return objects_.at(id);
    }

    bool Stack::contains(Id id) const noexcept
    {
        return objects_.contains(id);
    }

    Stack::Id Stack::push(gsl::czstring key, Id parent)
    {
        int id = next_++;

        Object ob;
        ob.index = static_cast<int>(objects_.size()) + 1;
        ob.key = key;
        ob.parent = parent;
        ob.use_count = 1;

        if (parent && objects_.contains(parent)) {
            Object& po = objects_[parent];
            ++po.use_count;
        }

        objects_.emplace(id, std::move(ob));

        return id;
    }

    std::vector<int> Stack::erase(Id id)
    {
        if (!objects_.contains(id)) return {};

        Object* ob = &objects_[id];
        std::vector<int> to_rem;
        int top = static_cast<int>(objects_.size());

        while (true) {
            --ob->use_count;

            // Does not allow to remove object from stack if it is still used
            // by another one.
            if (ob->use_count) break;

            // Does not update indices of other objects right away but records
            // them for later.
            to_rem.push_back(ob->index);
            int pid = ob->parent;
            objects_.erase(id);

            if (!pid) break;

            id = pid;
            ob = &objects_[pid];
        }

        // Sorts indices of objects that were removed so it starts removal from
        // the end of stack to not mess up indices.
        std::sort(to_rem.begin(), to_rem.end(), std::greater<int>{});

        for (int i : to_rem) {
            for (auto& [_, ob] : objects_) {
                if (i < ob.index && ob.index <= top) {
                    --ob.index;
                }
            }
        }

        return to_rem;
    }
}
