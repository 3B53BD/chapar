#include "chapar/lua/state.hpp"

namespace chapar::lua {

    State::State()
        : L_{luaL_newstate()}
    { }

    State::~State()
    {
        lua_close(L_);
    }

    int State::dofile(gsl::czstring fname)
    {
        return luaL_dofile(L_, fname);
    }

    Ref State::operator[](gsl::czstring key)
    {
        lua_getglobal(L_, key);
        return {stack_.push(key), &stack_, L_};
    }
}
