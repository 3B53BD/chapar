#ifndef CHAPAR_YGDT9645zk9FKzrC_HEADER_GUARD
#define CHAPAR_YGDT9645zk9FKzrC_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <map>
#include <string>
#include <vector>

namespace chapar::lua {

    class Stack {
    public:
        using Id = int;
        struct Object {
            int index;
            std::string key;
            Id parent;
            int use_count;
        };

        Object const& operator[](Id id) const;
        bool contains(Id id) const noexcept;

        Id push(gsl::czstring key = "", Id parent = 0);
        std::vector<int> erase(Id id);

    private:
        std::map<Id, Object> objects_;
        Id next_{1};
    };
}

#endif
