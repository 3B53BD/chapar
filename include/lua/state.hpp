#ifndef CHAPAR_UB6GbVzc8m36DE6Z_HEADER_GUARD
#define CHAPAR_UB6GbVzc8m36DE6Z_HEADER_GUARD

#include <chapar/lua/ref.hpp>
#include <chapar/lua/stack.hpp>
#include <gsl-lite/gsl-lite.hpp>
#include <lua.hpp>

namespace chapar::lua {

    class State {
    public:
        State();
        ~State();

        int dofile(gsl::czstring fname);

        Ref operator[](gsl::czstring key);

        operator lua_State*() const noexcept { return L_; }

    private:
        lua_State* L_;
        Stack stack_;
    };
}

#endif
