ch = {
    --[[
    Type: Camera.
    Binds 1 VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER to VK_SHADER_STAGE_VERTEX_BIT.
    ]]
    camera = 0,
    --[[
    Type: Model.
    Binds 1 VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER of each node that has mesh to VK_SHADER_STAGE_VERTEX_BIT.
    ]]
    model = 1
}
