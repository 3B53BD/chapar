#ifndef CHAPAR_2W3Zy8fjiCTHdWam_HEADER_GUARD
#define CHAPAR_2W3Zy8fjiCTHdWam_HEADER_GUARD

#include <chapar/lua/stack.hpp>
#include <lua.hpp>

namespace chapar::lua {

    class RefIt;
    class RefSentinel {};

    class Ref {
    public:
        Ref() = default;
        Ref(Stack::Id id, Stack* stack, lua_State* L) noexcept;
        Ref(Ref&) = delete;
        Ref& operator=(Ref&) = delete;
        Ref(Ref&&) noexcept;
        Ref& operator=(Ref&&) noexcept;
        ~Ref() noexcept;

        Ref& operator=(lua_Integer n) noexcept;
        Ref& operator=(gsl::czstring s) noexcept;
        Ref operator[](gsl::czstring key) noexcept;
        Ref operator[](lua_Integer i) noexcept;

        bool to_boolean() const noexcept;
        lua_Number to_number() const noexcept;
        lua_Integer to_integer() const noexcept;
        std::string to_string() const noexcept;

        bool is_nil() const noexcept;
        bool is_none() const noexcept;
        bool is_noneornil() const noexcept;
        bool is_boolean() const noexcept;
        bool is_number() const noexcept;
        bool is_integer() const noexcept;
        bool is_string() const noexcept;
        bool is_function() const noexcept;
        bool is_userdata() const noexcept;
        bool is_lightuserdata() const noexcept;
        bool is_thread() const noexcept;
        bool is_table() const noexcept;

        lua_Integer len() const noexcept;

        RefIt begin() noexcept;
        RefSentinel end() noexcept;

        std::string type() const noexcept;

        bool valid() const noexcept;

        friend bool operator==(Ref const& lhs, Ref const& rhs) noexcept;
        friend bool operator!=(Ref const& lhs, Ref const& rhs) noexcept;

    private:
        void update() noexcept;
        Ref getglobal(gsl::czstring key) noexcept;
        Ref getfield(gsl::czstring key) noexcept;

        Stack::Id id_;
        Stack* stack_;
        lua_State* L_;
    };

    class RefIt {
    public:
        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = Ref;
        using pointer = Ref*;
        using reference = Ref&;

        RefIt() = default;
        RefIt(Ref* arr) noexcept;
        RefIt(RefIt&) = delete;
        RefIt& operator=(RefIt&) = delete;
        RefIt(RefIt&&) = default;
        RefIt& operator=(RefIt&&) = default;
        ~RefIt() = default;

        reference operator*() noexcept { return elm_; }
        pointer operator->() noexcept { return &elm_; }

        RefIt& operator++() { ++idx_; return *this; }
        // RefIt operator++(int) { auto tmp = *this; ++(*this); return tmp; }

        friend bool operator==(RefIt const& lhs, RefIt const& rhs) noexcept;
        friend bool operator!=(RefIt const& lhs, RefIt const& rhs) noexcept;
        friend bool operator==(RefIt const& lhs, RefSentinel) noexcept;
        friend bool operator==(RefSentinel, RefIt const& rhs) noexcept;
        friend bool operator!=(RefIt const& lhs, RefSentinel) noexcept;
        friend bool operator!=(RefSentinel, RefIt const& rhs) noexcept;

    private:
        Ref* arr_ = nullptr;
        Ref elm_;
        lua_Integer idx_ = 0;
    };
}

#endif
