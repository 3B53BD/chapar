#ifndef CHAPAR_XFznxoY8NC45fxuo_HEADER_GUARD
#define CHAPAR_XFznxoY8NC45fxuo_HEADER_GUARD

#include <chapar/core/window.hpp>
#include <chapar/vulkan/window.hpp>
#include <bitset>
#include <memory>
#include <string>
#include <utility>
#include <cstdint>

class SDL_Window;

namespace chapar::sdl {

    class Window : public WindowImpl, public vulkan::Window {
    public:
        static std::unique_ptr<Window> vulkan();

        Window() = default;
        Window(uint32_t flags);
        Window(Window const&) = delete;
        Window& operator=(Window const&) = delete;
        Window(Window&&) = delete;
        Window& operator=(Window&&) = delete;
        ~Window() noexcept;

        std::string title() const noexcept override;

        std::pair<int, int> origin() const noexcept override;
        std::pair<int, int> extent() const noexcept override;

        bool is_fullscreen() const noexcept override;
        bool is_focused() const noexcept override;
        bool is_resizable() const noexcept override;

        void set_origin(int x, int y) const noexcept override;
        void set_extent(int width, int height) const noexcept override;

        void set_resizable(bool resizable) noexcept override;
        void set_fullscreen(WindowFullscreanMode mode) noexcept override;
        void set_maximize() const noexcept override;
        void set_minimize() const noexcept override;

        void set_focus() noexcept override;

        void set_title(std::string_view title) const noexcept override;

        std::vector<gsl::czstring> extensions() const noexcept override;
        std::pair<VkSurfaceKHR, bool> surface(VkInstance instance) const noexcept override;

        SDL_Window* handle() const noexcept { return window_; }

    protected:
        SDL_Window* window_ = nullptr;

    private:
        static constexpr std::uint8_t resizable_  = 0;
        static constexpr std::uint8_t fullscreen_ = 1;
        static constexpr std::uint8_t focused_    = 2;
        std::bitset<3> status_;
    };
}

#endif
