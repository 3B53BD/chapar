#ifndef CHAPAR_xyQQHtXGL6auEvsr_HEADER_GUARD
#define CHAPAR_xyQQHtXGL6auEvsr_HEADER_GUARD

#include <memory>
#include <string>
#include <string_view>
#include <utility>
#include <cstdint>

namespace chapar {

    enum class WindowFullscreanMode : uint8_t {
        real = 0,
        fake = 1,
        off  = 2
    };

    class WindowImpl;

    class Window {
    public:
        virtual void set_title(std::string_view title) const noexcept;

        virtual void set_origin(int x, int y) const noexcept;
        virtual void set_extent(int width, int height) const noexcept;

        virtual void set_resizable(bool resizable) noexcept;
        virtual void set_fullscreen(WindowFullscreanMode fullscreen) noexcept;
        virtual void set_maximize() const noexcept;
        virtual void set_minimize() const noexcept;

        virtual void set_focus() noexcept;

        virtual std::string title() const noexcept;

        virtual std::pair<int, int> origin() const noexcept;
        virtual std::pair<int, int> extent() const noexcept;

        virtual bool is_fullscreen() const noexcept;
        virtual bool is_focused() const noexcept;
        virtual bool is_resizable() const noexcept;

    protected:
        std::unique_ptr<WindowImpl> impl_;
    };

    class WindowImpl {
    public:
        virtual void set_title(std::string_view title) const noexcept = 0;

        virtual void set_origin(int x, int y) const noexcept = 0;
        virtual void set_extent(int width, int height) const noexcept = 0;

        virtual void set_resizable(bool resizable) noexcept = 0;
        virtual void set_fullscreen(WindowFullscreanMode mode) noexcept = 0;
        virtual void set_maximize() const noexcept = 0;
        virtual void set_minimize() const noexcept = 0;

        virtual void set_focus() noexcept = 0;

        virtual std::string title() const noexcept = 0;

        virtual std::pair<int, int> origin() const noexcept = 0;
        virtual std::pair<int, int> extent() const noexcept = 0;

        virtual bool is_fullscreen() const noexcept = 0;
        virtual bool is_focused() const noexcept = 0;
        virtual bool is_resizable() const noexcept = 0;
    };
}

#endif
