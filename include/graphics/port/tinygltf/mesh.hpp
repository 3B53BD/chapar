#ifndef CHAPAR_cMhsxKbPjxLM8Jmq_HEADER_GUARD
#define CHAPAR_cMhsxKbPjxLM8Jmq_HEADER_GUARD

#include <chapar/graphics/raw_scene.hpp>
#include <vector>

namespace tinygltf {
    struct Mesh;
    class Model;
}

namespace chapar::graphics::tinygltf_port {
    namespace details {}
    namespace details {}

    RawMesh load_mesh(
        tinygltf::Model const& tiny_model,
        tinygltf::Mesh const& tiny_mesh,
        std::vector<VertexComponent> const& components
    );
}

#endif
