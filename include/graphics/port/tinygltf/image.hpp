#ifndef CHAPAR_TYsFmbaPThcuexk7_HEADER_GUARD
#define CHAPAR_TYsFmbaPThcuexk7_HEADER_GUARD

#include <chapar/graphics/raw_scene.hpp>
#include <memory>
#include <optional>
#include <cstddef>
#include <cstdint>

namespace chapar::graphics::tinygltf_port {
    namespace details {}
    namespace details {}

    class GltfImage : public RawImage {
    public:
        GltfImage() = default;
        GltfImage(uint32_t width, uint32_t height, std::byte const* data, std::size_t size);
        GltfImage(GltfImage const&) = delete;
        GltfImage& operator=(GltfImage const&) = delete;
        GltfImage(GltfImage&&) = delete;
        GltfImage& operator=(GltfImage&&) = delete;
        ~GltfImage() noexcept override = default;

        uint32_t width() const noexcept override;
        uint32_t height() const noexcept override;
        uint32_t mip_levels() const noexcept override;
        std::byte const* data() const noexcept override;
        std::size_t size() const noexcept override;
        std::optional<std::size_t> offset(uint32_t mip_level) const noexcept override;

    private:
        uint32_t width_;
        uint32_t height_;
        std::byte const* data_;
        std::size_t size_;
    };
}

#endif
