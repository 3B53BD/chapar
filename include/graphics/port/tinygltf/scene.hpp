#ifndef CHAPAR_i5PgNrZSB7ggcRsV_HEADER_GUARD
#define CHAPAR_i5PgNrZSB7ggcRsV_HEADER_GUARD

#include <chapar/graphics/raw_scene.hpp>
#include <gsl-lite/gsl-lite.hpp>
#include <vector>

namespace chapar::graphics {
    class RawScene;
}

namespace chapar::graphics::tinygltf_port {
    namespace details {}
    namespace details {}

    RawScene load_scene(
        gsl::czstring filename,
        std::vector<VertexComponent> const& vertex_components
    );
}

#endif
