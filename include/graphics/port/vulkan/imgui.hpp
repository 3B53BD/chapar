#ifndef CHAPAR_VvTLuPRuf3RZqvaM_HEADER_GUARD
#define CHAPAR_VvTLuPRuf3RZqvaM_HEADER_GUARD

#include <chapar/imgui/imgui.h>
#include <chapar/imgui/imgui_impl_sdl.h>
#include <chapar/imgui/imgui_impl_vulkan.h>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

union SDL_Event;
class SDL_Window;

namespace chapar::sdl {
    class Window;
}

namespace chapar::vulkan {
    class CommandBuffer;
    class CommandPool;
    class DescriptorPool;
    class Device;
    class Framebuffer;
    class Instance;
    class PhysicalDevice;
    class Queue;
    class RenderPass;
    class Swapchain;
}

namespace chapar::graphics::vulkan_port {
    namespace details {}
    namespace details {}

    class Imgui {
    public:
        Imgui() = default;
        Imgui(
            sdl::Window const& window,
            vulkan::Instance const& instance,
            vulkan::PhysicalDevice const& physical_device,
            vulkan::Device const& device,
            vulkan::Queue const& queue,
            vulkan::Swapchain const& swapchain,
            vulkan::RenderPass const& render_pass,
            vulkan::CommandPool const& command_pool
        );
        Imgui(Imgui const&) = delete;
        Imgui& operator=(Imgui const&) = delete;
        Imgui(Imgui&&) = delete;
        Imgui& operator=(Imgui&&) = delete;
        ~Imgui() noexcept;

        void process_event(SDL_Event* event);
        void new_frame(float* val);
        VkCommandBuffer record(VkCommandBufferInheritanceInfo const& info, std::size_t buffer_index);

    private:
        std::vector<std::unique_ptr<vulkan::CommandBuffer>> command_buffers_;
        std::unique_ptr<vulkan::DescriptorPool> descriptor_pool_;
        SDL_Window* window_ = nullptr;
    };
}

#endif
