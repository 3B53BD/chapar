#ifndef CHAPAR_yWqrSsVGWmtbV6Xi_HEADER_GUARD
#define CHAPAR_yWqrSsVGWmtbV6Xi_HEADER_GUARD

#include <chapar/vma/vma.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>
#include <cstddef>
#include <cstdint>

namespace chapar::graphics {
    class RawScene;
}

namespace chapar::vulkan {
    class CommandPool;
    class Queue;
    class Scene;
}

namespace chapar::graphics::vulkan_port {
    namespace details {}
    namespace details {}

    struct SceneCreateInfo {
        RawScene const* data;
        VkDevice device;
        VmaAllocator allocator;
        vulkan::Queue const* transfer_queue;
        uint32_t image_count;
        vulkan::CommandPool const* pool;
    };

    std::unique_ptr<vulkan::Scene> create_scene(SceneCreateInfo const& ci);
}

#endif
