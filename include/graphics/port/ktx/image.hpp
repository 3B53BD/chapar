#ifndef CHAPAR_7mQ6jVJtJatnMSfK_HEADER_GUARD
#define CHAPAR_7mQ6jVJtJatnMSfK_HEADER_GUARD

#include <chapar/graphics/texture.hpp>
#include <gsl-lite/gsl-lite.hpp>
#include <optional>
#include <cstdint>
#include <cstddef>

class ktxTexture;

namespace chapar::graphics {

    class KtxImage : public Image {
    public:
        KtxImage() = default;
        KtxImage(gsl::czstring filename);
        KtxImage(KtxImage const&) = delete;
        KtxImage& operator=(KtxImage const&) = delete;
        KtxImage(KtxImage&&) = delete;
        KtxImage& operator=(KtxImage&&) = delete;
        ~KtxImage() noexcept override;

        uint32_t width() const noexcept override;
        uint32_t height() const noexcept override;
        uint32_t mip_levels() const noexcept override;
        std::byte const* data() const noexcept override;
        std::size_t size() const noexcept override;
        std::optional<std::size_t> offset(uint32_t mip_level) const noexcept override;

    private:
        ktxTexture* texture_;
    };
}

#endif
