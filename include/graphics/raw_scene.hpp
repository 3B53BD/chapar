#ifndef CHAPAR_VgnF2NrfhwipmDBY_HEADER_GUARD
#define CHAPAR_VgnF2NrfhwipmDBY_HEADER_GUARD

#include <glm/glm.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <optional>
#include <vector>
#include <cstddef>
#include <cstdint>

namespace chapar::graphics {

    enum class VertexComponent {
        position,
        normal,
        texcoord,
        color
    };

    struct RawMaterial {};

    struct RawPrimitive {
        std::vector<std::byte> vertices;
        std::vector<std::byte> indices;
        uint32_t vertex_count = 0;
        uint32_t index_count = 0;
        VkIndexType index_type = VK_INDEX_TYPE_UINT32;
        std::optional<RawMaterial> material;
    };

    struct RawMesh {
        std::vector<RawPrimitive> primitives;
    };

    struct RawNode {
        std::vector<RawNode> children;
        std::optional<std::size_t> mesh_index;
    };

    class RawImage {
    public:
        virtual ~RawImage() noexcept = default;
        virtual uint32_t width() const noexcept = 0;
        virtual uint32_t height() const noexcept = 0;
        virtual uint32_t mip_levels() const noexcept = 0;
        virtual std::byte const* data() const noexcept = 0;
        virtual std::size_t size() const noexcept = 0;
        virtual std::optional<std::size_t> offset(uint32_t mip_level) const noexcept = 0;
    };

    struct RawTexture {
        std::size_t image_index;
        std::optional<std::size_t> sampler_index;
    };

    struct RawScene {
        std::vector<RawMesh> meshes;
        std::vector<RawNode> nodes;
        std::vector<std::unique_ptr<RawImage>> images;
        std::vector<VkSamplerCreateInfo> sampler_create_infos;
        std::vector<RawTexture> textures;
    };
}

#endif
