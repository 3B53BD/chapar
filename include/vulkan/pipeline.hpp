#ifndef CHAPAR_DSdJcfLXhr9FGs7Q_HEADER_GUARD
#define CHAPAR_DSdJcfLXhr9FGs7Q_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <memory>
#include <span>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    VkPipelineLayoutCreateInfo create_pipeline_layout_create_info(std::vector<VkDescriptorSetLayout> const& layouts) noexcept;





    class PipelineLayout {
    public:
        static VkPipelineLayoutCreateInfo create_info(std::vector<VkDescriptorSetLayout> const& layouts) noexcept;

        PipelineLayout() = default;
        PipelineLayout(VkDevice device, VkPipelineLayoutCreateInfo const& create_info);
        PipelineLayout(PipelineLayout const&) = delete;
        PipelineLayout& operator=(PipelineLayout const&) = delete;
        PipelineLayout(PipelineLayout&&) = delete;
        PipelineLayout& operator=(PipelineLayout&&) = delete;
        ~PipelineLayout() noexcept;

        VkPipelineLayout handle() const noexcept { return pipeline_layout_; }

    private:
        VkPipelineLayout pipeline_layout_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class PipelineCreateInfoBuilder {
    public:
        PipelineCreateInfoBuilder() noexcept;
        void reset() noexcept;

        void set_vertex_input(
            std::vector<VkVertexInputBindingDescription> const& bindings,
            std::vector<VkVertexInputAttributeDescription> const& attributes
        ) noexcept;

        void add_viewporct(VkViewport const& viewport) noexcept;
        void add_viewport(float x, float y, float width, float height, float min_depth, float max_depth) noexcept;

        void add_scissor(VkRect2D const& scissor) noexcept;
        void add_scissor(VkOffset2D const& offset, VkExtent2D const& extent) noexcept;
        void add_scissor(int32_t x, int32_t y, uint32_t width, uint32_t height) noexcept;

        void set_samples(VkSampleCountFlagBits samples) noexcept;
        void add_color_blend_attachment() noexcept;
        void add_shader_stage(VkPipelineShaderStageCreateInfo info) noexcept;
        void set_render_pass(VkRenderPass render_pass) noexcept;
        void set_pipeline_layout(VkPipelineLayout pipeline_layout) noexcept;

        VkGraphicsPipelineCreateInfo pipeline_create_info() noexcept;

    private:
        VkPipelineVertexInputStateCreateInfo vertex_input_sci_;
        VkPipelineInputAssemblyStateCreateInfo input_assembly_sci_;
        std::vector<VkViewport> viewports_;
        std::vector<VkRect2D> scissors_;
        VkPipelineMultisampleStateCreateInfo multisample_sci_;
        std::vector<VkPipelineColorBlendAttachmentState> color_blend_attachment_ss_;
        std::vector<VkPipelineShaderStageCreateInfo> shader_scis_;
        VkPipelineLayout pipeline_layout_;
        VkRenderPass render_pass_;

        VkPipelineViewportStateCreateInfo viewport_sci_ = {};
        VkPipelineRasterizationStateCreateInfo rasterization_sci_ = {};
        VkPipelineDepthStencilStateCreateInfo depth_stencil_sci_ = {};
        VkPipelineColorBlendStateCreateInfo color_blend_sci_ = {};
    };





    class Pipeline {
    public:
        Pipeline() = default;
        Pipeline(
            VkDevice device,
            VkGraphicsPipelineCreateInfo const& create_info
        );
        Pipeline(Pipeline const&) = delete;
        Pipeline& operator=(Pipeline const&) = delete;
        Pipeline(Pipeline&&) = delete;
        Pipeline& operator=(Pipeline&&) = delete;
        ~Pipeline() noexcept;

        VkPipeline handle() const noexcept { return pipeline_; }

    private:
        VkPipeline pipeline_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
