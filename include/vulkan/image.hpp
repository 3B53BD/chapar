#ifndef CHAPAR_W93NvmDyZqjqp8mZ_HEADER_GUARD
#define CHAPAR_W93NvmDyZqjqp8mZ_HEADER_GUARD

#include <chapar/vma/vma.hpp>
#include <vulkan/vulkan.h>
#include <cstdint>

namespace chapar::vulkan {
    class CommandBuffer;





    class Image {
    public:
        static VkImageCreateInfo create_info() noexcept;

        Image() = default;
        Image(
            VmaAllocator allocator,
            VkImageCreateInfo const& image_create_info,
            VmaAllocationCreateInfo const& allocation_create_info
        );
        Image(Image const&) = delete;
        Image& operator=(Image const&) = delete;
        Image(Image&&) = delete;
        Image& operator=(Image&&) = delete;
        ~Image() noexcept;

        void transition_layout(
            CommandBuffer const& cmd_buffer,
            VkImageLayout to_layout
        ) noexcept;

        VkImageLayout layout() const noexcept;
        VkImageUsageFlags usage() const noexcept;
        VkFormat format() const noexcept;
        VkExtent3D const& extent() const noexcept;
        uint32_t mip_levels() const noexcept;
        VkImage handle() const noexcept { return image_; }

    private:
        VmaAllocator allocator_ = VK_NULL_HANDLE;
        VmaAllocation allocation_ = VK_NULL_HANDLE;
        VmaAllocationInfo allocation_info_ = {};
        VkImage image_ = VK_NULL_HANDLE;
        VkImageLayout layout_ = VK_IMAGE_LAYOUT_UNDEFINED;
        VkImageUsageFlags usage_ = {};
        VkFormat format_ = VK_FORMAT_UNDEFINED;
        VkExtent3D extent_ = {};
        uint32_t mip_levels_ = 1;
    };





    class ImageView {
    public:
        ImageView() = default;
        ImageView(
            VkDevice device,
            VkImage image,
            VkFormat format,
            uint32_t mip_levels,
            VkImageAspectFlags aspect_mask
        );
        ImageView(
            VkDevice device,
            Image const& image,
            VkImageAspectFlags aspect_mask
        );
        ImageView(ImageView const&) = delete;
        ImageView& operator=(ImageView const&) = delete;
        ImageView(ImageView&&) = delete;
        ImageView& operator=(ImageView&&) = delete;
        ~ImageView() noexcept;

        VkImageView handle() const noexcept { return view_; }

    private:
        void create(
            VkImage image,
            VkFormat format,
            VkImageAspectFlags aspect_mask,
            uint32_t mip_levels
        );

        VkImageView view_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class Sampler {
    public:
        static VkSamplerCreateInfo create_info() noexcept;

        Sampler() = default;
        Sampler(VkDevice device, VkSamplerCreateInfo const& info);
        Sampler(Sampler const&) = delete;
        Sampler& operator=(Sampler const&) = delete;
        Sampler(Sampler&&) = delete;
        Sampler& operator=(Sampler&&) = delete;
        ~Sampler() noexcept;

        VkSampler handle() const noexcept { return sampler_; }

    private:
        VkSampler sampler_;
        VkDevice device_;
    };
}

#endif
