#ifndef CHAPAR_kbL8CtaygZsYC6Ca_HEADER_GUARD
#define CHAPAR_kbL8CtaygZsYC6Ca_HEADER_GUARD

#include <vulkan/vulkan.h>

namespace chapar::vulkan {

    class Surface {
    public:
        Surface() = default;
        Surface(VkInstance instace, VkSurfaceKHR surface);
        Surface(Surface const&) = delete;
        Surface& operator=(Surface const&) = delete;
        Surface(Surface&&) = delete;
        Surface& operator=(Surface&&) = delete;
        ~Surface() noexcept;

        VkSurfaceKHR handle() const noexcept { return surface_; }

        void set_format(VkSurfaceFormatKHR format) noexcept;
        VkSurfaceFormatKHR const& format() const noexcept;

    private:
        VkSurfaceFormatKHR format_ = {};
        VkSurfaceKHR surface_ = VK_NULL_HANDLE;
        VkInstance instance_ = VK_NULL_HANDLE;
    };
}

#endif
