#ifndef CHAPAR_3weRALPkVwT7wD75_HEADER_GUARD
#define CHAPAR_3weRALPkVwT7wD75_HEADER_GUARD

#include <chapar/vma/vma.hpp>
#include <vulkan/vulkan.h>

namespace chapar::vulkan {

    class Allocator {
    public:
        static VmaAllocationCreateInfo allocation_create_info() noexcept;

        Allocator() = default;
        Allocator(VkInstance instance, VkPhysicalDevice physical_device, VkDevice device);
        Allocator(Allocator const&) = delete;
        Allocator& operator=(Allocator const&) = delete;
        Allocator(Allocator&&) = delete;
        Allocator& operator=(Allocator&&) = delete;
        ~Allocator() noexcept;

        VmaAllocator handle() const noexcept { return allocator_; }

    private:
        VmaAllocator allocator_ = VK_NULL_HANDLE;
    };
}

#endif
