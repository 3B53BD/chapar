#ifndef CHAPAR_TkPWvih36oVqDTiq_HEADER_GUARD
#define CHAPAR_TkPWvih36oVqDTiq_HEADER_GUARD

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class Allocator;
    class DescriptorPool;
    class DescriptorSetLayout;
    class Device;
    class Swapchain;
    class UniformBuffer;





    class Camera {
    public:
        Camera() = default;
        Camera(
            Device const& device,
            Allocator const& allocator,
            uint32_t image_count,
            VkDescriptorSetLayout set_layout
        );
        Camera(Camera const&) = delete;
        Camera& operator=(Camera const&) = delete;
        Camera(Camera&&) = delete;
        Camera& operator=(Camera&&) = delete;
        ~Camera() = default;

        void set_aspect(VkExtent2D const& extent) noexcept;
        void rotate(glm::vec3 const& delta) noexcept;
        void translate(glm::vec3 const& delta) noexcept;

        void update(std::size_t buffer_index) noexcept;

        void write(uint32_t binding) const noexcept;
        VkDescriptorSet descriptor_set(std::size_t index) const noexcept;

    private:
        struct UBO {
            alignas(16) glm::mat4 view;
            alignas(16) glm::mat4 proj;
        };

        glm::vec3 rotation_ = {};
        glm::vec3 translation_ = {};
        float aspect_ = {};

        struct Uniform {
            std::unique_ptr<UniformBuffer> buffer;
            VkDescriptorSet descriptor_set = VK_NULL_HANDLE;
        };

        std::vector<Uniform> uniforms_;
        std::unique_ptr<DescriptorPool> descriptor_pool_;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
