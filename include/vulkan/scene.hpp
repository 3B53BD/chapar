#ifndef CHAPAR_Y2VyJetXJUbM7bpC_HEADER_GUARD
#define CHAPAR_Y2VyJetXJUbM7bpC_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>
#include <glm/ext/quaternion_float.hpp>
#include <memory>
#include <vector>

namespace chapar::vulkan {
    class CommandBuffer;
    class DescriptorPool;
    class Image;
    class ImageView;
    class InputBuffer;
    class Sampler;
    class UniformBuffer;
}

namespace chapar::vulkan {

    struct Material {};

    struct Primitive {
        std::unique_ptr<InputBuffer> buffer;
        Material const* material = nullptr;
    };

    struct Mesh {
        std::vector<Primitive> primitives;
    };

    struct Model {
        glm::vec3 trl = {}; // translation
        glm::vec3 scl = glm::vec3{1.0f}; // scale
        glm::vec3 rot = {}; // rotation
        glm::mat4 mtx = {}; // matrix

        glm::mat4 matrix() const noexcept;
    };

    struct Uniform {
        std::unique_ptr<UniformBuffer> buffer;
        VkDescriptorSet descriptor_set = VK_NULL_HANDLE;

        void write(VkDevice device, uint32_t binding) const noexcept;
        void update(glm::mat4 const& m) const noexcept;
    };

    struct Node {
        Node* parent = nullptr;
        std::vector<Node> children;
        Model model;
        Mesh const* mesh = nullptr;
        std::vector<Uniform> uniforms;

        struct UBO {
            glm::mat4 model;
        };
    };

    struct Texture {
        std::shared_ptr<Image> image;
        std::shared_ptr<ImageView> image_view;
        std::shared_ptr<Sampler> sampler;
        VkDescriptorSet descriptor_set = VK_NULL_HANDLE;

        void write(VkDevice device, uint32_t binding) const noexcept;
    };

    struct Scene {
        std::vector<Node> nodes;
        std::vector<Mesh> meshes;
        std::vector<Texture> textures;
        std::unique_ptr<DescriptorPool> descriptor_pool;
        std::vector<std::unique_ptr<CommandBuffer>> command_buffers;
        std::size_t image_index = 0;
        uint32_t binding = 0;
        VkDevice device = VK_NULL_HANDLE;

        void describe(VkDescriptorSetLayout descriptor_set_layout) noexcept;
        void write() const noexcept;
        void update() const noexcept;
        void record(VkPipelineLayout pipeline_layout) const noexcept;

    private:
        void describe(Node& node, VkDescriptorSetLayout descriptor_set_layout) noexcept;
        void write(Node const& node) const noexcept;
        void update(Node const& node) const noexcept;
        void record(Node const& node, VkPipelineLayout pipeline_layout) const noexcept;
    };
}

#endif
