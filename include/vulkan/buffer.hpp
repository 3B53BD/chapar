#ifndef CHAPAR_EmqDiihsiaiT9DYF_HEADER_GUARD
#define CHAPAR_EmqDiihsiaiT9DYF_HEADER_GUARD

#include <chapar/vma/vma.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <span>
#include <vector>
#include <cstddef>

namespace chapar::vulkan {

    class Allocator;
    class Device;
    class Queue;





    class Buffer
    {
    public:
        static VkBufferCreateInfo create_info() noexcept;

        Buffer() = default;
        Buffer(
            VmaAllocator allocator,
            VkBufferCreateInfo const& buffer_info,
            VmaAllocationCreateInfo const& allocator_info
        );
        Buffer(Buffer const&) = delete;
        Buffer& operator=(Buffer const&) = delete;
        Buffer(Buffer&&) = delete;
        Buffer& operator=(Buffer&&) = delete;
        virtual ~Buffer() noexcept;

        VkResult map() noexcept;
        void unmap() noexcept;

        void* memcpy(std::size_t offset, std::span<std::byte const> source) noexcept;

        VkDeviceSize size() const noexcept;
        VkDescriptorBufferInfo descriptor() const noexcept;

        VkBuffer handle() const noexcept { return buffer_; }

    private:
        VmaAllocator allocator_ = VK_NULL_HANDLE;
        VmaAllocation allocation_ = VK_NULL_HANDLE;
        VmaAllocationInfo allocation_info_ = {};
        VkBuffer buffer_ = VK_NULL_HANDLE;
        void* mapped_data_ = nullptr;
    };





    class InputBuffer {
    public:
        virtual ~InputBuffer() noexcept = default;
        virtual void bind(VkCommandBuffer command_buffer) const noexcept = 0;
        virtual void draw(VkCommandBuffer command_buffer) const noexcept = 0;
        virtual VkDeviceSize size() const noexcept = 0;
        virtual VkDescriptorBufferInfo descriptor() const noexcept = 0;
        virtual VkBuffer handle() const noexcept = 0;
    };





    class VertexBuffer : public InputBuffer {
    public:
        VertexBuffer() = default;
        VertexBuffer(
            VkDevice device,
            VmaAllocator allocator,
            Queue const& transfer_queue,
            std::vector<std::byte> const& vertices,
            uint32_t vertex_count
        );
        VertexBuffer(VertexBuffer const&) = delete;
        VertexBuffer& operator=(VertexBuffer const&) = delete;
        VertexBuffer(VertexBuffer&&) = delete;
        VertexBuffer& operator=(VertexBuffer&&) = delete;
        ~VertexBuffer() noexcept override = default;

        void bind(VkCommandBuffer command_buffer) const noexcept override;
        void draw(VkCommandBuffer command_buffer) const noexcept override;

        VkDeviceSize size() const noexcept override;
        VkDescriptorBufferInfo descriptor() const noexcept override;
        VkBuffer handle() const noexcept override;

    private:
        uint32_t vertex_count_ = 0;
        std::unique_ptr<Buffer> buffer_;
    };





    class IndexVertexBuffer : public InputBuffer {
    public:
        IndexVertexBuffer() = default;
        IndexVertexBuffer(
            VkDevice device,
            VmaAllocator allocator,
            Queue const& transfer_queue,
            std::vector<std::byte> const& vertices,
            std::vector<std::byte> const& indices,
            uint32_t index_count,
            VkIndexType index_type
        );
        IndexVertexBuffer(IndexVertexBuffer const&) = delete;
        IndexVertexBuffer& operator=(IndexVertexBuffer const&) = delete;
        IndexVertexBuffer(IndexVertexBuffer&&) = delete;
        IndexVertexBuffer& operator=(IndexVertexBuffer&&) = delete;
        ~IndexVertexBuffer() noexcept override = default;

        void bind(VkCommandBuffer command_buffer) const noexcept override;
        void draw(VkCommandBuffer command_buffer) const noexcept override;

        VkDeviceSize size() const noexcept override;
        VkDescriptorBufferInfo descriptor() const noexcept override;
        VkBuffer handle() const noexcept override;

    private:
        uint32_t index_count_ = 0;
        std::unique_ptr<InputBuffer> input_buffer_;
        VkIndexType index_type_ = VK_INDEX_TYPE_UINT32;
        std::unique_ptr<Buffer> buffer_;
        VkDeviceSize vertex_offset_ = 0;
    };





    class UniformBuffer {
    public:
        UniformBuffer() = default;
        UniformBuffer(VmaAllocator allocator, std::size_t size);
        UniformBuffer(UniformBuffer const&) = delete;
        UniformBuffer& operator=(UniformBuffer const&) = delete;
        UniformBuffer(UniformBuffer&&) = delete;
        UniformBuffer& operator=(UniformBuffer&&) = delete;
        ~UniformBuffer() = default;

        VkResult map() noexcept;
        void unmap() noexcept;

        void* memcpy(std::size_t offset, std::span<std::byte const> source) noexcept;

        VkDeviceSize size() const noexcept;
        VkDescriptorBufferInfo descriptor() const noexcept;

        VkBuffer handle() const noexcept;

    private:
        std::unique_ptr<Buffer> buffer_;
    };
}

#endif
