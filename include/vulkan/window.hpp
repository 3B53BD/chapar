#ifndef CHAPAR_dJV96PN3aXWitvto_HEADER_GUARD
#define CHAPAR_dJV96PN3aXWitvto_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <utility>
#include <vector>

namespace chapar::vulkan {

    class Window {
    public:
        virtual std::vector<gsl::czstring> extensions() const noexcept = 0;
        virtual std::pair<VkSurfaceKHR, bool> surface(VkInstance instance) const noexcept = 0;
    };
}

#endif
