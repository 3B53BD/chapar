#ifndef CHAPAR_KUoWebNExzuc55LM_HEADER_GUARD
#define CHAPAR_KUoWebNExzuc55LM_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class QueueFamily {
    public:
        QueueFamily() = default;
        QueueFamily(
            uint32_t index,
            VkQueueFamilyProperties const& properties,
            bool surface_support
        );
        QueueFamily(QueueFamily const&) = default;
        QueueFamily& operator=(QueueFamily const&) = default;
        QueueFamily(QueueFamily&&) = default;
        QueueFamily& operator=(QueueFamily&&) = default;
        ~QueueFamily() = default;

        uint32_t index() const noexcept;
        bool graphics_bit() const noexcept;
        bool compute_bit() const noexcept;
        bool transfer_bit() const noexcept;
        bool sparse_binding_bit() const noexcept;
        bool protected_bit() const noexcept;
        bool surface_bit() const noexcept;

    private:
        uint32_t index_ = UINT32_MAX;
        VkQueueFlags capabilities_ = {};
        bool surface_support_ = false;
    };





    class QueueSumbitInfo {
    public:
        QueueSumbitInfo() noexcept;
        QueueSumbitInfo(QueueSumbitInfo const&) = default;
        QueueSumbitInfo& operator=(QueueSumbitInfo const&) = default;
        QueueSumbitInfo(QueueSumbitInfo&&) = default;
        QueueSumbitInfo& operator=(QueueSumbitInfo&&) = default;
        ~QueueSumbitInfo() = default;

        void wait(VkSemaphore semaphore, VkPipelineStageFlags stage) noexcept;
        void command(VkCommandBuffer buffer) noexcept;
        void signal(VkSemaphore signal) noexcept;

        VkSubmitInfo const& info() const noexcept;

    private:
        std::vector<VkSemaphore> wait_semaphores_;
        std::vector<VkPipelineStageFlags> wait_stages_;
        std::vector<VkCommandBuffer> command_buffers_;
        std::vector<VkSemaphore> signal_semaphores_;
        VkSubmitInfo info_ = {};
    };





    class Queue {
    public:
        Queue() = default;
        Queue(VkQueue queue, uint32_t family) noexcept;
        Queue(Queue const&) = default;
        Queue& operator=(Queue const&) = default;
        Queue(Queue&&) = default;
        Queue& operator=(Queue&&) = default;
        ~Queue() = default;

        void wait_idle() const noexcept;

        void submit(VkSubmitInfo const& info, VkFence fence) const noexcept;

        uint32_t family() const noexcept;
        VkQueue handle() const noexcept { return queue_; }

    private:
        VkQueue queue_ = VK_NULL_HANDLE;
        uint32_t family_ = UINT32_MAX;
    };
}

#endif
