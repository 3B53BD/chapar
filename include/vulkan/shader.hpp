#ifndef CHAPAR_DaUVomPyhG7VLYxR_HEADER_GUARD
#define CHAPAR_DaUVomPyhG7VLYxR_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <utility>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class Shader {
    public:
        virtual VkPipelineShaderStageCreateInfo pipeline_stage() const noexcept = 0;

        static std::vector<uint32_t> read_spirv(gsl::czstring filename);
        std::pair<VkShaderModule, VkResult> create_module(VkDevice device, std::vector<uint32_t> const& code) noexcept;
    };





    class VertexShader : public Shader {
    public:
        VertexShader() = default;
        VertexShader(VkDevice device, std::vector<uint32_t> const& code);
        VertexShader(VertexShader const&) = delete;
        VertexShader& operator=(VertexShader const&) = delete;
        VertexShader(VertexShader&&) = delete;
        VertexShader& operator=(VertexShader&&) = delete;
        ~VertexShader() noexcept;

        VkPipelineShaderStageCreateInfo pipeline_stage() const noexcept override;

    private:
        VkShaderModule module_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class FragmentShader : public Shader {
    public:
        FragmentShader() = default;
        FragmentShader(VkDevice device, std::vector<uint32_t> const& code);
        FragmentShader(FragmentShader const&) = delete;
        FragmentShader& operator=(FragmentShader const&) = delete;
        FragmentShader(FragmentShader&&) = delete;
        FragmentShader& operator=(FragmentShader&&) = delete;
        ~FragmentShader() noexcept;

        VkPipelineShaderStageCreateInfo pipeline_stage() const noexcept override;

    private:
        VkShaderModule module_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
