#ifndef CHAPAR_sZD5RRx5SvpYAmkS_HEADER_GUARD
#define CHAPAR_sZD5RRx5SvpYAmkS_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <vector>

namespace chapar::vulkan {

    class Instance {
    public:
        static std::vector<VkLayerProperties> layer_properties();

        Instance() = default;
        Instance(
            std::vector<gsl::czstring> const& validation_layers,
            std::vector<gsl::czstring> const& enabled_extensions,
            bool debug
        );
        Instance(Instance const&) = delete;
        Instance& operator=(Instance const&) = delete;
        Instance(Instance&&) = delete;
        Instance& operator=(Instance&&) = delete;
        ~Instance() noexcept;

        std::vector<gsl::czstring> const& validation_layers() const noexcept;
        std::vector<gsl::czstring> const& enabled_extensions() const noexcept;
        std::vector<VkPhysicalDevice> physical_devices() const noexcept;
        VkInstance handle() const noexcept { return instance_; }

    private:
        VkResult create_debug_utils_messenger_ext(
            VkDebugUtilsMessengerCreateInfoEXT const* create_info
        ) noexcept;
        void destroy_debug_utils_messenger_ext() noexcept;

        VkInstance instance_ = VK_NULL_HANDLE;
        std::vector<gsl::czstring> validation_layers_;
        std::vector<gsl::czstring> extensions_;
        VkDebugUtilsMessengerEXT debug_messenger_ = VK_NULL_HANDLE;
    };

}

#endif
