#ifndef CHAPAR_h4bM5WD58P23zcz3_HEADER_GUARD
#define CHAPAR_h4bM5WD58P23zcz3_HEADER_GUARD

#include <chapar/vma/vma.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class Attachment {
    public:
        virtual ~Attachment() noexcept = default;
        virtual VkImageView image_view() const noexcept = 0;
        virtual VkDescriptorImageInfo descriptor() const noexcept = 0;
    };





    class SwapchainAttachment : public Attachment {
    public:
        SwapchainAttachment() = default;
        SwapchainAttachment(
            VkDevice device,
            VkImage image,
            VkFormat format
        );
        SwapchainAttachment(SwapchainAttachment&) = delete;
        SwapchainAttachment& operator=(SwapchainAttachment&) = delete;
        SwapchainAttachment(SwapchainAttachment&&) = delete;
        SwapchainAttachment& operator=(SwapchainAttachment&&) = delete;
        ~SwapchainAttachment() noexcept override;

        VkImageView image_view() const noexcept override { return image_view_; }
        VkDescriptorImageInfo descriptor() const noexcept override;

    private:
        VkImageView image_view_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class ColorAttachment : public Attachment {
    public:
        ColorAttachment() = default;
        ColorAttachment(
            VkDevice device,
            VmaAllocator allocator,
            VkImageCreateInfo ici
        );
        ColorAttachment(ColorAttachment&) = delete;
        ColorAttachment& operator=(ColorAttachment&) = delete;
        ColorAttachment(ColorAttachment&&) = delete;
        ColorAttachment& operator=(ColorAttachment&&) = delete;
        ~ColorAttachment() noexcept override;

        VkImageView image_view() const noexcept override { return image_view_; }
        VkDescriptorImageInfo descriptor() const noexcept override;

    private:
        VkImage image_ = VK_NULL_HANDLE;
        VmaAllocation allocation_ = VK_NULL_HANDLE;
        VkImageView image_view_ = VK_NULL_HANDLE;
        VmaAllocator allocator_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class DepthAttachment : public Attachment {
    public:
        DepthAttachment() = default;
        DepthAttachment(
            VkDevice device,
            VmaAllocator allocator,
            VkImageCreateInfo ici
        );
        DepthAttachment(DepthAttachment&) = delete;
        DepthAttachment& operator=(DepthAttachment&) = delete;
        DepthAttachment(DepthAttachment&&) = delete;
        DepthAttachment& operator=(DepthAttachment&&) = delete;
        ~DepthAttachment() noexcept override;

        VkImageView image_view() const noexcept override { return image_view_; }
        VkDescriptorImageInfo descriptor() const noexcept override;

    private:
        VkImage image_ = VK_NULL_HANDLE;
        VmaAllocation allocation_ = VK_NULL_HANDLE;
        VkImageView image_view_ = VK_NULL_HANDLE;
        VmaAllocator allocator_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class Framebuffer {
    public:
        Framebuffer() = default;
        Framebuffer(
            VkDevice device,
            VkRenderPass render_pass,
            std::vector<VkImageView> image_views,
            uint32_t width,
            uint32_t height
        );
        Framebuffer(Framebuffer&) = delete;
        Framebuffer& operator=(Framebuffer&) = delete;
        Framebuffer(Framebuffer&&) = delete;
        Framebuffer& operator=(Framebuffer&&) = delete;
        ~Framebuffer() noexcept;

        VkFramebuffer handle() const noexcept { return framebuffer_; }

    private:
        std::vector<std::unique_ptr<Attachment>> attachments_;
        VkFramebuffer framebuffer_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
