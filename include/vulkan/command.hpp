#ifndef CHAPAR_jAs2zaDTsNGXRXok_HEADER_GUARD
#define CHAPAR_jAs2zaDTsNGXRXok_HEADER_GUARD

#include "chapar/vulkan/queue.hpp"
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {
    class Buffer;
    class Image;
}

namespace chapar::vulkan {

    class CommandPool {
    public:
        static VkCommandPoolCreateInfo create_info(uint32_t queue_family_index) noexcept;

        CommandPool() = default;
        CommandPool(
            VkDevice device,
            VkCommandPoolCreateInfo const& info
        );
        CommandPool(CommandPool const&) = delete;
        CommandPool& operator=(CommandPool const&) = delete;
        CommandPool(CommandPool&&) = delete;
        CommandPool& operator=(CommandPool&&) = delete;
        ~CommandPool() noexcept;

        VkCommandBufferAllocateInfo allocate_info(uint32_t count) const noexcept;
        std::vector<VkCommandBuffer> allocate_command_buffers(VkCommandBufferAllocateInfo const& info) const;
        void free_command_buffers(std::vector<VkCommandBuffer> const& command_buffers) const noexcept;

        Queue const& queue() const noexcept;
        VkCommandPool handle() const noexcept { return command_pool_; }

    private:
        VkCommandPool command_pool_ = VK_NULL_HANDLE;
        Queue queue_;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class CommandBuffer {
    public:
        static VkCommandBufferBeginInfo begin_info() noexcept;
        static VkCommandBufferInheritanceInfo inheritance_info() noexcept;

        CommandBuffer() = default;
        CommandBuffer(CommandPool const& pool, VkCommandBuffer buffer) noexcept;
        CommandBuffer(CommandPool const& pool, VkCommandBufferAllocateInfo const& info);
        CommandBuffer(CommandBuffer const&) = delete;
        CommandBuffer& operator=(CommandBuffer const&) = delete;
        CommandBuffer(CommandBuffer&&) = delete;
        CommandBuffer& operator=(CommandBuffer&&) = delete;
        ~CommandBuffer() noexcept;

        VkResult begin(VkCommandBufferBeginInfo const& info) const noexcept;
        VkResult end() const noexcept;

        void begin_render_pass(VkRenderPass render_pass, VkFramebuffer framebuffer, VkRect2D render_area, std::vector<VkClearValue> const& clear_values, VkSubpassContents contents) const noexcept;
        void end_render_pass() const noexcept;

        void bind(VkPipeline pipeline) const noexcept;
        void bind(VkPipelineLayout layout, uint32_t first_set, std::vector<VkDescriptorSet> const& sets) const noexcept;

        void pipeline_barrier(
            VkPipelineStageFlags src,
            VkPipelineStageFlags dst,
            std::vector<VkImageMemoryBarrier> const& barriers
        ) const noexcept;

        void copy_to(
            Buffer const& src,
            Buffer const& dst,
            std::vector<VkBufferCopy> const& regions
        ) const noexcept;

        void copy_to(
            Buffer const& src,
            Image const& dst,
            std::vector<VkBufferImageCopy> const& regions
        ) const noexcept;

        VkCommandBuffer handle() const noexcept { return command_buffer_; }

    private:
        VkCommandBuffer command_buffer_ = VK_NULL_HANDLE;
        CommandPool const* command_pool_ = nullptr;
    };





    class OneTimeSubmit {
    public:
        OneTimeSubmit() = default;
        OneTimeSubmit(VkDevice device, Queue const& queue);
        OneTimeSubmit(OneTimeSubmit const&) = delete;
        OneTimeSubmit& operator=(OneTimeSubmit const&) = delete;
        OneTimeSubmit(OneTimeSubmit&&) = delete;
        OneTimeSubmit& operator=(OneTimeSubmit&&) = delete;
        ~OneTimeSubmit() noexcept;

        VkCommandBuffer command_buffer_handle() const noexcept;
        CommandBuffer const* command_buffer() const noexcept;

    private:
        std::unique_ptr<CommandPool> pool_;
        std::unique_ptr<CommandBuffer> buffer_;
        Queue queue_;
    };
}

#endif
