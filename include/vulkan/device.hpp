#ifndef CHAPAR_3cnbNkiegn8D5J8M_HEADER_GUARD
#define CHAPAR_3cnbNkiegn8D5J8M_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <utility>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class CommandPool;
    class CommandBuffer;
    class Queue;





    class Device {
    public:
        Device() = default;
        Device(
            VkPhysicalDevice physical_device,
            std::vector<uint32_t> const& queue_family_indices,
            std::vector<gsl::czstring> const& validation_layers,
            std::vector<gsl::czstring> const& extensions,
            VkPhysicalDeviceFeatures const& features
        );
        Device(Device const&) = delete;
        Device& operator=(Device const&) = delete;
        Device(Device&&) = delete;
        Device& operator=(Device&&) = delete;
        ~Device() noexcept;

        void wait_idle() const noexcept;

        VkQueue queue(uint32_t family) const noexcept;
        VkDevice handle() const noexcept;

    private:
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
