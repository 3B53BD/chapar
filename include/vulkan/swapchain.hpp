#ifndef CHAPAR_YyQYZ2fB8kEbpFDj_HEADER_GUARD
#define CHAPAR_YyQYZ2fB8kEbpFDj_HEADER_GUARD

#include <chapar/vulkan/image.hpp>
#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class Queue;





    class Swapchain {
    public:
        Swapchain() = default;
        Swapchain(
            VkDevice device,
            VkSurfaceKHR surface,
            VkSurfaceTransformFlagBitsKHR pre_transform,
            uint32_t image_count,
            VkFormat image_format,
            VkColorSpaceKHR image_color_space,
            VkExtent2D image_extent,
            std::vector<uint32_t> const& queue_family_indices,
            VkPresentModeKHR present_mode
        );
        Swapchain(Swapchain const&) = delete;
        Swapchain& operator=(Swapchain const&) = delete;
        Swapchain(Swapchain&&) = delete;
        Swapchain& operator=(Swapchain&&) = delete;
        ~Swapchain() noexcept;

        void resize(VkExtent2D extent);

        VkResult aquire_next_image(VkSemaphore& semaphore);
        void wait_idle(VkFence fence) noexcept;
        VkResult present(Queue const& queue, std::vector<VkSemaphore> const& wait_semaphores) const;

        std::vector<VkImage> const& images() const noexcept;
        uint32_t image_count() const noexcept;
        uint32_t image_index() const noexcept;
        VkExtent2D const& image_extent() const noexcept;
        VkSurfaceFormatKHR const& image_format() const noexcept;
        VkSwapchainKHR handle() const noexcept;

    private:
        void create();

        uint32_t image_index_ = UINT32_MAX;
        std::vector<VkImage> images_;
        std::vector<VkFence> idle_images_;
        VkExtent2D image_extent_ = {};
        VkSwapchainKHR swapchain_ = VK_NULL_HANDLE;
        VkSurfaceFormatKHR image_format_ = {};
        VkPresentModeKHR present_mode_ = {};
        VkSurfaceKHR surface_ = VK_NULL_HANDLE;
        std::vector<uint32_t> queue_family_indices_;
        VkSurfaceTransformFlagBitsKHR pre_transform_ = {};
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
