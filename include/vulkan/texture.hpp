#ifndef CHAPAR_SfVrFrwaYF7MMVNs_HEADER_GUARD
#define CHAPAR_SfVrFrwaYF7MMVNs_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <memory>

namespace chapar::vulkan {

    class Image;
    class ImageView;
    class Sampler;





    class Texture {
    public:
        Texture() = default;
        Texture(
            std::shared_ptr<Image> image,
            std::shared_ptr<ImageView> image_view,
            std::shared_ptr<Sampler> sampler
        );
        Texture(Texture const&) = default;
        Texture& operator=(Texture const&) = default;
        Texture(Texture&&) = default;
        Texture& operator=(Texture&&) = default;
        ~Texture() = default;

        Image const& image() const noexcept;
        ImageView const& image_view() const noexcept;
        Sampler const& sampler() const noexcept;

        VkDescriptorImageInfo descriptor() const noexcept;

    private:
        std::shared_ptr<Image> image_;
        std::shared_ptr<ImageView> image_view_;
        std::shared_ptr<Sampler> sampler_;
    };
}

#endif
