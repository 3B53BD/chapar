#ifndef CHAPAR_ge73LWZe5AnS3Uhv_HEADER_GUARD
#define CHAPAR_ge73LWZe5AnS3Uhv_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <vector>
#include <optional>
#include <utility>

namespace chapar::vulkan {

    struct SubpassDescription {
        std::vector<VkAttachmentReference> input;
        std::vector<VkAttachmentReference> color;
        std::vector<VkAttachmentReference> resolve;
        std::optional<VkAttachmentReference> depth;
    };





    struct SubpassDependency {
        uint32_t subpass;
        VkPipelineStageFlags stage;
        VkAccessFlags access;
    };





    VkSubpassDescription subpass_description(SubpassDescription const& sd);

    SubpassDependency external_dependency();

    VkSubpassDependency dependency(
        SubpassDependency const& src,
        SubpassDependency const& dst
    );





    class RenderPass {
    public:
        RenderPass() = default;
        RenderPass(
            VkDevice device,
            std::vector<VkAttachmentDescription> const& attachments,
            std::vector<VkSubpassDescription> const& descriptions,
            std::vector<VkSubpassDependency> const& dependencies
        );
        RenderPass(RenderPass const&) = delete;
        RenderPass& operator=(RenderPass const&) = delete;
        RenderPass(RenderPass&&) = delete;
        RenderPass& operator=(RenderPass&&) = delete;
        ~RenderPass() noexcept;

        VkRenderPass handle() const noexcept { return render_pass_; }

    private:
        VkRenderPass render_pass_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
