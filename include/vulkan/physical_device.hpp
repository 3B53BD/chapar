#ifndef CHAPAR_AiEYLZp3kaz7qs9j_HEADER_GUARD
#define CHAPAR_AiEYLZp3kaz7qs9j_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <vector>

namespace chapar::vulkan {

    class PhysicalDevice {
    public:
        PhysicalDevice() = default;
        PhysicalDevice(VkPhysicalDevice physical_device) noexcept;
        PhysicalDevice(PhysicalDevice const&) = default;
        PhysicalDevice& operator=(PhysicalDevice const&) = default;
        PhysicalDevice(PhysicalDevice&&) = default;
        PhysicalDevice& operator=(PhysicalDevice&&) = default;
        ~PhysicalDevice() = default;

        VkSampleCountFlagBits max_samples() const noexcept;
        std::vector<VkQueueFamilyProperties> queue_family_properties() const noexcept;
        VkBool32 support_surface(uint32_t index, VkSurfaceKHR surface) const noexcept;
        std::vector<VkSurfaceFormatKHR> formats(VkSurfaceKHR surface) const noexcept;
        VkSurfaceCapabilitiesKHR capabilities(VkSurfaceKHR surface) const noexcept;
        std::vector<VkPresentModeKHR> present_modes(VkSurfaceKHR surface) const noexcept;
        VkFormatProperties format_properties(VkFormat format) const noexcept;
        VkPhysicalDeviceFeatures const& features() const noexcept;
        VkPhysicalDeviceProperties const& properties() const noexcept;

        VkPhysicalDevice handle() const noexcept { return physical_device_; }

    private:
        VkPhysicalDevice physical_device_ = VK_NULL_HANDLE;
        std::vector<VkExtensionProperties> extensions_;
        VkPhysicalDeviceFeatures features_ = {};
        VkPhysicalDeviceProperties properties_ = {};
        VkPhysicalDeviceMemoryProperties memory_properties_ = {};
    };
}

#endif
