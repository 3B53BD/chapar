#ifndef CHAPAR_UXBHt9BAKVfNYSLD_HEADER_GUARD
#define CHAPAR_UXBHt9BAKVfNYSLD_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class DescriptorPool {
    public:
        DescriptorPool() = default;
        DescriptorPool(
            VkDevice device,
            uint32_t max_sets,
            std::vector<VkDescriptorPoolSize> const& pool_sizes
        );
        DescriptorPool(DescriptorPool const&) = delete;
        DescriptorPool& operator=(DescriptorPool const&) = delete;
        DescriptorPool(DescriptorPool&&) = delete;
        DescriptorPool& operator=(DescriptorPool&&) = delete;
        ~DescriptorPool() noexcept;

        std::vector<VkDescriptorSet> allocate_sets(
            uint32_t count,
            VkDescriptorSetLayout layout
        ) const;

        VkDescriptorPool handle() const noexcept { return pool_; }

    private:
        VkDescriptorPool pool_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    class DescriptorSetLayout {
    public:
        DescriptorSetLayout() = default;
        DescriptorSetLayout(
            VkDevice device,
            std::vector<VkDescriptorSetLayoutBinding> const& bindings
        );
        DescriptorSetLayout(DescriptorSetLayout const&) = delete;
        DescriptorSetLayout& operator=(DescriptorSetLayout const&) = delete;
        DescriptorSetLayout(DescriptorSetLayout&&) = delete;
        DescriptorSetLayout& operator=(DescriptorSetLayout&&) = delete;
        ~DescriptorSetLayout() noexcept;

        VkDescriptorSetLayout handle() const noexcept { return layout_; }

    private:
        VkDescriptorSetLayout layout_;
        VkDevice device_;
    };





    VkWriteDescriptorSet write_descriptor_set(
        VkDescriptorSet descriptor_set,
        uint32_t binding,
        std::vector<VkDescriptorBufferInfo> const& infos
    );

    VkWriteDescriptorSet write_descriptor_set(
        VkDescriptorSet descriptor_set,
        uint32_t binding,
        std::vector<VkDescriptorImageInfo> const& infos
    );
}

#endif
