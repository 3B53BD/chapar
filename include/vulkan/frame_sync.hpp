#ifndef CHAPAR_vg7Pnkum2CJdkpWB_HEADER_GUARD
#define CHAPAR_vg7Pnkum2CJdkpWB_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <utility>
#include <vector>

namespace chapar::vulkan {

    struct Frame {
        VkFence idle = VK_NULL_HANDLE;
        VkSemaphore aquired = VK_NULL_HANDLE;
        VkSemaphore rendered = VK_NULL_HANDLE;
    };





    class FrameSync {
    public:
        FrameSync() = default;
        FrameSync(VkDevice device, std::size_t max_frames) noexcept;
        FrameSync(FrameSync const&) = delete;
        FrameSync& operator=(FrameSync const&) = delete;
        FrameSync(FrameSync&&) = delete;
        FrameSync& operator=(FrameSync&&) = delete;
        ~FrameSync() noexcept;

        std::pair<Frame, VkResult> wait_idle() noexcept;
        void advance() noexcept;

    private:
        std::vector<Frame> frames_;
        std::size_t current_ = 0;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
