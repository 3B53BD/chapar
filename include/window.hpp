#ifndef CHAPAR_zsAZXR4uiTw7MV9Y_HEADER_GUARD
#define CHAPAR_zsAZXR4uiTw7MV9Y_HEADER_GUARD

#include <chapar/core/window.hpp>
#include <memory>

namespace chapar {

    class ApplicationWindow : public Window {
    public:
        ApplicationWindow() = default;
        ApplicationWindow(std::unique_ptr<WindowImpl> window);
        ApplicationWindow(ApplicationWindow const&) = delete;
        ApplicationWindow& operator=(ApplicationWindow const&) = delete;
        ApplicationWindow(ApplicationWindow&&) = delete;
        ApplicationWindow& operator=(ApplicationWindow&&) = delete;
        ~ApplicationWindow() noexcept;
    };
}

#endif
