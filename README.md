# Status

![](doc/static/application.png)

Spinning red box. I am working on a config.lua parser to make it more dynamic. Also it has been compiled for Arch Linux only.

ImGui is showing up by the use of second subpass that writes to swapchain attachment directly. Example shows some artifacts. Though RenderDoc can not see them. Descriptor set layouts are loaded from lua config file. But their usage is still hardcoded.





# Overview

- __[cmake/](cmake)__ - Helper cmake files.
- __[doc/](doc)__ - Documentation.
- __[example/](example)__ - Example projects that can be build with `chapar`.
- __[external/](external)__ - External dependencies that are provided with chapar.
- __[include/](include)__ - Public API of chapar.
- __[lib/](lib)__ - Some legacy files and lua little parser for 'config' type of files.
- __[script/](script)__ - Scripts to build project.
- __[source/](source)__ - Source files for chapar.
- __[test/](test)__ - Tests for chapar. Currently empty.
- __[tool/](tool)__ - Additional tools for chapar. Currently empty.





## CMake

- __[root CMakeLists.cmake](CMakeLists.cmake)__
    - Guards against building in the same directory as the root one.
    - Checks availabilty of packages: `doctest`, `fmt`, `glm`, `gsl-lite`, `Ktx`, `SDL2`, `TinyGLTF`.
    - Checks availabilty of modules: `vulkan`.
    - Currently all symbols of target shared libraries are visible.
    - Sets directories where to put target files: `CMAKE_LIBRARY_OUTPUT_DIRECTORY`, `CMAKE_ARCHIVE_OUTPUT_DIRECTORY`, `CMAKE_RUNTIME_OUTPUT_DIRECTORY`, `FAKE_INSTALL_OUTPUT_DIRECTORY`, `PACKAGE_OUTPUT_DIRECTORY`, `SHADER_OUTPUT_DIRECTORY`.
    - Adds custom target to install to fake directory at build directory.
- __[bin.cmake](cmake/bin.cmake)__ - Default template for executable targets.
- __[lib.cmake](cmake/lib.cmake)__ - Default template for library targets.
- __[config.cmake.in](cmake/config.cmake.in)__ - Package config that will be configured to support installation.
- __[warnings.cmake](cmake/warnings.cmake)__ - Assigns appropriate warnings for different compilers to `CHAPAR_WARNINGS`.





## [Example](example/simple)

File __[main.cpp](example/simple/main.cpp)__ is a temporary solution. Everything that is coded is written here first. Then it becomes separated and encapsulated. In other words it is a mess.





## Window

![](doc/static/window_white.png)\
__[image with black lines](doc/static/window_black.png)__

- __[core/window.hpp](include/core/window.hpp)__ - Window abstraction with bridge pattern: Window and WindowImpl.
- __[sdl/window.hpp](include/sdl/window.hpp)__ - SDL2 wrapper that implements WindowImpl interface and vulkan interface.
- __[vulkan/window.hpp](include/vulkan/window.hpp)__ - Window interface that vulkan needs to __be implemente__d.
__
Mkan/](include/graphics/port/vulkan)__ - Map
## [Graphics](include/graphics) structures.

- __[scene.hpp](include/graphics/scene.hpp)__ - Ss tructures and classes that are used as middleware between different implementations. As for now there are:
    - `enum class VertexComponent` - Programmable vertex layout when loading vertices from model files.
    - `struct Material` - Empty structure. Currently there is no material support.
    - `struct Primitive` - Contains correctly mapped vertices and indices, their count, index type and material.
    - `struct Mesh` - Collection of primitives.
    - `struct Node` - Stores its children and points to mesh by index. Loaders of model data have to load meshes first.
    - `class Image` - Interface of an image loader.
    - `struct Texture` - stores indices to image and sampler create info. Loaders of model data have to load images and sampler create infos first.
    - `struct Scene` - stores all loaded meshes, nodes, images, sampler create infos and textures. Sampler create info is stored as `VkSamplerCreateInfo` for simplicity.
- __[port/](include/graphics/port)__ - Implementation of graphics abstractions to access external dependencies (like `tinygltf`).
    - __[tinygltf/](include/graphics/port/tinygltf)__ - Maps __[TinyGLTF](https://github.com/syoyo/tinygltf)__ structures to __[Graphics](include/graphics)__ structures.
    - __[vulkan/](include/graphics/port/vulkan)__ - Maps __[Graphics](include/graphics)__ structures to __[Vulkan](include/vulkan)__ structures.





## [Vulkan](include/vulkan)

### [allocator.hpp](include/vulkan/allocator.hpp)

`class Allocator` is a dull wrapper around `VmaAllocator` to use RAII. Provides a static function that returns a default-initialized `VkImageCreateInfo` structure.

### [attachment.hpp](include/vulkan/attachment.hpp)

`class Attachment` is an interface for image attachments that will be used by framebuffer with `class ColorAttachment` and `class DepthAttachment` as its implementations.

### [buffer.hpp](include/vulkan/buffer.hpp)

`class Buffer` is a wrapper around `VkBuffer` and allocation/deallocation of memory with __[VMA](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator)__. Provides a helper static function that returns a default-initialized `VkBufferCreateInfo` structure. Has `map`, `unmap`, `memcpy(offset, source)`, `size`, `descriptor`, `handle` functions.

`class InputBuffer` is an interface of special buffer that stores vertex and/or index data on gpu side. `class VertexBuffer` and `class IndexVertexBuffer` are his implementations. They aggregate `class Buffer`. Can bind their internal buffers to command buffer, draw stored vertices and indices, return their sizes, descriptors, handles.

`class UniformBuffer` is a special wrapper around buffer that stores uniform data.

### [camera.hpp](include/vulkan/camera.hpp)

`class Camera` can be described as a special wrapper around uniform buffer. It is used to move, rotate and set aspect of camera. As for buffer functionality it has functions to return descriptor and update buffer.

### [command.hpp](include/vulkan/command.hpp)

`class CommandPool` is a wrapper around `VkCommandPool`. It allocates and frees command buffers.

`class CommandBuffer` is a wrapper around `VkCommandBuffer`. It begins/ends command buffer, begins/ends render pass, sets pipeline barrier and copies from buffer to other buffer or image.

### [descriptor.hpp](include/vulkan/descriptor.hpp)

`class DescriptorPool` is a wrapper around `VkDescriptorPool` with the ability to allocate sets.

`class DescriptorSetLayout` is a dull wrapper around `VkDescriptorSetLayout`.

There is no wrapper for `VkDescriptorSet` as for now.

`write_descriptor_set` is a function with two overloads: one is for buffer descriptor info, the other is for image descriptor info.

### [device.hpp](include/vulkan/device.hpp)

`class Device` is wrapper around `VkDevice`. It provides ability to wait untill device is idle and begin/end one time submit command buffer.

### [frame_sync.hpp](include/vulkan/frame_sync.hpp)

`class FrameSync` is used to synchronize frames.

### [framebuffer.hpp](include/vulkan/framebuffer.hpp)

`class Framebuffer` is a dull wrapper around `VkFramebuffer`.

### [image.hpp](include/vulkan/image.hpp)

`class Image` is a wrapper around `VkImage`. Has a static function that returns a default-initialized `VkImageCreateInfo` structure and a function to transition layout. It provides access to layout, usage, format, extent, mip_levels and a vulkan handle.

`class ImageView` is a dull wrapper around `VkImageView`. Can be created from vulkan data types (convenient for creating from swapchain images) and chapar data types.

`class Sampler` is a dull wrapper around `VkSampler`. Provides a static function that returns a default-initialized `VkImageCreateInfo` structure.

### [instance.hpp](include/vulkan/instance.hpp)

`class Instance` is a wrapper around `VkInstance`. It provides access to enumerate physical devices.

### [mesh.hpp](include/vulkan/mesh.hpp)

`class Primitive` is an interface of a primitive that can be binded and drawn to command buffer.

`class IndexedPrimitive` implements `Primitive` interface and can be described as a special wrapper around `IndexVertexBuffer`.

`class NonIndexedPrimitive` implements `Primitive` interface and can be described as a special wrapper around `VertexBuffer`.

`class Mesh` stores primitves and provides only one function to record primitives to command buffer.

### [node.hpp](include/vulkan/node.hpp)

`class Node` stores a pointer to `Mesh` and information about its translation, rotation and scale. It can record `Mesh` to command buffer and update uniform buffers. It also stores its children and a pointer to its parent.

### [physical_device.hpp](include/vulkan/physical_device.hpp)

`class PhysicalDevice` is a wrapper around `VkPhysicalDevice`. It provides access to maximum supported sampler count, queue family properties, support of surface, surface formats, surface capabilities, present modes, format properties, physical device features and properties.

### [pipeline.hpp](include/vulkan/pipeline.hpp)

`create_pipeline_layout_create_info` function to create valid `VkPipelineLayoutCreateInfo` structure from vector of `VkDescriptorSetLayout`.

`class PipelineCreateInfoBuilder` is a helper class to create valid `VkGraphicsPipelineCreateInfo` structure.

`class Pipeline` is a wrapper around `VkPipeline`. Has functions to bind itself and descriptor sets to a draw call.

### [queue.hpp](include/vulkan/queue.hpp)

`class QueueFamily` is a helper class to parse queue family capabilities.

`class Queue` is a wrapper class. Can submit command buffers and wait idle.

### [render_pass.hpp](include/vulkan/render_pass.hpp)

`class Subpass` is usefull to create valid `VkSubpassDescription` and `VkSubpassDependency` with solid interface.

`class RenderPass` is a dull wrapper around `VkRenderPass`.

### [scene.hpp](include/vulkan/scene.hpp)

`class Scene` stores associated vertex buffers, nodes, textures, descriptor pool, descriptor set layout, descriptor sets and pipeline. Has a function to bind all necessary commands to draw calls (vertex buffers, textures, pipelines).

### [shader.hpp](include/vulkan/shader.hpp)

`class Shader` is an interface for `class VertexShader` and `class FragmentShader`. Each one implements different initialization of `VkPipelineShaderStageCreateInfo` structure.

### [surface.hpp](include/vulkan/surface.hpp)

`class Surface` is a wrapper around `VkSurfaceKHR` and additionally stores surface format. Format can be determined only after creation of `Surface` object. So it needs to be set with separate call.

### [swapchain.hpp](include/vulkan/swapchain.hpp)

`class SwapchainAttachment` is exclusively used by swapchain.

`class Swapchain` is a wrapper around `VkSwapchainKHR`. It gives access to images, image count, current image index, image extent and image format. It also provides ability to resize internal images, request next image, wait idle and present image.

### [texture.hpp](include/vulkan/texture.hpp)

`class Texture` is a handler that gives access to resources like image, image view and sampler. It also create descriptor from these resources.

### [window.hpp](include/vulkan/window.hpp)

See section _Window_.





# Requirements

- [shaderc](https://github.com/google/shaderc/tree/main/glslc) - Used in example/simple/shaders/CMakeLists.txt.
- [gsl-lite](https://github.com/gsl-lite/gsl-lite) - According to [cpp core guidelines](https://github.com/isocpp/CppCoreGuidelines) I am obligated to use it.
- [TinyGLTF](https://github.com/syoyo/tinygltf)
- [SDL2](https://www.libsdl.org/download-2.0.php)

## [SDL2](https://www.libsdl.org/index.php)

Have to be provided by user. For example with:
```bash
$ sudo pacman -S sdl2
```

## [STB](https://github.com/nothings/stb)

`chapar` builds `stb_image` and `stb_image_write` as separate libraries. Nothing to do externally to make it work.

## [TinyGLTF](https://github.com/syoyo/tinygltf)

Have to be provided by a user. [TinyGLTF](https://github.com/syoyo/tinygltf) is a header-only library, so no library is built. Just install header files.

```bash
$ mkdir build && cd build
$ cmake --install .
```

After this `chapar` will load `<tiny_gltf.h>` header at __[tiny_gltf.hpp](external/tiny_gltf/source/tiny_gltf.cpp)__ with several macros enabled: `TINYGLTF_IMPLEMENTATION`, `STB_IMAGE_IMPLEMENTATION`, `STB_IMAGE_WRITE_IMPLEMENTATION`. For this to work `chapar` provides `stb_image` and `stb_image_write`.

## [Vulkan Memory Allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator)

`chapar` builds `vma` by itself. Nothing to do externally to make it work.





# Todo

## Testing

Currently there is no strict API, so tests will be too much of a burden than a helping hand.

- __[doctest](https://github.com/onqtam/doctest)__

## Logging

I'm still not sure which one to pick.

- __[quill](https://github.com/quilljs/quill)__
- __[fmt](https://github.com/fmtlib/fmt)__
- __[NanoLog](https://github.com/PlatformLab/NanoLog)__
- __[reckless](https://github.com/mattiasflodin/reckless)__
- __[format](https://en.cppreference.com/w/cpp/utility/format)__

## Error handling

Some say that having a dedicated class for handling errors can be a security issue, as an overflow can lead to knowing the address of that class. Since it doesn't have to be so secure and I want to try to create it, there will be one.

Currently there is __[error.hpp](include/core/error.hpp)__ with `class Exception` that stores error message string.





# Building

```bash
$ mkdir build
$ bash script/make.sh -d # debug mode
```

## Scripts

### [make.sh](script/make.sh)

Builds debug and release modes in different directories. So it is safe to build all modes together by option: `-dr`.

Build mode:\
`-*d*` - debug.\
`-*r*` - release.
