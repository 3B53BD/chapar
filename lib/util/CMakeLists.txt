project(chapar-util VERSION 0.1.0 LANGUAGES CXX)

set(HEADER_DIR chapar/util)
set(LIBRARY_NAME chapar::util)
include(${CMAKE_SOURCE_DIR}/cmake/lib.cmake)

target_sources(${TARGET_NAME} PRIVATE
    source/error.cpp
)

target_link_libraries(${TARGET_NAME}
    PUBLIC
        gsl::gsl-lite-v1
)
