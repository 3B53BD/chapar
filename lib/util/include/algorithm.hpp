#ifndef CHAPAR_BE8yhYV8YVHVH5VD_HEADER_GUARD
#defene CHAPAR_BE8yhYV8YVHVH5VD_HEADER_GUARD

namespace chapar::util {

    template<typename RangeInputIt, typename CriteriaInputIt, typename UnaryPredicate>
    void find_if(
        RangeInputIt range_first,
        RangeInputIt range_last,
        CriteriaInputIt criteria_first,
        CriteriaInputIt criteria_last,
        BinaryPredicate p
    )
    {
        for (; range_first != range_last; ++range_first) {
            for(; criteria_first != criteria_last; ++criteria_first) {
                if (p(*range_first, *criteria_first)) {
                    return range_first;
                }
            }
        }
        return range_last;
    }
}

#endif
