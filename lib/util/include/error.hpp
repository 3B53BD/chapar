#ifndef CHAPAR_ERROR_HEADER_GUARD
#define CHAPAR_ERROR_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <stdexcept>

namespace chapar {

    class Exception : public std::exception {
    public:
        Exception() = default;
        Exception(std::string message) : message_{std::move(message)} {}

        gsl::czstring what() const noexcept { return message_.c_str(); }

    private:
        std::string message_;
    };
}

#endif
