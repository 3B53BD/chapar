#include "chapar/os/window.hpp"

namespace chapar::os {

    std::string Window::title() const noexcept
    {
        return impl_->title();
    }

    std::pair<int, int> Window::origin() const noexcept
    {
        return impl_->origin();
    }

    std::pair<int, int> Window::extent() const noexcept
    {
        return impl_->extent();
    }

    bool Window::is_fullscreen() const noexcept
    {
        return impl_->is_fullscreen();
    }

    bool Window::is_focused() const noexcept
    {
        return impl_->is_focused();
    }

    bool Window::is_resizable() const noexcept
    {
        return impl_->is_resizable();
    }

    void Window::set_origin(int x, int y) const noexcept
    {
        impl_->set_origin(x, y);
    }

    void Window::set_extent(int width, int height) const noexcept
    {
        impl_->set_extent(width, height);
    }

    void Window::set_resizable(bool resizable) noexcept
    {
        impl_->set_resizable(resizable);
    }

    void Window::set_fullscreen(WindowFullscreanMode mode) noexcept
    {
        impl_->set_fullscreen(mode);
    }

    void Window::set_maximize() const noexcept
    {
        impl_->set_maximize();
    }

    void Window::set_minimize() const noexcept
    {
        impl_->set_minimize();
    }

    void Window::set_focus() noexcept
    {
        impl_->set_focus();
    }

    void Window::set_title(std::string_view title) const noexcept
    {
        impl_->set_title(title);
    }

    std::shared_ptr<WindowImpl> Window::window_impl() const noexcept
    {
        return impl_;
    }
}
