#ifndef CHAPAR_VULKAN_COMMAND_HEADER_GUARD
#define CHAPAR_VULKAN_COMMAND_HEADER_GUARD

#include "chapar/vulkan/queue.hpp"
#include <vulkan/vulkan.h>
#include <vector>
#include <span>

namespace chapar::vulkan {

    class Device;
    class CommandBuffer;





    // ==================================================
    // CommandPool
    // ==================================================

    class CommandPool {
    public:
        CommandPool() = default;
        CommandPool(
            Device const& device,
            VkCommandPoolCreateFlags flags,
            Queue queue
        );
        CommandPool(CommandPool&) = delete;
        CommandPool& operator=(CommandPool&) = delete;
        CommandPool(CommandPool&&) = delete;
        CommandPool& operator=(CommandPool&&) = delete;
        ~CommandPool() noexcept;

        std::vector<CommandBuffer> allocate_command_buffers(uint32_t count) const;
        void free_command_buffers(std::span<CommandBuffer> command_buffers) const noexcept;

        VkCommandPool handle() const noexcept { return command_pool_; }

    private:
        VkCommandPool command_pool_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
        Queue queue_;
    };





    // ==================================================
    // CommandBuffer
    // ==================================================

    class CommandBuffer {
    public:
        CommandBuffer() = default;
        CommandBuffer(VkCommandBuffer command_buffer) noexcept;
        CommandBuffer(CommandBuffer&) = default;
        CommandBuffer& operator=(CommandBuffer&) = default;
        CommandBuffer(CommandBuffer&&) = default;
        CommandBuffer& operator=(CommandBuffer&&) = default;
        ~CommandBuffer() = default;

        VkResult begin(VkCommandBufferUsageFlags flags) const noexcept;
        VkResult end() const noexcept;

        VkCommandBuffer handle() const noexcept { return command_buffer_; }

    private:
        VkCommandBuffer command_buffer_ = VK_NULL_HANDLE;
    };
}

#endif
