#infdef CHAPAR_5SP67tcEs9zuW5yu_HEADER_GUARD
#define CHAPAR_5SP67tcEs9zuW5yu_HEADER_GUARD

namespace chapar::vulkan {

    // ==================================================
    // Use Cases
    // ==================================================

    struct QueueCapabilities {
        using Bits = std::bitset<6>;
        static constexpr Bits graphics_mask = 1 << 0;
        static constexpr Bits compute_mask = 1 << 1;
        static constexpr Bits transfer_mask = 1 << 2;
        static constexpr Bits sparse_binding_mask = 1 << 3;
        static constexpr Bits protected_mask = 1 << 4;
        static constexpr Bits present_mask = 1 << 5;
    };





    // ==================================================
    // Use Cases
    // ==================================================

    class QueueFamily {
    public:
        QueueFamily() = default;
        QueueFamily(
            uint32_t index,
            QueueCapabilities::Bits capabilities
        );
        QueueFamily(QueueFamily&) = default;
        QueueFamily& operator=(QueueFamily&) = default;
        QueueFamily(QueueFamily&&) = default;
        QueueFamily& operator=(QueueFamily&&) = default;
        ~QueueFamily() = default;

        uint32_t index() const noexcept;
        QueueCapabilities::Bits const& capabilities() const noexcept;

    private:
        uint32_t index_ = UINT32_MAX;
        QueueCapabilities::Bits capabilities_;
    };





    // ==================================================
    // Use Cases
    // ==================================================

    QueueCapabilities::Bits queue_capabilities(
        PhysicalDevice const& physical_device,
        VkQueueFamilyProperties properties,
        uint32_t index = UINT32_MAX,
        VkSurfaceKHR surface = VK_NULL_HANDLE
    );

    std::vector<QueueFamily> create_queue_families(
        PhysicalDevice const& physical_device,
        Surface const& surface
    ) noexcept;





    // ==================================================
    // Queue
    // ==================================================

    class Queue {
    public:
        Queue() = default;
        Queue(VkQueue queue, uint32_t family)
        Queue(Queue&) = default;
        Queue& operator=(Queue&) = default;
        Queue(Queue&&) = default;
        Queue& operator=(Queue&&) = default;
        ~Queue() = default;

        void wait_idle() const noexcept;
        uint32_t family() const noexcept;
        VkQueue handle() const noexcept;

    private:
        VkQueue queue_ = VK_NULL_HANDLE;
        uint32_t index_ = UINT32_MAX;
    };
}

#endif
