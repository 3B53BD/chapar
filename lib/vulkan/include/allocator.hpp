#ifndef CHAPAR_VULKAN_ALLOCATOR_HEADER_GUARD
#define CHAPAR_VULKAN_ALLOCATOR_HEADER_GUARD

#include "chapar/vma/vma.hpp"

namespace chapar::vulkan {

    class Instance;
    class PhysicalDevice;
    class Device;

    class Allocator {
    public:
        Allocator() = default;
        Allocator(
            Instance const& instance,
            PhysicalDevice const& physical_device,
            Device const& device
        );
        Allocator(Allocator&) = delete;
        Allocator& operator=(Allocator&) = delete;
        Allocator(Allocator&&) = delete;
        Allocator& operator=(Allocator&&) = delete;
        ~Allocator() noexcept;

        VmaAllocator handle() const noexcept { return allocator_; }

    private:
        VmaAllocator allocator_ = VK_NULL_HANDLE;
    };
}

#endif
