#ifndef CHAPAR_VULKAN_TEXTURE_HEADER_GUARD
#define CHAPAR_VULKAN_TEXTURE_HEADER_GUARD

namespace chapar::vulkan {

    class Texture : public Attachment {
    public:
        Texture() = default;
        /**
         * Creates texture that can be used as attachment
         */
        Texture(
            Device const& device,
            Allocator const& allocator,
            VkFormat format,
            VkImageUsageFlags usage,
            uint32_t width,
            uint32_t height,
        );
        Texture(Texture&) = delete;
        Texture& operator=(Texture&) = delete;
        Texture(Texture&&) = delete;
        Texture& operator=(Texture&&) = delete;
        ~Texture() noexcept;

    private:
        uint32_t width_;
        uint32_t height_;
        uint32_t mip_levels_;
        VkFormat format_;
        VkClearValue clear_value_;
        Image image_;
        ImageView image_view_;
        Sampler sampler_;
    };
}

#endif
