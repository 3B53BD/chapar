#ifndef CHAPAR_y4FfURFtwenJhi92_HEADER_GUARD
#define CHAPAR_y4FfURFtwenJhi92_HEADER_GUARD

namespace chapar::vulkan {

    class Instance {
    public:
        Instance(sdl::VulkanWindow const& window);

    private:
        std::unique_ptr<core::Instance> instance_;
        core::PhysicalDevice physical_device_;
        std::unique_ptr<core::Device> device_;
        std::unique_ptr<core::Allocator> allocator_;
        Queue graphics_queue_;
        Queue present_queue_;
    };
}

#endif
