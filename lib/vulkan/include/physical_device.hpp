#ifndef CHAPAR_x47NR5vCCf25SUgu_HEADER_GUARD
#define CHAPAR_x47NR5vCCf25SUgu_HEADER_GUARD

namespace chapar::vulkan {

    // ==================================================
    // Use Cases
    // ==================================================

    class PhysicalDevice {
    public:
        PhysicalDevice() = default;
        PhysicalDevice(VkPhysicalDevice physical_device) noexcept;
        PhysicalDevice(PhysicalDevice const&) = default;
        PhysicalDevice& operator=(PhysicalDevice const&) = default;
        PhysicalDevice(PhysicalDevice&&) = default;
        PhysicalDevice& operator=(PhysicalDevice&&) = default;
        ~PhysicalDevice() = default;

        std::vector<VkQueueFamilyProperties> queue_family_properties() const noexcept;
        std::vector<VkSurfaceFormatKHR> formats(VkSurfaceKHR surface) const noexcept;
        VkSurfaceCapabilitiesKHR capabilities(VkSurfaceKHR surface) const noexcept;
        std::vector<VkPresentModeKHR> present_modes(VkSurface surface) const noexcept;
        VkFormatProperties format_properties(VkFormat format) const noexcept;

    private:
        VkPhysicalDevice physical_device_ = VK_NULL_HANDLE;
        std::vector<VkExtensionProperties> extensions_;
        VkPhysicalDeviceFeatures features_ = {};
        VkPhysicalDeviceProperties properties_ = {};
        VkPhysicalDeviceMemoryProperties memory_properties_ = {};
    };





    // ==================================================
    // Use Cases
    // ==================================================

    class PhysicalDeviceUseCase {
    public:
        static std::vector<PhysicalDevice> PhysicalDeviceUseCase::physical_devices(Instance const& instance) noexcept;

        static std::pair<VkSurfaceFormatKHR, bool> supported_format(
            VkSurfaceKHR surface,
            std::span<VkSurfaceFormatKHR> candidates
        ) noexcept;

        static VkSampleCountFlagBits max_samples() noexcept;

        static VkFormat choose_format(
            PhysicalDevice const& physical_device,
            std::vector<VkFormat> const& candidates,
            VkImageTiling tiling,
            VkFormatFeatureFlags features
        ) noexcept;

        static VkFormat default_depth_format() noexcept;
    };
}

#endif
