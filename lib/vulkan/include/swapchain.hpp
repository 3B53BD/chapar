#ifndef CHAPAR_gtyUpVRN2r47R6ve_HEADER_GUARD
#define CHAPAR_gtyUpVRN2r47R6ve_HEADER_GUARD

namespace chapar::vulkan {

    class SwapchainAttachment : public Attachment {
    public:

    private:

    };

    class Swapchain {
    public:
        Swapchain() = default;
        Swapchain(
            VkDevice device,
            VkSurfaceKHR surface,
            VkSurfaceTransformFlagBitsKHR pre_transform,
            uint32_t image_count,
            VkFormat image_format,
            VkColorSpaceKHR image_color_space,
            VkExtent2D image_extent,
            std::vector<uint32_t> const& queue_family_indices,
            VkPresentModeKHR present_mode
        );
        Swapchain(Swapchain const&) = delete;
        Swapchain& operator=(Swapchain const&) = delete;
        Swapchain(Swapchain&&) = delete;
        Swapchain& operator=(Swapchain&&) = delete;
        ~Swapchain() noexcept;

        void resize(VkExtent2D extent);

        VkResult aquire_next_image(VkSemaphore& semaphore);

        std::vector<VkImage> images() const noexcept;
        uint32_t image_count() const noexcept;
        uint32_t image_index() const noexcept;
        uint32_t handle() const noexcept;

    private:
        void create();

        uint32_t image_index_ = UINT32_MAX;
        uint32_t image_count_ = 0;
        VkExtent2D image_extent_ = {};
        VkSwapchainKHR swapchain_ = VK_NULL_HANDLE;
        VkSurfaceFormatKHR image_format_ = {};
        VkPresentModeKHR present_mode_ = {};
        VkSurface surface_ = VK_NULL_HANDLE;
        std::vector<uint32_t> queue_families_;
        VkSurfaceTransformFlagBitsKHR pre_transform_ = {};
    };





    // ==================================================
    // Use Cases
    // ==================================================

    std::unique_ptr<Swapchain> create_swapchain(
        os::Window const& window,
        Surface const& surface,
        PhysicalDevice const& physical_device,
        Device const& device,
        std::vector<uint32_t> const& queue_family_indices
    )
}

#endif
