#ifndef CHAPAR_VULKAN_RENDER_PASS_HEADER_GUARD
#define CHAPAR_VULKAN_RENDER_PASS_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <vector>
#include <utility>
#include <optional>

namespace chapar::vulkan {

    class Device;

    VkAttachmentDescription attachment_description() noexcept;

    struct Subpass {
        std::vector<uint32_t> color_refs;
        std::vector<uint32_t> resolve_refs;
        std::optional<uint32_t> depth_ref;
        std::vector<std::pair<Subpass*, VkSubpassDependency>> dependencies;
    };

    class RenderPass {
    public:
        RenderPass() = default;
        RenderPass(
            Device const& device,
            std::vector<VkAttachmentDescription> attachments,
            std::vector<Subpass> const& subpasses
        );
        RenderPass(RenderPass&) = delete;
        RenderPass& operator=(RenderPass&) = delete;
        RenderPass(RenderPass&&) = delete;
        RenderPass& operator=(RenderPass&&) = delete;
        ~RenderPass() noexcept;

    private:
        std::vector<Subpass> subpasses_;
        VkRenderPass render_pass_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
