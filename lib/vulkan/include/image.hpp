#ifndef CHAPAR_VULKAN_IMAGE_HEADER_GUARD
#define CHAPAR_VULKAN_IMAGE_HEADER_GUARD

#include "chapar/vma/vma.hpp"
#include <vulkan/vulkan.h>
#include <memory>

namespace chapar::vulkan {

    class Device;



    

    // ==================================================
    // Image
    // ==================================================

    class Image {
    public:
        Image() = default;
        Image(
            VmaAllocator allocator,
            VkImageCreateInfo const& image_create_info,
            VmaAllocationCreateInfo const& allocation_create_info
        );
        Image(Image&) = delete;
        Image& operator=(Image&) = delete;
        Image(Image&&) = delete;
        Image& operator=(Image&&) = delete;
        ~Image() noexcept;

        VkImageLayout layout() const noexcept
        {
            return layout_;
        }
        VkImageUsageFlags usage() const noexcept
        {
            return usage_;
        }
        VkFormat format() const noexcept
        {
            return format_;
        }
        VkExtent3D const& extent() const noexcept
        {
            return extent_;
        }
        uint32_t mip_levels() const noexcept
        {
            return mip_levels_;
        }
        VkImage handle() const noexcept
        {
            return image_;
        }

    private:
        VmaAllocator allocator_ = VK_NULL_HANDLE;
        VmaAllocation allocation_ = VK_NULL_HANDLE;
        VmaAllocationInfo allocation_info_ = {};
        VkImage image_ = VK_NULL_HANDLE;
        VkImageLayout layout_ = VK_IMAGE_LAYOUT_UNDEFINED;
        VkImageUsageFlags usage_ = {};
        VkFormat format_ = VK_FORMAT_UNDEFINED;
        VkExtent3D extent_ = {};
        uint32_t mip_levels_ = 1;
    };





    // ==================================================
    // ImageBuilder
    // ==================================================

    class ImageBuilder {
    public:
        ImageBuilder() noexcept;
        ImageBuilder& reset() noexcept;
        ImageBuilder& set_allocator(VmaAllocator allocator) noexcept;
        ImageBuilder& set_format(VkFormat format) noexcept;
        ImageBuilder& set_extent(VkExtent3D extent) noexcept;
        ImageBuilder& set_mip_levels(uint32_t mip_levels) noexcept;
        ImageBuilder& set_samples(VkSampleCountFlagBits samples) noexcept;
        ImageBuilder& set_tiling(VkImageTiling tiling) noexcept;
        ImageBuilder& set_image_usage(VkImageUsageFlags usage) noexcept;
        ImageBuilder& set_memory_usage(VmaMemoryUsage usage) noexcept;
        std::unique_ptr<Image> image() const noexcept;

    private:
        VkImageCreateInfo image_;
        VmaAllocationCreateInfo allocation_{};
        VmaAllocator allocator_;
    };





    // ==================================================
    // ImageView
    // ==================================================

    class ImageView {
    public:
        ImageView() = default;
        ImageView(
            Device const& device,
            Image const& image,
            VkImageAspectFlags aspect_mask
        );
        ImageView(ImageView&) = delete;
        ImageView& operator=(ImageView&) = delete;
        ImageView(ImageView&&) = delete;
        ImageView& operator=(ImageView&&) = delete;
        ~ImageView() noexcept;

        VkImageView handle() const noexcept
        {
            return view_;
        }

    private:
        VkImageView view_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
