#ifndef CHAPAR_VULKAN_FRAMEBUFFER_HEADER_GUARD
#define CHAPAR_VULKAN_FRAMEBUFFER_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>

namespace chapar::vulkan {

    class Attachment;

    class Framebuffer {
    public:
        Framebuffer() = default;
        Framebuffer(
            VkRenderPass const& render_pass,
            std::vector<Attachment const*> const& attachments,
            uint32_t width,
            uint32_t height
        );
        Framebuffer(Framebuffer&) = delete;
        Framebuffer& operator=(Framebuffer&) = delete;
        Framebuffer(Framebuffer&&) = delete;
        Framebuffer& operator=(Framebuffer&&) = delete;
        ~Framebuffer() noexcept;

    private:
        VkFramebuffer framebuffer_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
