#ifndef CHAPAR_VULKAN_SYNC_HEADER_GUARD
#define CHAPAR_VULKAN_SYNC_HEADER_GUARD

namespace chapar::vulkan {

    struct FrameSync {
        VkFence idle = VK_NULL_HANDLE;
        VkSemaphore aquired = VK_NULL_HANDLE;
        VkSemaphore rendered = VK_NULL_HANDLE;
    };

    class Synchronization {
    public:
        Synchronization() = default;
        Synchronization(Device const& device, std::size_t max_frames) noexcept;
        Synchronization(Synchronization&) = delete;
        Synchronization& operator=(Synchronization&) = delete;
        Synchronization(Synchronization&&) = delete;
        Synchronization& operator=(Synchronization&&) = delete;
        ~Synchronization() = default;

        std::pair<FrameSync, VkResult> wait_idle() noexcept;
        void advance() noexcept;

    private:
        std::vector<FrameSync> frames_;
        std::size_t current_ = 0;
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
