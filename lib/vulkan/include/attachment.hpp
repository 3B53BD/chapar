#ifndef CHAPAR_VULKAN_ATTACHMENT_HEADER_GUARD
#define CHAPAR_VULKAN_ATTACHMENT_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <memory>

namespace chapar::vulkan {

    class Allocator;
    class Device;
    class Image;
    class ImageView;





    // ==================================================
    // Attachment
    // ==================================================

    class Attachment {
    public:
        virtual uint32_t width() const noexcept = 0;
        virtual uint32_t height() const noexcept = 0;
        virtual VkFormat format() const noexcept = 0;
        virtual VkClearValue clear_value() const noexcept = 0;
        virtual ImageView const& image_view() const noexcept = 0;
    };





    // ==================================================
    // ColorAttachment
    // ==================================================

    class ColorAttachment : public Attachment {
    public:
        ColorAttachment() = default;
        ColorAttachment(
            Device const& device,
            Allocator const& allocator,
            VkFormat format,
            uint32_t width,
            uint32_t height,
            VkSampleCountFlagBits samples
        );
        ColorAttachment(ColorAttachment&) = delete;
        ColorAttachment& operator=(ColorAttachment&) = delete;
        ColorAttachment(ColorAttachment&&) = delete;
        ColorAttachment& operator=(ColorAttachment&&) = delete;
        ~ColorAttachment() noexcept;

        uint32_t width() const noexcept override;
        uint32_t height() const noexcept override;
        VkFormat format() const noexcept override;
        VkClearValue clear_value() const noexcept override;
        ImageView const& image_view() const noexcept override;

    private:
        std::unique_ptr<Image> image_;
        std::unique_ptr<ImageView> image_view_;
    };





    // ==================================================
    // DepthAttachment
    // ==================================================

    class DepthAttachment : public Attachment {
    public:
        DepthAttachment() = default;
        DepthAttachment(
            Device const& device,
            Allocator const& allocator,
            VkFormat format,
            uint32_t width,
            uint32_t height,
            VkSampleCountFlagBits samples
        );
        DepthAttachment(DepthAttachment&) = delete;
        DepthAttachment& operator=(DepthAttachment&) = delete;
        DepthAttachment(DepthAttachment&&) = delete;
        DepthAttachment& operator=(DepthAttachment&&) = delete;
        ~DepthAttachment() noexcept;

        uint32_t width() const noexcept override;
        uint32_t height() const noexcept override;
        VkFormat format() const noexcept override;
        VkClearValue clear_value() const noexcept override;
        ImageView const& image_view() const noexcept override;

    private:
        std::unique_ptr<Image> image_;
        std::unique_ptr<ImageView> image_view_;
    };
}

#endif
