#ifndef CHAPAR_VULKAN_SURFACE_HEADER_GUARD
#define CHAPAR_VULKAN_SURFACE_HEADER_GUARD

#include <vulkan/vulkan.h>

namespace chapar::sdl {
    class VulkanWindow;
}

namespace chapar::vulkan {

    class Instance;

    class Surface {
    public:
        Surface() = default;
        Surface(
            Instance const& instance,
            sdl::VulkanWindow const& window
        );
        Surface(Surface&) = delete;
        Surface& operator=(Surface&) = delete;
        Surface(Surface&&) = delete;
        Surface& operator=(Surface&&) = delete;
        ~Surface() noexcept;

        VkSurfaceKHR handle() const noexcept { return surface_; }

        void set_format(VkSurfaceFormatKHR format) noexcept;
        VkSurfaceFormatKHR const& format() const noexcept;

    private:
        VkSurfaceFormatKHR format_ = {};
        VkSurfaceKHR surface_ = VK_NULL_HANDLE;
        VkInstance instance_ = VK_NULL_HANDLE;
    };
}

#endif
