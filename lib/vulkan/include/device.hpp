#ifndef CHAPAR_mPboGDQxvTS6dWBp_HEADER_GUARD
#define CHAPAR_mPboGDQxvTS6dWBp_HEADER_GUARD

namespace chapar::vulkan {

    // ==================================================
    // Use Cases
    // ==================================================

    class Device {
    public:
        Device() = default;
        Device(
            VkPhysicalDevice physical_device,
            std::vector<uint32_t> const& queue_family_indices,
            std::vector<gsl::czstring> const& validation_layers,
            std::vector<gsl::czstring> const& enabled_extensions,
            VkPhysicalDeviceFeatures const& features
        );
        Device(Device&) = delete;
        Device& operator=(Device&) = delete;
        Device(Device&&) = delete;
        Device& operator=(Device&&) = delete;
        ~Device() noexcept;

        void wait_idle() const noexcept;
        Queue queue(uint32_t family) const noexcept;
        VkDevice handle() const noexcept;

    private:
        VkDevice device_ = VK_NULL_HANDLE;
    };





    // ==================================================
    // Use Cases
    // ==================================================

    std::unique_ptr<Device> device(
        Instance const& instance,
        PhysicalDevice const& physical_device,
        std::vector<uint32_t> queue_family_indices,
        std::vector<gsl::czstring> const& enabled_extensions,
        VkPhysicalDeviceFeatures const& features
    );
}

#endif
