#ifndef CHAPAR_5ZAfHBKHoPqqxc7E_HEADER_GUARD
#define CHAPAR_5ZAfHBKHoPqqxc7E_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <span>
#include <vector>

namespace chapar::sdl {
    class VulkanWindow;
}

namespace chapar::vulkan::core {
    namespace details {} // Atom C++ syntax highlight fix :]

    class PhysicalDevice;

    class Instance {
    public:
        Instance() = default;
        Instance(sdl::VulkanWindow const& window);
        Instance(Instance const&) = delete;
        Instance& operator=(Instance const&) = delete;
        Instance(Instance&&) = delete;
        Instance& operator=(Instance&&) = delete;
        ~Instance() noexcept;

        std::vector<gsl::czstring> validation_layers() const noexcept;
        std::vector<VkPhysicalDevice> physical_devices() const noexcept;
        VkInstance handle() const noexcept { return instance_; }

    private:
#ifndef NDEBUG
        void check_validation_layers_support() const;
        VkResult create_debug_utils_messenger_ext(
            VkDebugUtilsMessengerCreateInfoEXT const* create_info
        ) noexcept;
        void destroy_debug_utils_messenger_ext() noexcept;
#endif

        VkInstance instance_ = VK_NULL_HANDLE;
        std::vector<gsl::czstring> validation_layers_;
        std::vector<gsl::czstring> extensions_;

#ifndef NDEBUG
        VkDebugUtilsMessengerEXT debug_messenger_ = VK_NULL_HANDLE;
#endif
    };
}

#endif
