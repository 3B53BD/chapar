#ifndef CHAPAR_VULKAN_QUEUE_HEADER_GUARD
#define CHAPAR_VULKAN_QUEUE_HEADER_GUARD

#include <vulkan/vulkan.h>
#include <bitset>
#include <limits>

namespace chapar::vulkan {

    class Device;

    struct QueueCapabilities {
        using Bits = std::bitset<5>;
        static constexpr Bits graphics_bit = 1 << 0;
        static constexpr Bits compute_bit = 1 << 1;
        static constexpr Bits transfer_bit = 1 << 2;
        static constexpr Bits sparse_binding_bit = 1 << 3;
        static constexpr Bits protected_bit = 1 << 4;
        static constexpr Bits present_bit = 1 << 5;
    };

    class QueueFamily {
    public:
        QueueFamily() = default;
        QueueFamily(
            VkPhysicalDevice physical_device,
            VkQueueFamilyProperties properties,
            uint32_t index = std::numeric_limits<uint32_t>::max(),
            VkSurfaceKHR surface = VK_NULL_HANDLE
        );
        QueueFamily(QueueFamily&) = default;
        QueueFamily& operator=(QueueFamily&) = default;
        QueueFamily(QueueFamily&&) = default;
        QueueFamily& operator=(QueueFamily&&) = default;
        ~QueueFamily() = default;

        uint32_t index() const noexcept;
        QueueCapabilities::Bits const& capabilities() const noexcept;

    private:
        uint32_t index_ = UINT32_MAX;
        QueueCapabilities::Bits capabilities_;
    };

    class Queue {
    public:
        Queue() = default;
        Queue(Device const& device, uint32_t family);
        Queue(Queue&) = default;
        Queue& operator=(Queue&) = default;
        Queue(Queue&&) = default;
        Queue& operator=(Queue&&) = default;
        ~Queue() = default;

        void wait_idle() const noexcept;

        uint32_t family() const noexcept;

        VkQueue handle() const noexcept { return queue_; }

    private:
        VkQueue queue_ = VK_NULL_HANDLE;
        uint32_t family_ = UINT32_MAX;
    };
}

#endif
