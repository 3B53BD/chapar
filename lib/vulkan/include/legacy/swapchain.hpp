#ifndef CHAPAR_VULKAN_SWAPCHAIN_HEADER_GUARD
#define CHAPAR_VULKAN_SWAPCHAIN_HEADER_GUARD

#include "chapar/vulkan/attachment.hpp"
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

namespace chapar::os {
    class WindowImpl;
}

namespace chapar::vulkan {

    class Device;
    class Frames;
    class PhysicalDevice;
    class Queue;
    class Surface;





    // ==================================================
    // SwapchainAttachment
    // ==================================================

    class SwapchainAttachment : public Attachment {
    public:
        SwapchainAttachment() = default;
        SwapchainAttachment(
            VkDevice device,
            VkSwapchainKHR swapchain,
            VkFormat format,
            uint32_t width,
            uint32_t height
        );
        SwapchainAttachment(SwapchainAttachment&) = delete;
        SwapchainAttachment& operator=(SwapchainAttachment&) = delete;
        SwapchainAttachment(SwapchainAttachment&&) = delete;
        SwapchainAttachment& operator=(SwapchainAttachment&&) = delete;
        ~SwapchainAttachment() noexcept;

        uint32_t width() const noexcept override;
        uint32_t height() const noexcept override;
        VkFormat format() const noexcept override;
        VkClearValue clear_value() const noexcept override;
        ImageView const& image_view() const noexcept override;

        uint32_t image_count() const noexcept;

    private:
        std::unique_ptr<VkImage[]> images_;
        std::unique_ptr<VkImageView[]> image_views_;
        uint32_t image_count_ = 0;
        uint32_t image_index_ = UINT32_MAX;
        VkFormat format_ = VK_FORMAT_UNDEFINED;
        VkExtent2D extent_ = {};
        VkDevice device_ = VK_NULL_HANDLE;
    };





    // ==================================================
    // Swapchain
    // ==================================================

    class Swapchain {
    public:
        Swapchain() = default;
        Swapchain(
            os::WindowImpl const& window,
            Surface const& surface,
            PhysicalDevice const& physical_device,
            Device const& device,
            std::vector<uint32_t> const& queue_families
        );
        Swapchain(Swapchain&) = delete;
        Swapchain& operator=(Swapchain&) = delete;
        Swapchain(Swapchain&&) = delete;
        Swapchain& operator=(Swapchain&&) = delete;
        ~Swapchain() noexcept;

        VkResult aquire_next_image(VkSemaphore& semaphore);

        void wait_idle(VkFence fence) noexcept;

        VkResult present(
            Queue const& present_queue,
            std::vector<VkSemaphore> const& wait_semaphores
        ) const;

        SwapchainAttachment const& attachment() const noexcept;
        uint32_t image_count() const noexcept;
        uint32_t image_index() const noexcept;

    private:
        std::unique_ptr<SwapchainAttachment> attachment_;
        std::unique_ptr<VkFence[]> idle_images_;
        uint32_t image_index_ = UINT32_MAX;
        VkPresentModeKHR present_mode_ = {};
        VkSwapchainKHR swapchain_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;

        VkSwapchainKHR swapchain_ = VK_NULL_HANDLE;
        uint32_t image_count_ = 0;
        VkExtent2D image_extent_ = {};
        VkPresentModeKHR present_mode_ = {};
        std::vector<uint32_t> queue_family_indices_;
        VkSurfaceTransformFlagBitsKHR pre_transform_ = {};
        VkSurfaceFormatKHR format_ = {};
        VkSurfaceKHR surface_ = VK_NULL_HANDLE;
        VkDevice device_ = VK_NULL_HANDLE;
    };





    // ==================================================
    // SwapchainController
    // ==================================================

    class SwapchainController {
    public:
        SwapchainController() = default;

        void choose_present_mode();
        void choose_image_extent();
        void choose_image_count();

    private:
        Swapchain swapchain_;
        SwapchainAttachment attachment_;
    };
}

#endif
