#ifndef CHAPAR_VULKAN_PHYSICAL_DEVICE_HEADER_GUARD
#define CHAPAR_VULKAN_PHYSICAL_DEVICE_HEADER_GUARD

#include "chapar/vulkan/queue.hpp"
#include <vulkan/vulkan.h>
#include <span>
#include <utility>
#include <vector>

namespace chapar::vulkan {

    class PhysicalDevice {
    public:
        PhysicalDevice() = default;
        PhysicalDevice(VkPhysicalDevice physical_device);
        PhysicalDevice(PhysicalDevice const&) = default;
        PhysicalDevice& operator=(PhysicalDevice const&) = default;
        PhysicalDevice(PhysicalDevice&&) = default;
        PhysicalDevice& operator=(PhysicalDevice&&) = default;
        ~PhysicalDevice() = default;

        std::vector<QueueFamily> queue_families(VkSurfaceKHR surface = VK_NULL_HANDLE) const noexcept;

        std::pair<VkSurfaceFormatKHR, bool> supported_format(
            VkSurfaceKHR surface,
            std::span<VkSurfaceFormatKHR> candidates
        ) const noexcept;

        VkSurfaceCapabilitiesKHR capabilities(VkSurfaceKHR surface) const noexcept;

        VkSampleCountFlagBits max_samples() const noexcept;

        VkFormat choose_format(
            std::vector<VkFormat> const& candidates,
            VkImageTiling tiling,
            VkFormatFeatureFlags features
        ) const noexcept;

        VkFormat default_depth_format() const noexcept;

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Vulkan API wrapper
        // - - - - - - - - - - - - - - - - - - - - - - - - - -

        std::vector<VkQueueFamilyProperties> queue_family_properties() const noexcept;

        std::vector<VkSurfaceFormatKHR> formats(VkSurfaceKHR surface) const noexcept;

        std::vector<VkPresentModeKHR> present_modes() const noexcept;

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Getters
        // - - - - - - - - - - - - - - - - - - - - - - - - - -

        VkPhysicalDeviceFeatures const& features() const noexcept { return features_; }
        VkPhysicalDeviceProperties const& properties() const noexcept { return properties_; }
        VkPhysicalDevice handle() const noexcept { return physical_device_; }

    private:
        VkPhysicalDevice physical_device_ = VK_NULL_HANDLE;
        std::vector<VkExtensionProperties> extensions_;
        VkPhysicalDeviceFeatures features_ = {};
        VkPhysicalDeviceProperties properties_ = {};
        VkPhysicalDeviceMemoryProperties memory_properties_ = {};
    };
}

#endif
