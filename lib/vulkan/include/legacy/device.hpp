#ifndef CHAPAR_VULKAN_DEVICE_HEADER_GUARD
#define CHAPAR_VULKAN_DEVICE_HEADER_GUARD

#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <vector>

namespace chapar::vulkan {

    class Instance;
    class PhysicalDevice;

    class Device {
    public:
        Device() = default;
        Device(
            Instance const& instance,
            PhysicalDevice const& physical_device,
            std::vector<uint32_t> const& queue_families,
            std::vector<gsl::czstring> const& extensions,
            VkPhysicalDeviceFeatures const& features
        );
        Device(Device&) = delete;
        Device& operator=(Device&) = delete;
        Device(Device&&) = delete;
        Device& operator=(Device&&) = delete;
        ~Device() noexcept;

        VkDevice handle() const noexcept { return device_; }

        void wait_idle() const noexcept;

    private:
        VkDevice device_ = VK_NULL_HANDLE;
    };
}

#endif
