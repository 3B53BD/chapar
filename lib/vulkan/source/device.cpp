#include "chapar/vulkan/device.hpp"

namespace chapar::vulkan {

    // ==================================================
    // Use Cases
    // ==================================================

    Device::Device(
        VkPhysicalDevice physical_device,
        std::vector<uint32_t> const& queue_family_indices,
        std::vector<gsl::czstring> const& validation_layers,
        std::vector<gsl::czstring> const& enabled_extensions,
        VkPhysicalDeviceFeatures const& features
    )
    {
        std::array<float, 1> priorities = { 1.0f };
        std::vector<VkDeviceQueueCreateInfo> queues;
        for (auto const family : queue_families) {
            VkDeviceQueueCreateInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            info.pNext = {};
            info.flags = {};
            info.queueFamilyIndex = family;
            info.queueCount = static_cast<uint32_t>(priorities.size());
            info.pQueuePriorities = priorities.data();

            queues.emplace_back(info);
        }

        VkDeviceCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.queueCreateInfoCount = static_cast<uint32_t>(queues.size());
        info.pQueueCreateInfos = queues.data();
        info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
        info.ppEnabledLayerNames = validation_layers.data();
        info.enabledExtensionCount = static_cast<uint32_t>(enabled_extensions.size());
        info.ppEnabledExtensionNames = enabled_extensions.data();
        info.pEnabledFeatures = &features;

        VkResult res = vkCreateDevice(physical_device.handle(), &info, nullptr, &device_);
        if (res != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create device."};
        }
    }

    Device::~Device() noexcept
    {
        if (device_ == VK_NULL_HANDLE) return;
        vkDestroyDevice(device_, nullptr);
    }

    void Device::wait_idle() const noexcept
    {
        vkDeviceWaitIdle(device_);
    }

    VkQueue Device::queue(uint32_t family) const noexcept
    {
        VkQueue queue;
        vkGetDeviceQueue(device_, family, 0, &queue);

        return queue;
    }

    VkDevice Device::handle() const noexcept
    {
        return device_;
    }





    // ==================================================
    // Use Cases
    // ==================================================

    std::unique_ptr<Device> device(
        Instance const& instance,
        PhysicalDevice const& physical_device,
        std::vector<uint32_t> queue_family_indices,
        std::vector<gsl::czstring> const& enabled_extensions,
        VkPhysicalDeviceFeatures const& features
    )
    {
        auto validation_layers = instance.validation_layers();

        std::vector<gsl::czstring> extensions = {"VK_EXT_memory_budget"};
        extensions.insert(extensions.end(), enabled_extensions.begin(), enabled_extensions.end());
        std::sort(extensions.begin(), extensions.end());
        extensions.erase(std::unique(extensions.begin(), extensions.end()), extensions.end());

        return std::unique_ptr<Device>(
            physical_device.handle(),
            queue_family_indices,
            validation_layers,
            extensions,
            features
        );
    }
}
