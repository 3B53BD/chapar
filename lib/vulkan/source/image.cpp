#include "chapar/vulkan/image.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/util/error.hpp"

namespace chapar::vulkan {

    Image::Image(
        VmaAllocator allocator,
        VkImageCreateInfo const& image_create_info,
        VmaAllocationCreateInfo const& allocation_create_info
    )
    : allocator_{allocator}
    , layout_{image_create_info.initialLayout}
    , usage_{image_create_info.usage}
    , format_{image_create_info.format}
    , extent_{image_create_info.extent}
    , mip_levels_{image_create_info.mipLevels}
    {
        VkResult result = vmaCreateImage(allocator_, &image_create_info, &allocation_create_info, &image_, &allocation_, &allocation_info_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vma failed to create image."};
        }
    }

    Image::~Image() noexcept
    {
        if (image_ == VK_NULL_HANDLE) return;
        vmaDestroyImage(allocator_, image_, allocation_);
    }

    ImageBuilder::ImageBuilder() noexcept
    {
        reset();
    }
    ImageBuilder& ImageBuilder::reset() noexcept
    {
        image_.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        image_.pNext = {};
        image_.flags = {};
        image_.imageType = VK_IMAGE_TYPE_2D;
        image_.format = VK_FORMAT_UNDEFINED;
        image_.extent = {};
        image_.mipLevels = 1;
        image_.arrayLayers = 1;
        image_.samples = VK_SAMPLE_COUNT_1_BIT;
        image_.tiling = VK_IMAGE_TILING_OPTIMAL;
        image_.usage = {};
        image_.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        image_.queueFamilyIndexCount = {};
        image_.pQueueFamilyIndices = {};
        image_.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        allocation_.usage = {};
        allocator_ = VK_NULL_HANDLE;

        return *this;
    }
    ImageBuilder& ImageBuilder::set_allocator(VmaAllocator allocator) noexcept
    {
        allocator_ = allocator;
        return *this;
    }
    ImageBuilder& ImageBuilder::set_format(VkFormat format) noexcept
    {
        image_.format = format;
        return *this;
    }
    ImageBuilder& ImageBuilder::set_extent(VkExtent3D extent) noexcept
    {
        image_.extent = extent;
        return *this;
    }
    ImageBuilder& ImageBuilder::set_mip_levels(uint32_t mip_levels) noexcept
    {
        image_.mipLevels = mip_levels;
        return *this;
    }
    ImageBuilder& ImageBuilder::set_samples(VkSampleCountFlagBits samples) noexcept
    {
        image_.samples = samples;
        return *this;
    }
    ImageBuilder& ImageBuilder::set_tiling(VkImageTiling tiling) noexcept
    {
        image_.tiling = tiling;
        return *this;
    }
    ImageBuilder& ImageBuilder::set_image_usage(VkImageUsageFlags usage) noexcept
    {
        image_.usage = usage;
        return *this;
    }
    ImageBuilder& ImageBuilder::set_memory_usage(VmaMemoryUsage usage) noexcept
    {
        image_.usage = usage;
        return *this;
    }
    std::unique_ptr<Image> ImageBuilder::image() const noexcept
    {
        return std::make_unique<Image>(allocator_, image_, allocation_);
    }

    ImageView::ImageView(
        Device const& device,
        Image const& image,
        VkImageAspectFlags aspect_mask
    )
    : device_{device.handle()}
    {
        VkImageViewCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.image = image.handle();
        info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        info.format = image.format();
        info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        info.subresourceRange.aspectMask = aspect_mask;
        info.subresourceRange.baseMipLevel = 0;
        info.subresourceRange.levelCount = image.mip_levels();
        info.subresourceRange.baseArrayLayer = 0;
        info.subresourceRange.layerCount = 1;

        VkResult result = vkCreateImageView(device_, &info, nullptr, &view_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create image view."};
        }
    }

    ImageView::~ImageView() noexcept
    {
        if (view_ == VK_NULL_HANDLE) return;
        vkDestroyImageView(device_, view_, nullptr);
    }
}
