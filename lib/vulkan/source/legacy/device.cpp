#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/instance.hpp"
#include "chapar/vulkan/physical_device.hpp"
#include "chapar/util/error.hpp"
#include <array>
#include <vector>

namespace chapar::vulkan {

    Device::Device(
        Instance const& instance,
        PhysicalDevice const& physical_device,
        std::vector<uint32_t> const& queue_families,
        std::vector<gsl::czstring> const& extensions,
        VkPhysicalDeviceFeatures const& features
    )
    {
        std::array<float, 1> priorities = { 1.0f };
        std::vector<VkDeviceQueueCreateInfo> queues;
        for (auto const family : queue_families) {
            VkDeviceQueueCreateInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            info.pNext = {};
            info.flags = {};
            info.queueFamilyIndex = family;
            info.queueCount = static_cast<uint32_t>(priorities.size());
            info.pQueuePriorities = priorities.data();

            queues.emplace_back(info);
        }

        auto layers = instance.validation_layers();
        std::vector<gsl::czstring> exts{"VK_EXT_memory_budget"};
        exts.insert(exts.end(), extensions.begin(), extensions.end());
        std::sort(exts.begin(), exts.end());
        auto last = std::unique(exts.begin(), exts.end());
        exts.erase(last, exts.end());

        VkDeviceCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.queueCreateInfoCount = static_cast<uint32_t>(queues.size());
        info.pQueueCreateInfos = queues.data();
        info.enabledLayerCount = static_cast<uint32_t>(layers.size());
        info.ppEnabledLayerNames = layers.data();
        info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        info.ppEnabledExtensionNames = extensions.data();
        info.pEnabledFeatures = &features;

        VkResult res = vkCreateDevice(physical_device.handle(), &info, nullptr, &device_);
        if (res != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create device."};
        }
    }

    Device::~Device() noexcept
    {
        if (device_ == VK_NULL_HANDLE) return;
        vkDestroyDevice(device_, nullptr);
    }

    void Device::wait_idle() const noexcept
    {
        vkDeviceWaitIdle(device_);
    }
}
