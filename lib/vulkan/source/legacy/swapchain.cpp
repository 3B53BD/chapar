#include "chapar/vulkan/swapchain.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/physical_device.hpp"
#include "chapar/vulkan/queue.hpp"
#include "chapar/vulkan/surface.hpp"
#include "chapar/os/window.hpp"
#include "chapar/util/error.hpp"
#include <glm/glm.hpp>

namespace chapar::vulkan {

    // ==================================================
    // SwapchainAttachment
    // ==================================================

    SwapchainAttachment::SwapchainAttachment(
        VkDevice device,
        VkSwapchainKHR swapchain,
        VkFormat format,
        uint32_t width,
        uint32_t height
    )
    : format_{format}
    , extent_{width, height}
    , device_{device}
    {
        vkGetSwapchainImagesKHR(device_, swapchain, &image_count_, nullptr);
        images_ = std::make_unique<VkImage[]>(image_count_);
        image_views_ = std::make_unique<VkImageView[]>(image_count_);
        vkGetSwapchainImagesKHR(device_, swapchain, &image_count_, images_.get());

        for (gsl::index i = 0; i < image_count_; ++i) {
            VkImageViewCreateInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            info.pNext = {};
            info.flags = {};
            info.image = images_[i];
            info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            info.format = format_;
            info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            info.subresourceRange.baseMipLevel = 0;
            info.subresourceRange.levelCount = 1;
            info.subresourceRange.baseArrayLayer = 0;
            info.subresourceRange.layerCount = 1;

            VkResult result = vkCreateImageView(device_, &info, nullptr, &image_views_[i]);
            if (result != VK_SUCCESS) {
                throw Exception{"Vulkan failed to create image view."};
            }
        }
    }

    SwapchainAttachment::~SwapchainAttachment() noexcept
    {
        if (device_ == VK_NULL_HANDLE) return;

        for (gsl::index i = 0; i < image_count_; ++i) {
            vkDestroyImageView(device_, image_views_[i], nullptr);
        }
    }

    uint32_t SwapchainAttachment::width() const noexcept
    {
        return extent_.width;
    }

    uint32_t SwapchainAttachment::height() const noexcept
    {
        return extent_.height;
    }

    VkFormat SwapchainAttachment::format() const noexcept
    {
        return format_;
    }

    VkClearValue SwapchainAttachment::clear_value() const noexcept
    {
        return {{0.0f, 0.0f, 0.0f, 1.0f}};
    }

    ImageView const& SwapchainAttachment::image_view() const noexcept override
    {
        return image_view_;
    }

    uint32_t SwapchainAttachment::image_count() const noexcept
    {
        return image_count_;
    }





    // ==================================================
    // Swapchain
    // ==================================================

    Swapchain::Swapchain(
        VkDevice device,
        VkSurfaceKHR surface,
        VkSurfaceTransformFlagBitsKHR pre_transform,
        uint32_t image_count,
        VkFormat image_format,
        VkColorSpaceKHR image_color_space,
        VkExtent2D image_extent,
        std::vector<uint32_t> const& queue_family_indices,
        VkPresentModeKHR present_mode,
    )
    {
        image_count_ = image_count;
        image_extent_ = image_extent;
        present_mode_ = present_mode;
        queue_family_indices_ = queue_family_indices;
        pre_transform_ = pre_transform;
        format_.format = image_format;
        format_.colorSpace = image_color_space;
        surface_ = surface;
        device_ = device;

        create();

        attachment_ = std::make_unique<SwapchainAttachment>(device_, swapchain_, format.format, extent.width, extent.height);
        auto image_count = attachment_->image_count();
        idle_images_ = std::make_unique<VkFence[]>(image_count);
        for (gsl::index i = 0; i < image_count; ++i) {
            idle_images_[i] = VK_NULL_HANDLE;
        }
    }

    Swapchain::~Swapchain() noexcept
    {
        cleanup();
    }

    void recreate(VkExtent2D image_extent)
    {
        image_extent_ = image_extent;
        create()
    }

    void cleanup()
    {
        if (swapchain_ == VK_NULL_HANDLE) return;
        // attachment_.reset();
        vkDestroySwapchainKHR(device_, swapchain_, nullptr);
    }

    SwapchainAttachment const& Swapchain::attachment() const noexcept
    {
        return *attachment_;
    }

    uint32_t Swapchain::image_count() const noexcept
    {
        return attachment_->image_count();
    }

    uint32_t Swapchain::image_index() const noexcept
    {
        return image_index_;
    }

    void create()
    {
        VkSwapchainCreateInfoKHR info = {};
        info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        info.pNext = {};
        info.flags = {};
        info.surface = surface_;
        info.minImageCount = image_count_;
        info.imageFormat = format_.format;
        info.imageColorSpace = format_.colorSpace;
        info.imageExtent = image_extent_;
        info.imageArrayLayers = 1;
        info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

        if (queue_family_indices.size() == 1) {
            info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        }
        else {
            info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            info.queueFamilyIndexCount = static_cast<uint32_t>(queue_family_indices_.size());
            info.pQueueFamilyIndices = queue_family_indices_.data();
        }

        info.preTransform = pre_transform_;
        info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        info.presentMode = present_mode_;
        info.clipped = VK_TRUE;

        VkSwapchainKHR old_swapchain = swapchain_;
        info.oldSwapchain = old_swapchain;

        VkResult result = vkCreateSwapchainKHR(device_, &info, nullptr, &swapchain_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create swapchain."};
        }
    }

    VkResult Swapchain::aquire_next_image(VkSemaphore& semaphore)
    {
        VkResult result = vkAcquireNextImageKHR(
            device_,        // device
            swapchain_,     // swapchain
            UINT64_MAX,     // timeout
            semaphore,      // semaphore
            VK_NULL_HANDLE, // fence
            &image_index_   // pImageIndex
        );
        if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR && result != VK_ERROR_OUT_OF_DATE_KHR) {
            throw Exception{"Vulkan failed to aquire swapchain image."};
        }

        return result;
    }





    // ==================================================
    // SwapchainController
    // ==================================================

    SwapchainController::SwapchainController(
        os::Window const& window,
        Surface const& surface,
        PhysicalDevice const& physical_device,
        Device const& device,
        std::vector<uint32_t> const& queue_family_indices
    )
    {
        std::vector<VkPresentModeKHR> suitable_present_modes = {
            VK_PRESENT_MODE_MAILBOX_KHR,
            VK_PRESENT_MODE_IMMEDIATE_KHR,
            VK_PRESENT_MODE_FIFO_KHR
        };

        auto [width, height] = window.extent();
        auto surface_format = surface.format();
        auto h_surface = surface.handle();

        auto present_modes = physical_device.present_modes(h_surface);
        auto capabilities = physical_device.capabilities(h_surface);

        auto [present_mode, result] = choose_present_mode(present_modes, suitable_present_modes);
        if (!result) {
            throw Exception{"Vulkan can not find suitable swapchain present mode."};
        }

        auto image_count = choose_image_count(present_mode, capabilities);
        auto image_extent = choose_image_extent(capabilities, width, height);

        swapchain_ = Swapchain{
            device.handle(),
            surface.handle(),
            capabilities.currentTransform,
            image_count,
            surface_format.format,
            surface_format.colorSpace,
            {width, height},
            queue_family_indices,
            present_mode
        };
    }

    void SwapchainController::wait_idle(VkFence fence) noexcept
    {
        auto& idle = idle_images_[image_index_];
        if (idle != VK_NULL_HANDLE) {
            vkWaitForFences(
                device_,    // device
                1,          // fenceCount
                &idle,      // pFences
                VK_TRUE,    // waitAll
                UINT64_MAX  // timeout
            );
        }

        idle = fence;
    }

    VkResult SwapchainController::present(
        Queue const& present_queue,
        std::vector<VkSemaphore> const& wait_semaphores
    ) const
    {
        VkPresentInfoKHR info = {};
        info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        info.pNext = {};
        info.waitSemaphoreCount = static_cast<uint32_t>(wait_semaphores.size());
        info.pWaitSemaphores = wait_semaphores.data();
        info.swapchainCount = 1;
        info.pSwapchains = &swapchain_;
        info.pImageIndices = &image_index_;
        info.pResults = {};

        VkResult result = vkQueuePresentKHR(present_queue.handle(), &info);
        if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR && result != VK_ERROR_OUT_OF_DATE_KHR) {
            throw Exception{"Vulkan failed to present swapchain image."};
        }

        return result;
    }

    std::pair<VkPresentModeKHR, bool> SwapchainController::choose_present_mode(
        std::vector<VkPresentModeKHR> const& available,
        std::vector<VkPresentModeKHR> const& candidates
    ) noexcept
    {
        for (auto can : candidates) {
            for (auto ava : available) {
                if (can == ava) {
                    return std::make_pair(can, true);
                }
            }
        }

        return std::make_pair(VK_PRESENT_MODE_FIFO_KHR, false);
    }

    uint32_t SwapchainController::choose_image_count(
        VkPresentModeKHR present_mode,
        VkSurfaceCapabilitiesKHR const& cap
    )
    {
        uint32_t count = 0;
        switch (present_mode) {
            case VK_PRESENT_MODE_MAILBOX_KHR:
                count = 3;
                break;
            case VK_PRESENT_MODE_IMMEDIATE_KHR:
                count = 1;
                break;
            case VK_PRESENT_MODE_FIFO_KHR:
                count = 2;
                break;
            case VK_PRESENT_MODE_FIFO_RELAXED_KHR:
                count = 2;
                break;
            default:
                throw Exception{"Present mode is not supported."};
        }

        if (count < cap.minImageCount + 1) {
            count = cap.minImageCount + 1;
        }

        if (cap.maxImageCount > 0 && cap.maxImageCount < count) {
            count = cap.maxImageCount;
        }

        return count;
    }

    VkExtent2D SwapchainController::choose_image_extent(
        VkSurfaceCapabilitiesKHR const& cap,
        int width,
        int height
    )  noexcept
    {
        if (cap.currentExtent.width != UINT32_MAX) {
            return cap.currentExtent;
        }

        VkExtent2D extent;
        extent.width = glm::clamp(static_cast<uint32_t>(width), cap.minImageExtent.width, cap.maxImageExtent.width);
        extent.height = glm::clamp(static_cast<uint32_t>(height), cap.minImageExtent.height, cap.maxImageExtent.height);

        return extent;
    }
}
