#include "chapar/vulkan/queue.hpp"
#include "chapar/vulkan/device.hpp"

namespace chapar::vulkan {

    QueueFamily::QueueFamily(
        VkPhysicalDevice physical_device,
        VkQueueFamilyProperties properties,
        uint32_t index,
        VkSurfaceKHR surface
    )
    : index_{index}
    {
        if (properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            capabilities_ |= QueueCapabilities::graphics_bit;
        }

        if (properties.queueFlags & VK_QUEUE_COMPUTE_BIT) {
            capabilities_ |= QueueCapabilities::compute_bit;
        }

        if (properties.queueFlags & VK_QUEUE_TRANSFER_BIT) {
            capabilities_ |= QueueCapabilities::transfer_bit;
        }

        if (properties.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) {
            capabilities_ |= QueueCapabilities::sparse_binding_bit;
        }

        if (properties.queueFlags & VK_QUEUE_PROTECTED_BIT) {
            capabilities_ |= QueueCapabilities::protected_bit;
        }

        if (surface != VK_NULL_HANDLE) {
            VkBool32 res;
            vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, index_, surface, &res);
            if (res) {
                capabilities_ |= QueueCapabilities::present_bit;
            }
        }
    }

    uint32_t QueueFamily::index() const noexcept
    {
        return index_;
    }

    QueueCapabilities::Bits const& QueueFamily::capabilities() const noexcept
    {
        return capabilities_;
    }

    Queue::Queue(Device const& device, uint32_t family)
    : family_{family}
    {
        vkGetDeviceQueue(device.handle(), family_, 0, &queue_);
    }

    void Queue::wait_idle() const noexcept
    {
        vkQueueWaitIdle(queue_);
    }

    uint32_t Queue::family() const noexcept
    {
        return family_;
    }
}
