#include "chapar/vulkan/physical_device.hpp"

namespace chapar::vulkan {

    PhysicalDevice::PhysicalDevice(VkPhysicalDevice physical_device)
    : physical_device_{physical_device}
    {
        uint32_t count;
        vkEnumerateDeviceExtensionProperties(physical_device_, nullptr, &count, nullptr);
        extensions_.resize(count);
        vkEnumerateDeviceExtensionProperties(physical_device_, nullptr, &count, extensions_.data());

        vkGetPhysicalDeviceFeatures(physical_device_, &features_);
        vkGetPhysicalDeviceProperties(physical_device_, &properties_);
        vkGetPhysicalDeviceMemoryProperties(physical_device_, &memory_properties_);
    }

    std::vector<QueueFamily> PhysicalDevice::queue_families(VkSurfaceKHR surface) const noexcept
    {
        auto properties = queue_family_properties();
        std::vector<QueueFamily> families;
        families.reserve(properties.size());
        for (uint32_t i = 0; i < properties.size(); ++i) {
            families.emplace_back(physical_device_, properties[i], i, surface);
        }

        return families;
    }

    std::vector<VkQueueFamilyProperties> PhysicalDevice::queue_family_properties() const noexcept
    {
        uint32_t count;
        vkGetPhysicalDeviceQueueFamilyProperties(physical_device_, &count, nullptr);
        std::vector<VkQueueFamilyProperties> properties(count);
        vkGetPhysicalDeviceQueueFamilyProperties(physical_device_, &count, properties.data());

        return properties;
    }

    std::pair<VkSurfaceFormatKHR, bool> PhysicalDevice::supported_format(
        VkSurfaceKHR surface,
        std::span<VkSurfaceFormatKHR> candidates
    ) const noexcept
    {
        auto available = formats(surface);
        for (auto const& can : candidates) {
            for (auto const& fmt : available) {
                if (can.format == fmt.format && can.colorSpace == fmt.colorSpace) {
                    return std::make_pair(can, true);
                }
            }
        }

        VkSurfaceFormatKHR fmt;
        fmt.format = VK_FORMAT_B8G8R8A8_SRGB;
        fmt.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
        return std::make_pair(fmt, false);
    }

    std::vector<VkSurfaceFormatKHR> PhysicalDevice::formats(VkSurfaceKHR surface) const noexcept
    {
        uint32_t count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device_, surface, &count, nullptr);
        std::vector<VkSurfaceFormatKHR> formats(count);
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device_, surface, &count, formats.data());

        return formats;
    }

    VkSurfaceCapabilitiesKHR PhysicalDevice::capabilities(VkSurfaceKHR surface) const noexcept
    {
        VkSurfaceCapabilitiesKHR cap;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device_, surface, &cap);

        return cap;
    }

    std::vector<VkPresentModeKHR> PhysicalDevice::present_modes(VkSurface surface) const noexcept
    {
        uint32_t count;
        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device_, surface, &count, nullptr);
        std::vector<VkPresentModeKHR> modes(count);
        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device_, surface, &count, modes.data());

        return modes;
    }

    VkSampleCountFlagBits PhysicalDevice::max_samples() const noexcept
    {
        uint32_t const count = std::min(
            properties_.limits.framebufferColorSampleCounts, properties_.limits.framebufferDepthSampleCounts
        );

        if (count & VK_SAMPLE_COUNT_64_BIT) return VK_SAMPLE_COUNT_64_BIT;
        if (count & VK_SAMPLE_COUNT_32_BIT) return VK_SAMPLE_COUNT_32_BIT;
        if (count & VK_SAMPLE_COUNT_16_BIT) return VK_SAMPLE_COUNT_16_BIT;
        if (count & VK_SAMPLE_COUNT_8_BIT) return VK_SAMPLE_COUNT_8_BIT;
        if (count & VK_SAMPLE_COUNT_4_BIT) return VK_SAMPLE_COUNT_4_BIT;
        if (count & VK_SAMPLE_COUNT_2_BIT) return VK_SAMPLE_COUNT_2_BIT;

        return VK_SAMPLE_COUNT_1_BIT;
    }

    VkFormat PhysicalDevice::choose_format(
        std::vector<VkFormat> const& candidates,
        VkImageTiling tiling,
        VkFormatFeatureFlags features
    ) const noexcept
    {
        for (auto format : candidates)
        {
            VkFormatProperties properties;
            vkGetPhysicalDeviceFormatProperties(physical_device_, format, &properties);

            if (tiling == VK_IMAGE_TILING_LINEAR && (properties.linearTilingFeatures & features) == features) {
                return format;
            }
            else if (tiling == VK_IMAGE_TILING_OPTIMAL && (properties.optimalTilingFeatures & features) == features) {
                return format;
            }
        }

        return VK_FORMAT_UNDEFINED;
    }

    VkFormat PhysicalDevice::default_depth_format() const noexcept
    {
        std::vector<VkFormat> candidates{
            VK_FORMAT_D32_SFLOAT,
            VK_FORMAT_D32_SFLOAT_S8_UINT,
            VK_FORMAT_D24_UNORM_S8_UINT
        };

        return choose_format(candidates, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
    }
}
