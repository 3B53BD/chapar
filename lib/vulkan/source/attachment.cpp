#include "chapar/vulkan/attachment.hpp"
#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/vulkan/image.hpp"

namespace chapar::vulkan {

    // ==================================================
    // ColorAttachment
    // ==================================================

    ColorAttachment::ColorAttachment(
        Device const& device,
        Allocator const& allocator,
        VkFormat format,
        uint32_t width,
        uint32_t height,
        VkSampleCountFlagBits samples
    )
    {
        ImageBuilder ib;
        ib.set_allocator(allocator.handle());
        ib.set_format(format);
        ib.set_extent({width, height, 1});
        ib.set_samples(samples);
        ib.set_image_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
        ib.set_memory_usage(VMA_MEMORY_USAGE_GPU_ONLY);
        image_ = ib.image();

        image_view_ = std::make_unique<ImageView>(device, *image_, VK_IMAGE_ASPECT_COLOR_BIT);
    }

    ColorAttachment::~ColorAttachment() noexcept
    {
        image_view_.reset();
        image_.reset();
    }

    uint32_t ColorAttachment::width() const noexcept
    {
        auto& extent = image_->extent();
        return static_cast<uint32_t>(extent.width);
    }

    uint32_t ColorAttachment::height() const noexcept
    {
        auto& extent = image_->extent();
        return static_cast<uint32_t>(extent.height);
    }

    VkFormat ColorAttachment::format() const noexcept
    {
        return image_->format();
    }

    VkClearValue ColorAttachment::clear_value() const noexcept
    {
        return {{0.0f, 0.0f, 0.0f, 1.0f}};
    }

    ImageView const& ColorAttachment::image_view() const noexcept override
    {
        return *image_view_;
    }





    // ==================================================
    // DepthAttachment
    // ==================================================

    DepthAttachment::DepthAttachment(
        Device const& device,
        Allocator const& allocator,
        VkFormat format,
        uint32_t width,
        uint32_t height,
        VkSampleCountFlagBits samples
    )
    {
        ImageBuilder ib;
        ib.set_allocator(allocator.handle());
        ib.set_format(format);
        ib.set_extent({width, height, 1});
        ib.set_samples(samples);
        ib.set_image_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);
        ib.set_memory_usage(VMA_MEMORY_USAGE_GPU_ONLY);
        image_ = ib.image();

        image_view_ = std::make_unique<ImageView>(device, *image_, VK_IMAGE_ASPECT_DEPTH_BIT);
    }

    DepthAttachment::~DepthAttachment() noexcept
    {
        image_view_.reset();
        image_.reset();
    }

    uint32_t DepthAttachment::width() const noexcept
    {
        auto& extent = image_->extent();
        return static_cast<uint32_t>(extent.width);
    }

    uint32_t DepthAttachment::height() const noexcept
    {
        auto& extent = image_->extent();
        return static_cast<uint32_t>(extent.height);
    }

    VkFormat DepthAttachment::format() const noexcept
    {
        return image_->format();
    }

    VkClearValue DepthAttachment::clear_value() const noexcept
    {
        return {1.0f, 0};
    }

    ImageView const& DepthAttachment::image_view() const noexcept override
    {
        return *image_view_;
    }
}
