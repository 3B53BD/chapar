#include "chapar/vulkan/texture.hpp"

namespace chapar::vulkan {

    void Texture::create_image(
        Allocator const& allocator,
        VkFormat format,
        VkImageUsageFlags usage
    )
    {
        if (!(usage & VK_IMAGE_USAGE_TRANSFER_DST_BIT)) {
            usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
        }

        Image_Builder ib;
        ib.allocator(allocator);
        ib.format(format);
        ib.extent({width_, height_, 1});
        ib.mip_levels(mip_levels_);
        ib.tiling(VK_IMAGE_TILING_OPTIMAL);
        ib.image_usage(usage);
        ib.memory_usage(VMA_MEMORY_USAGE_GPU_ONLY);
        image_ = ib.image();
    }
}
