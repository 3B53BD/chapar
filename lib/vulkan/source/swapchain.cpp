#include "chapar/vulkan/swapchain.hpp"

namespace chapar::vulkan {

    Swapchain::Swapchain(
        VkDevice device,
        VkSurfaceKHR surface,
        VkSurfaceTransformFlagBitsKHR pre_transform,
        uint32_t image_count,
        VkFormat image_format,
        VkColorSpaceKHR image_color_space,
        VkExtent2D image_extent,
        std::vector<uint32_t> const& queue_family_indices,
        VkPresentModeKHR present_mode
    )
    {
        image_count_ = image_count;
        image_extent_ = image_extent;
        present_mode_ = present_mode;
        queue_family_indices_ = queue_family_indices;
        pre_transform_ = pre_transform;
        format_.format = image_format;
        format_.colorSpace = image_color_space;
        surface_ = surface;
        device_ = device;

        create();
    }

    Swapchain::~Swapchain()
    {
        if (swapchain_ == VK_NULL_HANDLE) return;
        vkDestroySwapchainKHR(device_, swapchain_, nullptr);
    }

    void Swapchain::resize(VkExtent2D extent)
    {
        create();
    }

    VkResult Swapchain::aquire_next_image(VkSemaphore& semaphore)
    {
        VkResult result = vkAcquireNextImageKHR(
            device_,        // device
            swapchain_,     // swapchain
            UINT64_MAX,     // timeout
            semaphore,      // semaphore
            VK_NULL_HANDLE, // fence
            &image_index_   // pImageIndex
        );
        if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR && result != VK_ERROR_OUT_OF_DATE_KHR) {
            throw Exception{"Vulkan failed to aquire swapchain image."};
        }

        return result;
    }

    std::vector<VkImage> images() const noexcept
    {
        vkGetSwapchainImagesKHR(device_, swapchain_, &image_count_, nullptr);
        std::vector<VkImage> images(image_count_);
        vkGetSwapchainImagesKHR(device_, swapchain_, &image_count_, images.get());

        return images;
    }

    uint32_t image_count() const noexcept
    {
        return image_count_;
    }

    uint32_t image_index() const noexcept
    {
        return image_index_;
    }

    uint32_t vk_swapchain() const noexcept
    {
        return swapchain_;
    }

    void Swapchain::create()
    {
        VkSwapchainCreateInfoKHR info = {};
        info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        info.pNext = {};
        info.flags = {};
        info.surface = surface_;
        info.minImageCount = image_count_;
        info.imageFormat = format_.format;
        info.imageColorSpace = format_.colorSpace;
        info.imageExtent = image_extent_;
        info.imageArrayLayers = 1;
        info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

        if (queue_family_indices.size() == 1) {
            info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        }
        else {
            info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            info.queueFamilyIndexCount = static_cast<uint32_t>(queue_family_indices_.size());
            info.pQueueFamilyIndices = queue_family_indices_.data();
        }

        info.preTransform = pre_transform_;
        info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        info.presentMode = present_mode_;
        info.clipped = VK_TRUE;

        VkSwapchainKHR old_swapchain = swapchain_;
        info.oldSwapchain = old_swapchain;

        VkResult result = vkCreateSwapchainKHR(device_, &info, nullptr, &swapchain_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create swapchain."};
        }
    }





    // ==================================================
    // Use Cases
    // ==================================================

    std::pair<VkPresentModeKHR, bool> choose_present_mode(
        std::vector<VkPresentModeKHR> const& available,
        std::vector<VkPresentModeKHR> const& candidates
    ) noexcept
    {
        for (auto can : candidates) {
            for (auto ava : available) {
                if (can == ava) {
                    return std::make_pair(can, true);
                }
            }
        }

        return std::make_pair(VK_PRESENT_MODE_FIFO_KHR, false);
    }

    uint32_t choose_image_count(
        VkPresentModeKHR present_mode,
        VkSurfaceCapabilitiesKHR const& cap
    )
    {
        uint32_t count = 0;
        switch (present_mode) {
            case VK_PRESENT_MODE_MAILBOX_KHR:
                count = 3;
                break;
            case VK_PRESENT_MODE_IMMEDIATE_KHR:
                count = 1;
                break;
            case VK_PRESENT_MODE_FIFO_KHR:
                count = 2;
                break;
            case VK_PRESENT_MODE_FIFO_RELAXED_KHR:
                count = 2;
                break;
            default:
                throw Exception{"Present mode is not supported."};
        }

        if (count < cap.minImageCount + 1) {
            count = cap.minImageCount + 1;
        }

        if (cap.maxImageCount > 0 && cap.maxImageCount < count) {
            count = cap.maxImageCount;
        }

        return count;
    }

    VkExtent2D choose_image_extent(
        VkSurfaceCapabilitiesKHR const& cap,
        int width,
        int height
    )  noexcept
    {
        if (cap.currentExtent.width != UINT32_MAX) {
            return cap.currentExtent;
        }

        VkExtent2D extent;
        extent.width = glm::clamp(static_cast<uint32_t>(width), cap.minImageExtent.width, cap.maxImageExtent.width);
        extent.height = glm::clamp(static_cast<uint32_t>(height), cap.minImageExtent.height, cap.maxImageExtent.height);

        return extent;
    }

    std::unique_ptr<Swapchain> create_swapchain(
        os::Window const& window,
        Surface const& surface,
        PhysicalDevice const& physical_device,
        Device const& device,
        std::vector<uint32_t> const& queue_family_indices
    )
    {
        std::vector<VkPresentModeKHR> suitable_present_modes = {
            VK_PRESENT_MODE_MAILBOX_KHR,
            VK_PRESENT_MODE_IMMEDIATE_KHR,
            VK_PRESENT_MODE_FIFO_KHR
        };

        auto [width, height] = window.extent();
        auto surface_format = surface.format();
        auto h_surface = surface.handle();

        auto present_modes = physical_device.present_modes(h_surface);
        auto capabilities = physical_device.capabilities(h_surface);

        auto [present_mode, result] = choose_present_mode(present_modes, suitable_present_modes);
        if (!result) {
            throw Exception{"Vulkan can not find suitable swapchain present mode."};
        }

        auto image_count = choose_image_count(present_mode, capabilities);
        auto image_extent = choose_image_extent(capabilities, width, height);

        return std::make_unique<Swapchain>(
            device.handle(),
            surface.handle(),
            capabilities.currentTransform,
            image_count,
            surface_format.format,
            surface_format.colorSpace,
            {width, height},
            queue_family_indices,
            present_mode
        );
    }

    void wait_idle()
}
