#include "chapar/vulkan/framebuffer.hpp"
#include "chapar/vulkan/attachment.hpp"
#include "chapar/core/error.hpp"

namespace chapar::vulkan {

    Framebuffer::Framebuffer(
        VkRenderPass const& render_pass,
        std::vector<Attachment const*> const& attachments,
        uint32_t width,
        uint32_t height
    )
    {
        std::vector<VkImageView> image_views;
        for (auto attachment : attachments) {
            auto const& image_view = attachment.image_view();
            image_views.push_back(image_view.handle());
        }

        VkFramebufferCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.renderPass = render_pass.handle();
        info.attachmentCount = static_cast<uint32_t>(image_views.size());
        info.pAttachments = image_views.data();
        info.width = width;
        info.height = height;
        info.layers = 1;

        VkResult result = vkCreateFramebuffer(device_, &info, nullptr, &framebuffer_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create framebuffer."};
        }
    }

    Framebuffer::~Framebuffer() noexcept
    {
        if (framebuffer_ == VK_NULL_HANDLE) return;
        vkDestroyFramebuffer(device_, framebuffer_, nullptr);
    }
}
