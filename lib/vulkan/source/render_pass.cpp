#include "chapar/vulkan/render_pass.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/util/error.hpp"
#include <gsl-lite/gsl-lite.hpp>

namespace chapar::vulkan {

    VkAttachmentDescription attachment_description() noexcept
    {
        VkAttachmentDescription ad = {};
        ad.flags = {};
        ad.format = VK_FORMAT_UNDEFINED;
        ad.samples = VK_SAMPLE_COUNT_1_BIT;
        ad.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        ad.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        ad.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        ad.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        ad.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        ad.finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        return ad;
    }

    std::pair<gsl::index, bool> dependency_index(
        gsl::index start_index,
        std::vector<Subpass> subpasses,
        Subpass* dependency
    )
    {
        for (gsl::index i = start_index; i < subpasses.size(); ++i) {
            if (&subpasses[i] == dependency) {
                return std::make_pair(i, true);
            }
        }

        return std::make_pair(0, false);
    }

    struct SubpassRef {
        std::vector<VkAttachmentReference> color_refs;
        std::vector<VkAttachmentReference> resolve_refs;
        VkAttachmentReference depth_ref;
        std::vector<std::pair<Subpass*, VkSubpassDependency>> dependencies;
    };

    RenderPass::RenderPass(
        Device const& device,
        std::vector<VkAttachmentDescription> attachments,
        std::vector<Subpass> const& subpasses
    )
    : subpasses_{subpasses}
    , device_{device.handle()}
    {
        std::vector<VkSubpassDescription> descriptions;
        std::vector<VkSubpassDependency> dependencies;
        std::vector<SubpassRef> refs(subpasses_.size());

        for (gsl::index subpass_index = 0; subpass_index < subpasses_.size(); ++subpass_index) {
            auto& subpass = subpasses_[subpass_index];

            for (auto const& color_ref : subpass.color_refs) {
                VkAttachmentReference ref = {};
                ref.attachment = color_ref;
                ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                refs[subpass_index].color_refs.push_back(ref);
            }

            for (auto const& resolve_ref : subpass.resolve_refs) {
                VkAttachmentReference ref = {};
                ref.attachment = resolve_ref;
                ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                refs[subpass_index].resolve_refs.push_back(ref);
            }

            VkAttachmentReference const* depth_ref = nullptr;
            if (subpass.depth_ref.has_value()) {
                VkAttachmentReference ref = {};
                ref.attachment = subpass.depth_ref.value();
                ref.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
                refs[subpass_index].depth_ref = ref;

                depth_ref = &refs[subpass_index].depth_ref;
            }

            VkSubpassDescription h_desc = {};
            h_desc.flags = {};
            h_desc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
            h_desc.inputAttachmentCount = {};
            h_desc.pInputAttachments = {};
            h_desc.colorAttachmentCount = static_cast<uint32_t>(refs[subpass_index].color_refs.size());
            h_desc.pColorAttachments = refs[subpass_index].color_refs.data();
            h_desc.pResolveAttachments = refs[subpass_index].resolve_refs.data();
            h_desc.pDepthStencilAttachment = depth_ref;
            h_desc.preserveAttachmentCount = {};
            h_desc.pPreserveAttachments = {};

            descriptions.push_back(h_desc);

            for (auto& dependency : subpass.dependencies) {
                auto [index, result] = dependency_index(subpass_index, subpasses_, dependency.first);
                if (!result) {
                    throw Exception{"Subpass dependency is not found."};
                }

                dependency.second.srcSubpass = static_cast<uint32_t>(subpass_index);
                dependency.second.dstSubpass = static_cast<uint32_t>(index);

                dependencies.push_back(dependency.second);
            }
        }

        VkRenderPassCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        info.pNext = {};
        info.flags = {};
        info.attachmentCount = static_cast<uint32_t>(attachments.size());
        info.pAttachments = attachments.data();
        info.subpassCount = static_cast<uint32_t>(descriptions.size());
        info.pSubpasses = descriptions.data();
        info.dependencyCount = static_cast<uint32_t>(dependencies.size());
        info.pDependencies = dependencies.data();

        VkResult result = vkCreateRenderPass(device_, &info, nullptr, &render_pass_);
        if (result != VK_NULL_HANDLE) {
            throw Exception{"Vulkan failed to create render pass."};
        }
    }

    RenderPass::~RenderPass() noexcept
    {
        if (render_pass_ == VK_NULL_HANDLE) return;
        vkDestroyRenderPass(device_, render_pass_, nullptr);
    }
}
