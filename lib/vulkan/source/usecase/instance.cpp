#include "chapar/vulkan/instance.hpp"

namespace chapar::vulkan {

    Instance::Instance()
    {
        std::unique_ptr<os::Window> temp_window = std::make_unique<sdl::VulkanWindow>();
        instance_ = std::make_unique<core::Instance>(*temp_window);
        auto surface = std::make_shared<core::Surface>(*instance, *temp_window);

        // Choose the first available physical device.
        VkPhysicalDeviceFeatures features = {};
        features.samplerAnisotropy = VK_TRUE;
        features.sampleRateShading = VK_TRUE;
        physical_device_ = instance_->enumerate_physical_devices()[0];

        // Find appropriate supported format for surface.
        std::vector<VkSurfaceFormatKHR> candidates(1);
        candidates[0].format = VK_FORMAT_B8G8R8A8_SRGB;
        candidates[0].colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
        auto [format, result] = physical_device_->supported_format(surface.handle(), candidates);
        if (!result) {
            throw Exception{"No surface format available is appropriate."};
        }
        surface->set_format(format);

        // Find queue family indices.
        auto const families = physical_device.queue_families(surface.handle());
        std::optional<uint32_t> graphics_family;
        std::optional<uint32_t> present_family;

        for (auto const& family : families) {
            auto const& capabilities = family.capabilities();

            if (!graphics_family && (capabilities & QueueCapabilities::graphics_bit).any()) {
                graphics_family = family.index();
            }

            if (!present_family && (capabilities & QueueCapabilities::present_bit).any()) {
                present_family = family.index();
            }

            if (graphics_family && present_family) {
                break;
            }
        }

        if (!graphics_family || !present_family) {
            throw Exception{"No queue family is found."};
        }

        // Create device.
        std::vector<uint32_t> indices = {graphics_family, present_family};
        std::sort(indices.begin(), indices.end());
        auto last = std::unique(indices.begin(), indices.end());
        indices.erase(last, indices.end());
        std::vector<gsl::czstring> extensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
        device_ = std::make_unique<Device>(*instance_, *physical_device_, indices, extensions, features);

        allocator_ = std::unique_ptr<Allocator>(*instance_, *physical_device_, *device_);

        graphics_queue_ = Queue{*device_, graphics_family};
        present_queue_ = Queue{*device_, present_family};
    }

    Window Instance::open_window()
    {

    }
}
