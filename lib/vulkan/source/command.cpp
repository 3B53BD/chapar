#include "chapar/vulkan/command.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/util/error.hpp"

namespace chapar::vulkan {

    // ==================================================
    // CommandPool
    // ==================================================

    CommandPool::CommandPool(
        Device const& device,
        VkCommandPoolCreateFlags flags,
        Queue queue
    )
    : device_{device.handle()}
    , queue_{std::move(queue)}
    {
        VkCommandPoolCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        info.pNext = {};
        info.flags = flags;
        info.queueFamilyIndex = queue_.family();

        VkResult result = vkCreateCommandPool(device_, &info, nullptr, &command_pool_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create command pool."};
        }
    }

    CommandPool::~CommandPool() noexcept
    {
        if (command_pool_ == VK_NULL_HANDLE) return;
        vkDestroyCommandPool(device_, command_pool_, nullptr);
    }

    std::vector<CommandBuffer> CommandPool::allocate_command_buffers(uint32_t count) const
    {
        VkCommandBufferAllocateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        info.pNext = {};
        info.commandPool = command_pool_;
        info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        info.commandBufferCount = count;

        std::vector<VkCommandBuffer> handles(count);
        VkResult result = vkAllocateCommandBuffers(device_, &info, handles.data());
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to allocate command buffers."};
        }

        std::vector<CommandBuffer> cmd_bufs;
        for (auto handle : handles) {
            cmd_bufs.emplace_back(handle);
        }

        return cmd_bufs;
    }

    void CommandPool::free_command_buffers(std::span<CommandBuffer> command_buffers) const noexcept
    {
        auto size = command_buffers.size();
        std::vector<VkCommandBuffer> handles(size);
        for (gsl::index i{}; i < size; ++i) {
            handles[i] = command_buffers[i].handle();
        }
        vkFreeCommandBuffers(device_, command_pool_, static_cast<uint32_t>(size), handles.data());
    }





    // ==================================================
    // CommandBuffer
    // ==================================================

    CommandBuffer::CommandBuffer(VkCommandBuffer command_buffer) noexcept
    : command_buffer_{command_buffer}
    { }

    VkResult CommandBuffer::begin(VkCommandBufferUsageFlags flags) const noexcept
    {
        VkCommandBufferBeginInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        info.pNext = {};
        info.flags = flags;
        info.pInheritanceInfo = {};

        return vkBeginCommandBuffer(command_buffer_, &info);
    }

    VkResult CommandBuffer::end() const noexcept
    {
        return vkEndCommandBuffer(command_buffer_);
    }
}
