#include "chapar/vulkan/allocator.hpp"
#include "chapar/vulkan/instance.hpp"
#include "chapar/vulkan/physical_device.hpp"
#include "chapar/vulkan/device.hpp"
#include "chapar/util/error.hpp"

namespace chapar::vulkan {

    Allocator::Allocator(
        Instance const& instance,
        PhysicalDevice const& physical_device,
        Device const& device
    )
    {
        VmaAllocatorCreateInfo info = {};
        info.instance = instance.handle();
        info.physicalDevice = physical_device.handle();
        info.device = device.handle();

        VkResult result = vmaCreateAllocator(&info, &allocator_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vma failed to create allocator."};
        }
    }

    Allocator::~Allocator() noexcept
    {
        if (allocator_ == VK_NULL_HANDLE) return;
        vmaDestroyAllocator(allocator_);
    }
}
