#include "chapar/vulkan/queue.hpp"

namespace chapar::vulkan {

    // ==================================================
    // Use Cases
    // ==================================================

    QueueFamily::QueueFamily(
        uint32_t index,
        QueueCapabilities::Bits capabilities
    )
    : index_{index}
    , capabilities_{capabilities}
    { }

    uint32_t QueueFamily::index() const noexcept
    {
        return index_;
    }

    QueueCapabilities::Bits const& QueueFamily::capabilities() const noexcept
    {
        return capabilities_;
    }





    // ==================================================
    // Use Cases
    // ==================================================

    QueueCapabilities::Bits queue_capabilities(
        PhysicalDevice const& physical_device,
        VkQueueFamilyProperties properties,
        uint32_t index,
        VkSurfaceKHR surface
    )
    {
        QueueCapabilities::Bits capabilities;

        if (properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            capabilities |= QueueCapabilities::graphics_bit;
        }

        if (properties.queueFlags & VK_QUEUE_COMPUTE_BIT) {
            capabilities |= QueueCapabilities::compute_bit;
        }

        if (properties.queueFlags & VK_QUEUE_TRANSFER_BIT) {
            capabilities |= QueueCapabilities::transfer_bit;
        }

        if (properties.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) {
            capabilities |= QueueCapabilities::sparse_binding_bit;
        }

        if (properties.queueFlags & VK_QUEUE_PROTECTED_BIT) {
            capabilities |= QueueCapabilities::protected_bit;
        }

        if (surface != VK_NULL_HANDLE) {
            VkBool32 result; // CHECK
            vkGetPhysicalDeviceSurfaceSupportKHR(physical_device.handle(), index_, surface, &result);
            if (result) {
                capabilities |= QueueCapabilities::present_bit;
            }
        }

        return capabilities;
    }

    std::vector<QueueFamily> create_queue_families(
        PhysicalDevice const& physical_device,
        Surface const& surface
    ) noexcept
    {
        auto properties = physical_device.queue_family_properties();
        std::vector<QueueFamily> families;
        for (uint32_t i = 0; i < properties.size(); ++i) {
            families.emplace_back(i, properties[i]);
        }

        return families;
    }





    // ==================================================
    // Queue
    // ==================================================

    Queue::Queue(VkQueue queue, uint32_t family)
    : queue_{queue}
    , family_{family}
    { }

    void Queue::wait_idle() const noexcept
    {
        vkQueueWaitIdle(queue_);
    }

    uint32_t Queue::family() const noexcept
    {
        return family_;
    }





    // ==================================================
    // Queue
    // ==================================================

    Queue queue(Device const& device, uint32_t family)
    {
        return {device.queue(family), family};
    }
}
