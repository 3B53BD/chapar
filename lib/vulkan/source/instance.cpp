#include "chapar/vulkan/core/instance.hpp"
#include "chapar/vulkan/core/physical_device.hpp"
#include "chapar/sdl/window.hpp"
#include "chapar/util/error.hpp"
#include <iostream>
#include <string>
#include <cstring>

using namespace std::literals::string_literals;

namespace chapar::vulkan::core {

#ifndef NDEBUG
    VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
        VkDebugUtilsMessageSeverityFlagBitsEXT,
        VkDebugUtilsMessageTypeFlagsEXT,
        VkDebugUtilsMessengerCallbackDataEXT const* data,
        void*
    )
    {
        std::cerr << "#### validation layer ####\n" << data->pMessage << "\n\n";

        return VK_FALSE;
    }

    void Instance::check_validation_layers_support() const
    {
        uint32_t count;
        vkEnumerateInstanceLayerProperties(&count, nullptr);
        std::vector<VkLayerProperties> available(count);
        vkEnumerateInstanceLayerProperties(&count, available.data());

        for (auto const name : validation_layers_) {
            bool found = false;
            for (auto const& ava : available) {
                if (strcmp(ava.layerName, name) == 0) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                throw Exception{"Validation layer '"s + name + "' is not available."};
            }
        }
    }
#endif

    Instance::Instance(sdl::VulkanWindow const& window)
    {
        VkResult result;

        VkApplicationInfo app = {};
        app.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app.pNext = {};
        app.pApplicationName = "Manuka";
        app.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
        app.pEngineName = "No Engine";
        app.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        app.apiVersion = VK_API_VERSION_1_2;

#ifndef NDEBUG
        validation_layers_.push_back("VK_LAYER_KHRONOS_validation");
        check_validation_layers_support();
#endif

        extensions_ = window.extensions();
        extensions_.push_back("VK_KHR_get_physical_device_properties2");
        extensions_.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        std::sort(extensions_.begin(), extensions_.end());
        auto last = std::unique(extensions_.begin(), extensions_.end());
        extensions_.erase(last, extensions_.end());

        VkInstanceCreateInfo ici = {};
        ici.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        ici.flags = {};
        ici.pApplicationInfo = &app;
        ici.enabledLayerCount = static_cast<uint32_t>(validation_layers_.size());
        ici.ppEnabledLayerNames = validation_layers_.data();
        ici.enabledExtensionCount = static_cast<uint32_t>(extensions_.size());
        ici.ppEnabledExtensionNames = extensions_.data();

#ifndef NDEBUG
        VkDebugUtilsMessengerCreateInfoEXT dci = {};
        dci.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        dci.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        dci.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        dci.pfnUserCallback = debug_callback;

        ici.pNext = &dci;
#endif

        result = vkCreateInstance(&ici, nullptr, &instance_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create instance."};
        }

#ifndef NDEBUG
        auto func = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance_, "vkCreateDebugUtilsMessengerEXT"));
        if (!func) {
            throw Exception{"Vulkan failed to get address of debug utils messenger ext."};
        }

        result = func(instance_, &dci, nullptr, &debug_messenger_);
        if (result != VK_SUCCESS) {
            throw Exception{"Vulkan failed to create debug utils messenger ext."};
        }
#endif
    }

    Instance::~Instance() noexcept
    {
        if (instance_ == VK_NULL_HANDLE) return;
#ifndef NDEBUG
        auto func = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance_, "vkDestroyDebugUtilsMessengerEXT" ));
        if (func) {
            func(instance_, debug_messenger_, nullptr);
        }
#endif
        vkDestroyInstance(instance_, nullptr);
    }

    std::vector<gsl::czstring> Instance::validation_layers() const noexcept
    {
        return validation_layers_;
    }

    std::vector<VkPhysicalDevice> Instance::physical_devices() const noexcept
    {
        uint32_t count;
        vkEnumeratePhysicalDevices(instance_, &count, nullptr);
        std::vector<VkPhysicalDevice> devices(count);
        vkEnumeratePhysicalDevices(instance_, &count, devices.data());

        return devices;
    }
}
