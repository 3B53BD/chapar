#include "chapar/vulkan/sync.hpp"

namespace chapar::vulkan {

    Synchronization::Synchronization(Device const& device, std::size_t parallel_frames) noexcept
    : device_{device.handle()}
    {
        VkSemaphoreCreateInfo sci = {};
        sci.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        sci.pNext = {};
        sci.flags = {};

        VkFenceCreateInfo fci = {};
        fci.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fci.pNext = {};
        fci.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        frames_.resize(parallel_frames);
        for (auto& frame : frames_) {
            vkCreateFence(device_, &fci, nullptr, &frame.idle);
            vkCreateSemaphore(device_, &sci, nullptr, &frame.aquired);
            vkCreateSemaphore(device_, &sci, nullptr, &frame.rendered);
        }
    }

    Synchronization::~Synchronization() noexcept
    {
        if (device_ == VK_NULL_HANDLE) return;

        for (auto& frame : frames_) {
            vkDestroyFence(device_, frame.idle, nullptr);
            vkDestroySemaphore(device_, frame.aquired, nullptr);
            vkDestroySemaphore(device_, frame.rendered, nullptr);
        }
    }

    std::pair<FrameSync, VkResult> Synchronization::wait_idle() noexcept
    {
        VkResult result = vkWaitForFences(device_, 1, &frames_[current_].idle_, VK_TRUE, UINT64_MAX);

        return std::make_pair(frames_[current_], result);
    }

    void Synchronization::advance() noexcept
    {
        current_ = (current_ + 1) % frame_count_;
    }
}
