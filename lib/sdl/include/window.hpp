#ifndef CHAPAR_SDL_WINDOW_HEADER_GUARD
#define CHAPAR_SDL_WINDOW_HEADER_GUARD

#include "chapar/os/window.hpp"
#include <gsl-lite/gsl-lite.hpp>
#include <vulkan/vulkan.h>
#include <bitset>
#include <string>
#include <utility>
#include <vector>

class SDL_Window;

namespace chapar::sdl {

    class Window : public chapar::os::WindowImpl {
    public:
        Window() = default;
        Window(uint32_t flags);
        Window(Window&) = delete;
        Window& operator=(Window&) = delete;
        Window(Window&&) = delete;
        Window& operator=(Window&&) = delete;
        ~Window() noexcept;

        std::string title() const noexcept override;

        std::pair<int, int> origin() const noexcept override;
        std::pair<int, int> extent() const noexcept override;

        bool is_fullscreen() const noexcept override;
        bool is_focused() const noexcept override;
        bool is_resizable() const noexcept override;

        void set_origin(int x, int y) const noexcept override;
        void set_extent(int width, int height) const noexcept override;

        void set_resizable(bool resizable) noexcept override;
        void set_fullscreen(os::WindowFullscreanMode mode) noexcept override;
        void set_maximize() const noexcept override;
        void set_minimize() const noexcept override;

        void set_focus() noexcept override;

        void set_title(std::string_view title) const noexcept override;

        std::vector<gsl::czstring> extensions() const noexcept;

        SDL_Window* handle() const noexcept { return window_; }

    protected:
        SDL_Window* window_ = nullptr;

    private:
        static constexpr std::uint8_t resizable_  = 0;
        static constexpr std::uint8_t fullscreen_ = 1;
        static constexpr std::uint8_t focused_    = 2;
        std::bitset<3> status_;
    };

    class VulkanWindow : public Window {
    public:
        VulkanWindow();
        VulkanWindow(VulkanWindow&) = delete;
        VulkanWindow& operator=(VulkanWindow&) = delete;
        VulkanWindow(VulkanWindow&&) = delete;
        VulkanWindow& operator=(VulkanWindow&&) = delete;
        ~VulkanWindow() = default;

        std::vector<gsl::czstring> extensions() const noexcept;
        VkSurfaceKHR surface(VkInstance instance) const;
    };
}

#endif
