set(TARGET_NAME ${PROJECT_NAME})

add_executable(${TARGET_NAME})

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_20)

target_compile_options(${TARGET_NAME} PRIVATE ${CHAPAR_WARNINGS})
