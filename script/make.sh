#!/bin/bash

readonly COMPILER="-DCMAKE_CXX_COMPILER"
readonly CLANG="${COMPILER}=/usr/bin/clang++"

readonly BUILD_TYPE="-DCMAKE_BUILD_TYPE"
readonly DEBUG="${BUILD_TYPE}=Debug"
readonly RELEASE="${BUILD_TYPE}=Release"

readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
readonly SOURCE_DIR="${SCRIPT_DIR}/.."
readonly DEBUG_DIR="${SOURCE_DIR}/build/debug"
readonly RELEASE_DIR="${SOURCE_DIR}/build/release"

function build()
{
    local build_type=$1
    local source="${SOURCE_DIR}"
    local binary="$2/cmake"
    local flags="${CLANG} ${build_type}"
    cmake -S ${source} -B ${binary} ${flags}
    cmake --build ${binary}
}

while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do
    if [[ "$1" == *d* ]]; then build ${DEBUG} ${DEBUG_DIR}; fi;
    if [[ "$1" == *r* ]]; then build ${RELEASE} ${RELEASE_DIR}; fi;
    shift;
done
if [[ "$1" == '--' ]]; then shift; fi
